"""
Tests for `pointing_offset_pipeline_standalone` module.
"""

from unittest.mock import patch

import pytest

from ska_sdp_wflow_pointing_offset.pointing_pipeline_standalone import (
    StandAlonePipeline,
)
from tests.test_pointing_offset_pipeline import DEFAULT_ARGS


def test_standalone_pipeline_results_dir():
    """
    results_dir property is correctly set based on input args.
    """
    args = DEFAULT_ARGS.copy()
    res_dir = "my-save-dir"
    args["--results_dir"] = res_dir

    pipeline = StandAlonePipeline(args)
    assert pipeline.results_dir == res_dir


@pytest.mark.parametrize(
    "results_dir, expected_prefix",
    [
        (None, f"{DEFAULT_ARGS['--msdir']}/my-scan_"),
        ("/my-results-dir/bla", "/my-results-dir/bla/my-scan_"),
    ],
)
@patch("os.path.exists")
@patch("glob.glob")
def test_standalone_pipeline_common_prefix(
    mock_dir, path_exists, results_dir, expected_prefix
):
    """
    common_prefix attribute is correctly set when a
    prefix of MS data exists. With and without
    results_dir set.
    """
    path_exists.return_value = True
    mock_dir.return_value = [
        f"{DEFAULT_ARGS['--msdir']}/my-scan-1.ms",
        f"{DEFAULT_ARGS['--msdir']}/my-scan-2.ms",
        f"{DEFAULT_ARGS['--msdir']}/my-scan-3.ms",
    ]

    args = DEFAULT_ARGS.copy()
    args["--results_dir"] = results_dir
    pipeline = StandAlonePipeline(args)

    assert pipeline.common_prefix == expected_prefix


@pytest.mark.parametrize(
    "results_dir, expected_prefix",
    [
        (None, f"{DEFAULT_ARGS['--msdir']}/test_"),
        ("/my-results-dir/bla", "/my-results-dir/bla/test_"),
    ],
)
@patch("os.path.exists")
@patch("glob.glob")
def test_standalone_pipeline_common_prefix_noprefix(
    mock_dir, path_exists, results_dir, expected_prefix
):
    """
    common_prefix attribute is correctly set when no
    common MS prefix exists. With and without
    results_dir set.
    """
    path_exists.return_value = True
    mock_dir.return_value = [
        f"{DEFAULT_ARGS['--msdir']}/something-1.ms",
        f"{DEFAULT_ARGS['--msdir']}/other-thing-2.ms",
        f"{DEFAULT_ARGS['--msdir']}/third-thing-3.ms",
    ]

    args = DEFAULT_ARGS.copy()
    args["--results_dir"] = results_dir
    pipeline = StandAlonePipeline(args)

    assert pipeline.common_prefix == expected_prefix
