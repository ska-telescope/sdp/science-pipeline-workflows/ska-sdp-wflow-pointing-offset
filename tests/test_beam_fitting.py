"""
Unit tests for beam fitting functions
"""

import numpy
import pytest

from ska_sdp_wflow_pointing_offset.beam_fitting import (
    _fwhm_to_sigma,
    _sigma_to_fwhm,
)


def test_fwhm_to_sigma():
    """
    Unit test for _fwhm_to_sigma
    """
    fwhm = 2.0
    numpy.testing.assert_almost_equal(
        _fwhm_to_sigma(fwhm), 0.849322, decimal=6
    )


def test_sigma_to_fwhm():
    """
    Unit test for _sigma_to_fwhm
    """
    sigma = 0.01
    numpy.testing.assert_almost_equal(
        _sigma_to_fwhm(sigma), 0.023548, decimal=6
    )


@pytest.mark.parametrize("num_chunks", [1, 16])
def test_fit_to_vis(
    fullband_two_dish_fitted_beams,
    two_dish_fitted_beams,
    num_chunks,
    two_dish_ants,
):
    """
    Unit test for fitting primary beams to visibility amplitudes.
    Check the fitted_offsets_weighted_average_vis test for the
    fitted values.
    """
    if num_chunks == 1:
        fitted_beams = fullband_two_dish_fitted_beams
    else:
        fitted_beams = two_dish_fitted_beams
        assert (
            len(fitted_beams[1].get("SKA001"))
            == len(fitted_beams[0].get("SKA036"))
            == num_chunks
        )

    assert len(fitted_beams) == len(two_dish_ants)
    assert list(fitted_beams[1].keys()) + list(fitted_beams[0].keys()) == [
        antenna.name for antenna in two_dish_ants
    ]
    assert isinstance(fitted_beams[0], dict)
    assert isinstance(fitted_beams[1], dict)


@pytest.mark.parametrize("num_chunks", [1, 16])
def test_fit_to_gains(
    ants,
    num_chunks,
    fullband_fitted_beams_from_gains,
    fitted_beams_from_gains,
):
    """
    Unit test for fitting primary beams to gain amplitudes. Check the
    `fitted_offsets_weighted_average_gains` test for the fitted values.
    """
    if num_chunks == 1:
        fitted_beams = fullband_fitted_beams_from_gains
    else:
        fitted_beams = fitted_beams_from_gains
        for antenna in ants:
            beams_freq = fitted_beams.get(antenna.name)
            assert len(beams_freq) == num_chunks

    assert isinstance(fitted_beams, dict)
    assert list(fitted_beams.keys()) == [antenna.name for antenna in ants]
