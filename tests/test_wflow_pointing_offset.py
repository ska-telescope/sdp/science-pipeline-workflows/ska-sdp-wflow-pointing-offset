# pylint: disable=inconsistent-return-statements,too-many-arguments
# pylint: disable=too-many-locals,too-many-positional-arguments
"""Regression test for the pointing offset pipeline"""
import glob
import logging
import os
import tempfile
from unittest.mock import Mock, patch

import numpy
import pytest

from ska_sdp_wflow_pointing_offset.pointing_offset_cli import compute_offset
from ska_sdp_wflow_pointing_offset.pointing_pipeline_standalone import (
    StandAlonePipeline,
)
from tests.utils_three_dish import REFERENCE_COMMANDED_AZEL, REFERENCE_MJD

log = logging.getLogger("pointing-offset-logger")
log.setLevel(logging.WARNING)

DEFAULT_RUN = True
PERSIST = False


@patch(
    "ska_sdp_wflow_pointing_offset.pointing_pipeline_standalone.sorted",
    Mock(return_value=["some-ms", "other-ms"]),
)
@patch(
    "ska_sdp_wflow_pointing_offset.pointing_offset_cli.read_batch_visibilities"
)
@pytest.mark.parametrize("num_chunks", [1, 16])
@pytest.mark.parametrize("use_source_offset_column", [True, False])
@pytest.mark.parametrize(
    "enabled, mode, start_freq, end_freq",
    [
        (
            DEFAULT_RUN,
            "no_frequency_selection",
            None,
            None,
        ),
        (
            DEFAULT_RUN,
            "frequency_selection",
            8.562e8,
            8.567e8,
        ),
    ],
)
def test_wflow_pointing_offset(
    read_batch_visibilities,
    num_chunks,
    use_source_offset_column,
    enabled,
    mode,
    start_freq,
    end_freq,
    vis_array,
    on_sky_offsets_list,
    ants,
):
    """
    Main test routine.
    Note: Mock ms.
          Currently, we don't test cases with RFI mask applied,
          Please refer to the unit tests for apply_rfi_mask.

    :param read_batch_visibilities: Read visibilities and other
        parameters from each scan's measurement set
    :param num_chunks: Number of frequency chunks (integer)
    :param use_source_offset_column: Read on-sky offsets in
        cross-elevation and elevation from the SOURCE_OFFSET column
        of the pointing sub-tables? If False, antenna pointings in
        azimuth and elevation are read from the DIRECTION column of
        the pointing table and the on-sky offsets in cross-elevation
        and elevation are computed thereafter.
    :param enabled: Is this test enabled?
    :param mode: Which mode it is testing
    :param start_freq: Start frequency (Hz)
    :param end_freq: End frequency (Hz)
    :param vis_array: List of Visibility objects from all scans
    :param on_sky_offsets_list: List of the on-sky offsets from all
        scans in cross-elevation and elevation in degrees
    :param pointing_timestamps_mjd: Actual pointing timestamps in MJD
    :param ants: List of katpoint antennas
    """

    if not enabled:
        log.warning(
            "test_pointing_offset: test of %s mode is disabled, "
            "use enabled argument to change.",
            mode,
        )
        return True

    with tempfile.TemporaryDirectory() as tempdir:
        log.info("Putting output data into temporary %s.", tempdir)

        outfile = f"{tempdir}/test_pointing_offsets.txt"
        beamwidth_factor = [0.976, 1.098]
        thresh_width = 1.15

        read_batch_visibilities.return_value = (
            [vis_array],
            [on_sky_offsets_list],
            [ants],
            [REFERENCE_MJD],
            [REFERENCE_COMMANDED_AZEL],
            [[49.99992847442627] * len(on_sky_offsets_list[0])],
        )

        args = {
            "--start_freq": start_freq,
            "--end_freq": end_freq,
            "--apply_mask": False,
            "--rfi_file": None,
            "--num_chunks": num_chunks,
            "--fit_to_vis": False,
            "--fit_to_sep_pol": False,
            "--use_source_offset_column": use_source_offset_column,
            "--results_dir": tempdir,
            "--msdir": tempdir,
            "--bw_factor": True,
            "<bw_factor>": beamwidth_factor,
            "--thresh_width": thresh_width,
            "--use_modelvis": False,
        }

        pipeline = StandAlonePipeline(args)

        outplotfile = f"{tempdir}/test_"
        compute_offset(pipeline)
        assert os.path.exists(outfile)

        # We currently only check if the HDf5 file exists
        out_hdf = f"{tempdir}/test_pointing_offsets.hdf5"
        assert os.path.exists(out_hdf)

        read_out = numpy.loadtxt(outfile, delimiter=",", dtype=object)

        # Output data: Antenna name, Cross-el offset and uncertainty,
        # El offset and uncertainty, Expected width in H direction,
        # Expected width in V direction, Fitted width in H direction
        # and uncertainty, Fitted width in V direction and uncertainty,
        # and Fitted height and uncertainty
        assert read_out.shape == (3, 13)

        # Assert the right plots are outputted
        assert os.path.exists(outplotfile + "gain_amplitude.png")

        for i in range(3):
            assert os.path.exists(
                outplotfile + f"fitting_results_M00{i + 1}.png"
            )

        # If there are invalid fits, we do not generate a plot
        # of the pointing offsets since they are NaNs
        assert ~os.path.exists(outplotfile + "offsets.png")

        # If we need to save file to tests directory
        if PERSIST:
            log.info("Putting data into test_results directory.")
            test_dir = os.getcwd() + "/test_results"
            os.makedirs(test_dir, exist_ok=True)
            new_name = test_dir + "/pointing_offsets_" + f"{mode}" + ".txt"
            os.replace(outfile, new_name)
            new_hdf_name = (
                test_dir + "/pointing_offsets_" + f"{mode}" + ".hdf5"
            )
            os.replace(out_hdf, new_hdf_name)
            for plotname in glob.glob(f"{tempdir}" + "*.png"):
                new_plot_name = (
                    test_dir + f"/{mode}_" + os.path.basename(plotname)
                )
                os.replace(plotname, new_plot_name)
