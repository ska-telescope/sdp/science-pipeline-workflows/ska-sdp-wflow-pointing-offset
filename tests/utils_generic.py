"""
Function for checking data in JSON format
"""


def assert_json_data(expected_result, result):
    """Assert keys and values of data loaded from json."""
    for key, value in expected_result.items():
        if isinstance(value, dict):
            for sub_key, sub_value in value.items():
                assert (
                    result[key][sub_key] == sub_value
                ), f"result: {result[key][sub_key]}, expected: {sub_value}"
        else:
            assert (
                result[key] == value
            ), f"result: {result[key]}, expected: {value}"
