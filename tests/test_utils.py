"""
Utils function tests
"""

import logging
import os
from unittest.mock import patch

import numpy
import pytest

from ska_sdp_wflow_pointing_offset.utils import (
    DATA_STRUCTURE,
    antenna_and_offsets_reorder,
    average_output_offsets,
    construct_ms_file_name,
    convert_dict_to_numpy_array,
    convert_offsets_to_degrees_for_kafka,
    convert_to_structured,
    create_dict_for_output_offsets,
    create_empty_array,
    get_crosscorrelations,
    get_off_source_antenna_index,
    get_parameters_for_gaussian_fits_plot,
    handle_exception,
    merge_output_offsets,
    observing_band,
    select_channels,
    select_channels_and_split,
    split_array_into_chunks,
    weighted_avg_and_std,
)
from tests.conftest import NUM_CHUNKS
from tests.utils_generic import assert_json_data
from tests.utils_three_dish import (
    BASELINES,
    FREQS,
    NSCANS,
    OUTPUT_POINTING_OFFSETS,
    VIS_TIMESTAMPS_MJD,
)
from tests.utils_two_dish import (
    TWO_DISH_NANTS,
    TWO_DISH_ON_SKY_OFFSETS,
    TWO_DISH_OUTPUT_POINTING_OFFSETS,
    TWO_DISH_OUTPUT_POINTING_OFFSETS_FLIPPED,
    TWO_DISH_Y_PER_SCAN,
)

log = logging.getLogger("ska-sdp-pointing-offset")


@patch("glob.glob")
def test_construct_ms_file_name(mock_dir):
    """
    Unit test for construct_ms_file_name in src/utils.py
    """
    mock_dir.return_value = [
        "/product/eb-m001-20191031-12345/ska-sdp/ms-scan-0.ms",
        "/product/eb-m001-20191031-12345/ska-sdp/ms-scan-1.ms",
        "/product/eb-m001-20191031-12345/ska-sdp/ms-scan-2.ms",
    ]
    scan_ids = [0]
    test_dir = "/product/eb-m001-20191031-12345/ska-sdp/"
    files = construct_ms_file_name(test_dir, scan_ids)
    assert files == ["/product/eb-m001-20191031-12345/ska-sdp/ms-scan-0.ms"]


@patch("glob.glob")
def test_construct_ms_file_name_missing_files(mock_dir):
    """
    Function raises an error if not all Scan IDs can be
    associated with a MeasurementSet in the given directory
    """
    mock_dir.return_value = [
        "/product/eb-m001-20191031-12345/ska-sdp/ms-2-scan-1.ms",
        "/product/eb-m001-20191031-12345/ska-sdp/ms-2-scan-2.ms",
    ]
    scan_ids = [0]
    test_dir = "/product/eb-m001-20191031-12345/ska-sdp/"

    with pytest.raises(FileNotFoundError) as err:
        construct_ms_file_name(test_dir, scan_ids)

    assert "in the directory: [0]" in str(err)


def test_create_empty_array():
    """
    Test to check a correctly formatted array is sent to
    Kafka when an exception occurs during processing
    """
    antennas = [
        "SKA001",
        "SKA002",
        "SKA003",
        "SKA004",
        "SKA005",
        "SKA006",
        "SKA007",
    ]
    result = create_empty_array(antennas)

    assert isinstance(result, numpy.ndarray)
    assert result.shape == (7, 13)
    assert (result[:, 0] == numpy.array(antennas)).all()
    assert numpy.all(result[:, 1:] == "nan")


def test_handle_exception():
    """
    Test to check an exception during processing is handled
    correctly
    """
    scan_ids = [1, 2, 3, 4, 5]
    test_path = os.getcwd()

    try:
        # induce error
        # pylint: disable-next=consider-using-with
        open("non_existent_file.py", encoding="utf-8")

    except FileNotFoundError as err:
        result = handle_exception(err, err.__traceback__, scan_ids)
        # Omit the line entry since it changes
        result["traceback"].pop("line")

        expected_result = {
            "scan_ids": [1, 2, 3, 4, 5],
            "error": "FileNotFoundError: "
            "[Errno 2] No such file or directory: "
            "'non_existent_file.py'",
            "traceback": {
                "file": test_path + "/tests/test_utils.py",
                "function": "test_handle_exception",
            },
            "status": "failed",
        }
        assert result.keys() == expected_result.keys()
        assert_json_data(expected_result, result)


@pytest.mark.parametrize(
    "freq_array, expected_band",
    [
        (numpy.array([5.0e07, 7.0e07, 9.0e07]), "SKA1-Low"),
        (numpy.array([3.5e08, 4.0e08, 4.4e08]), "Band 1"),
        (numpy.array([1.3e09, 1.4e09, 1.5e09]), "Band 2"),
        (
            numpy.array(
                [
                    1.65e09,
                    1.74e09,
                    1.84e09,
                ]
            ),
            "Band 3",
        ),
        (
            numpy.array(
                [
                    2.8e09,
                    3.0e09,
                    3.1e09,
                ]
            ),
            "Band 4",
        ),
        (
            numpy.array(
                [
                    7.98e09,
                    8.24e09,
                    8.50e09,
                ]
            ),
            "Band 5a",
        ),
        (
            numpy.array(
                [
                    8.3e09,
                    8.8e09,
                    9.2e09,
                ]
            ),
            "Band 5b",
        ),
        (
            numpy.array(
                [
                    2.1e11,
                    2.2e11,
                    2.3e11,
                ]
            ),
            None,
        ),  # ALMA Band 6
    ],
)
def test_observing_band(freq_array, expected_band):
    """
    Unit test for function that determines the
    observing band based on input frequencies
    """
    result = observing_band(freq_array)
    assert result == expected_band


def test_convert_dict_to_numpy_array_fit_to_gains():
    """
    Test converting fitted parameters in a dictionary to a numpy array
    when the beams are fitted to the gain amplitudes
    """
    offsets = convert_dict_to_numpy_array(OUTPUT_POINTING_OFFSETS)

    assert (offsets[:, 0] == OUTPUT_POINTING_OFFSETS["antenna_names"]).all()

    assert (
        offsets[:, 1].astype(float) == OUTPUT_POINTING_OFFSETS["xel_offset"]
    ).all()

    assert (
        offsets[:, 2].astype(float)
        == OUTPUT_POINTING_OFFSETS["xel_offset_std"]
    ).all()

    assert (
        offsets[:, 3].astype(float) == OUTPUT_POINTING_OFFSETS["el_offset"]
    ).all()

    assert (
        offsets[:, 4].astype(float) == OUTPUT_POINTING_OFFSETS["el_offset_std"]
    ).all()

    assert (
        numpy.column_stack((offsets[:, 5], offsets[:, 6])).astype(float)
        == OUTPUT_POINTING_OFFSETS["expected_width"]
    ).all()

    assert (
        numpy.column_stack((offsets[:, 7], offsets[:, 9])).astype(float)
        == OUTPUT_POINTING_OFFSETS["fitted_width"]
    ).all()

    assert (
        numpy.column_stack((offsets[:, 8], offsets[:, 10])).astype(float)
        == OUTPUT_POINTING_OFFSETS["fitted_width_std"]
    ).all()

    assert (
        offsets[:, 11].astype(float)
        == OUTPUT_POINTING_OFFSETS["fitted_height"]
    ).all()

    assert (
        offsets[:, 12].astype(float)
        == OUTPUT_POINTING_OFFSETS["fitted_height_std"]
    ).all()


def test_convert_dict_to_numpy_array_fit_to_vis():
    """
    Test converting fitted parameters in a dictionary to a numpy array
    when the beams are fitted to the visibility amplitudes. This is
    the case when the antenna order match
    """
    offsets = convert_dict_to_numpy_array(TWO_DISH_OUTPUT_POINTING_OFFSETS)

    assert (
        offsets[:, 0] == TWO_DISH_OUTPUT_POINTING_OFFSETS["antenna_names"]
    ).all()

    assert (
        offsets[:, 1].astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["xel_offset"]
    ).all()

    assert (
        offsets[:, 2].astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["xel_offset_std"]
    ).all()

    assert (
        offsets[:, 3].astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["el_offset"]
    ).all()

    assert (
        offsets[:, 4].astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["el_offset_std"]
    ).all()

    assert (
        numpy.column_stack((offsets[:, 5], offsets[:, 6])).astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["expected_width"]
    ).all()

    assert (
        numpy.column_stack((offsets[:, 7], offsets[:, 9])).astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["fitted_width"]
    ).all()

    assert (
        numpy.column_stack((offsets[:, 8], offsets[:, 10])).astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["fitted_width_std"]
    ).all()

    assert (
        offsets[:, 11].astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["fitted_height"]
    ).all()

    assert (
        offsets[:, 12].astype(float)
        == TWO_DISH_OUTPUT_POINTING_OFFSETS["fitted_height_std"]
    ).all()


def test_antenna_and_offsets_reorder():
    """
    Test converting fitted parameters in a dictionary to a numpy array
    when the beams are fitted to the visibility amplitudes. This is
    the case when the antenna order does not match
    """
    offsets = antenna_and_offsets_reorder(
        TWO_DISH_OUTPUT_POINTING_OFFSETS_FLIPPED
    )

    assert (
        TWO_DISH_OUTPUT_POINTING_OFFSETS.get(key).all() == value.all()
        for key, value in offsets.items()
    )


def test_convert_offsets_to_degrees_for_kafka():
    """
    Test converting fitted parameters in a dictionary
    to numpy array in degrees for kafka
    """
    # Making a copy of the output pointing offsets
    output_offsets = OUTPUT_POINTING_OFFSETS.copy()

    offsets = convert_offsets_to_degrees_for_kafka(output_offsets)
    assert (offsets[:, 0] == OUTPUT_POINTING_OFFSETS["antenna_names"]).all()

    numpy.testing.assert_almost_equal(
        offsets[:, 1].astype(float),
        numpy.array([-0.02825083, -0.02120803, -0.05859639]),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        offsets[:, 2].astype(float),
        numpy.array([0.09633197, 0.09826398, 0.14194628]),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        offsets[:, 3].astype(float),
        numpy.array([-0.02227774, -0.01296603, -0.01760986]),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        offsets[:, 4].astype(float),
        numpy.array([0.02598822, 0.02986256, 0.03535665]),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        offsets[:, 5].astype(float),
        numpy.array([1.9145379, 1.91138319, 1.91295825]),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        offsets[:, 7].astype(float),
        numpy.array([2.09089545, 1.96133308, 2.09225222]),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        offsets[:, 8].astype(float),
        numpy.array([0.20252855, 0.20589181, 0.28985648]),
        decimal=6,
    )

    assert (
        offsets[:, 11].astype(float)
        == OUTPUT_POINTING_OFFSETS["fitted_height"]
    ).all()

    assert (
        offsets[:, 12].astype(float)
        == OUTPUT_POINTING_OFFSETS["fitted_height_std"]
    ).all()


def test_get_off_source_antenna_index():
    """
    Unit test for the function that extracts the index of
    the moving antenna in a two-dish observation scenario.
    """
    for i, on_sky_offset in enumerate(TWO_DISH_ON_SKY_OFFSETS):
        index = get_off_source_antenna_index(on_sky_offset)
        if i == 0:
            # In the first set of reference pointing observation,
            # the five-point scan is performed by the second antenna
            assert index == [1]
        else:
            # In the first set of reference pointing observation,
            # the five-point scan is performed by the first antenna
            assert index == [0]


def test_get_off_source_antenna_index_incorrect_on_sky_offsets(on_sky_offsets):
    """
    Unit test for the function that extracts the index of
    the moving antenna in a two-dish observation scenario.
    Here, we test the function raises an error when the
    on-sky offsets in a three-dish observation scenario
    is passed to it.
    """
    with pytest.raises(ValueError):
        get_off_source_antenna_index(on_sky_offsets)


@pytest.mark.parametrize("nants", [2, 3])
def test_create_dict_for_output_offsets(nants):
    """
    Unit test for function that creates a dictionary
    of the computed offsets filled with NaNs
    """
    result = create_dict_for_output_offsets(nants)

    for item in result.items():
        # Check all the values are NaNs since the
        # initialised values of the dictionary are NaNs
        assert numpy.isnan(item[1].astype(float)).all()

        # Check the output shapes
        if item[0] in (
            "antenna_names",
            "xel_offset",
            "xel_offset_std",
            "el_offset",
            "el_offset_std",
            "fitted_height",
            "fitted_height_std",
        ):
            assert item[1].shape == (nants,)
        else:
            assert item[0] in (
                "expected_width",
                "fitted_width",
                "fitted_width_std",
            )
            # expected/fitted width and their standard deviations
            assert item[1].shape == (nants, 2)


def test_merge_output_offsets():
    """
    Unit test for merge_output_offsets
    With valid values
    """
    offset1 = {}
    offset2 = {}
    offset3 = {}
    for key, value in OUTPUT_POINTING_OFFSETS.items():
        offset1[key] = value[0]
        offset2[key] = value[1]
        offset3[key] = value[2]

    merged_offsets = merge_output_offsets([offset1, offset2, offset3])

    # Assert two dicts are equal
    for key, value in merged_offsets.items():
        assert (OUTPUT_POINTING_OFFSETS[key] == value).all()


def test_merge_output_offsets_withnan():
    """
    Unit test for merge_output_offsets
    With NaN values
    """

    offset1 = {}
    offset2 = {}
    for key, value in OUTPUT_POINTING_OFFSETS.items():
        offset1[key] = value[0]
        offset2[key] = value[1]

    nanoffsets = create_dict_for_output_offsets(1)
    merged_with_nan = merge_output_offsets([offset1, offset2, nanoffsets])
    for key, value in merged_with_nan.items():
        assert numpy.isnan(value[2].astype(float)).all()


@pytest.mark.parametrize(
    "axis, expected_avg, expected_std",
    [
        ((0, 1), numpy.array(3.31707317), numpy.array(2.61565749)),
        (
            0,
            numpy.array([1.8, 2.40718563, 3.0, 5.81818182]),
            numpy.array([2.82842712, 2.82842712, 0.0, 2.82842712]),
        ),
        (
            1,
            numpy.array([2.18867925, 6.89820359]),
            numpy.array([1.38100892, 1.86728545]),
        ),
    ],
)
def test_weighted_avg_and_std_dev(axis, expected_avg, expected_std):
    """
    Unit test for weighted_avg_and_std function.
    """

    data = numpy.array([[1, 2, 3, 4], [5, 6, 7, 8]])
    weights = numpy.array([[2, 1.5, 0.6, 1.2], [0.5, 0.17, 0.0, 1.0]])

    average, std = weighted_avg_and_std(data, axis, weights)

    numpy.testing.assert_almost_equal(average, expected_avg)
    numpy.testing.assert_almost_equal(std, expected_std)


@pytest.mark.parametrize(
    "axis, expected_avg, expected_std",
    [
        ((0, 1), numpy.array(3.31707317), numpy.array(1.14620768)),
        (
            0,
            numpy.array([1.8, 2.40718563, 3.0, 5.81818182]),
            numpy.array([2.33238076, 2.55676703, 0.0, 2.00824746]),
        ),
        (
            1,
            numpy.array([2.18867925, 6.89820359]),
            numpy.array([0.73929634, 1.26448167]),
        ),
    ],
)
def test_weighted_avg_and_std_err(axis, expected_avg, expected_std):
    """
    Unit test for weighted_avg_and_std function.
    """

    data = numpy.array([[1, 2, 3, 4], [5, 6, 7, 8]])
    weights = numpy.array([[2, 1.5, 0.6, 1.2], [0.5, 0.17, 0.0, 1.0]])

    average, std = weighted_avg_and_std(data, axis, weights, std_error=True)

    numpy.testing.assert_almost_equal(average, expected_avg)
    numpy.testing.assert_almost_equal(std, expected_std)


@pytest.mark.parametrize(
    "axis, expected_shape",
    [
        ((0, 1, 2, 3), ()),
        ((0, 1, 2), (4,)),
        ((0, 1), (6, 4)),
        (0, (10, 6, 4)),
        (1, (3, 6, 4)),
        (3, (3, 10, 6)),
        ((1, 2), (3, 4)),
        ((2, 3), (3, 10)),
        ((1, 2, 3), (3,)),
    ],
)
def test_weighted_avg_and_std_shapes(axis, expected_shape):
    """
    Unit test for `weighted_avg_and_std` function to test data shapes.
    """

    data = numpy.ones([3, 10, 6, 4])
    weights = numpy.full(data.shape, 0.01)

    average, std = weighted_avg_and_std(data, axis, weights)

    assert average.shape == expected_shape
    assert average.shape == std.shape


def test_average_output_offsets():
    """
    Unit test for averaging output offsets from fitting to
    the parallel-hands polarisations gain amplitudes
    """
    fitted_parameters = average_output_offsets(
        [OUTPUT_POINTING_OFFSETS, OUTPUT_POINTING_OFFSETS]
    )

    # Check the number of keys
    assert len(list(fitted_parameters.keys())) == 10
    assert (
        fitted_parameters["antenna_names"]
        == OUTPUT_POINTING_OFFSETS["antenna_names"]
    ).all()

    numpy.testing.assert_almost_equal(
        fitted_parameters["xel_offset"],
        OUTPUT_POINTING_OFFSETS["xel_offset"],
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["el_offset"],
        OUTPUT_POINTING_OFFSETS["el_offset"],
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["expected_width"],
        OUTPUT_POINTING_OFFSETS["expected_width"],
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["fitted_width"],
        OUTPUT_POINTING_OFFSETS["fitted_width"],
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["fitted_height"],
        OUTPUT_POINTING_OFFSETS["fitted_height"],
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["xel_offset_std"], 0.0, decimal=6
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["el_offset_std"], 0.0, decimal=6
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["fitted_width_std"], 0.0, decimal=6
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters["fitted_height_std"], 0.0, decimal=6
    )


def test_convert_to_structured(pointing_numpy):
    """
    Test conversion of pointing offset data to numpy structured type
    """
    nants = pointing_numpy.shape[0]
    last_scan_index = 12345
    test = convert_to_structured(pointing_numpy, last_scan_index)
    assert test.dtype == DATA_STRUCTURE
    assert test.shape == (nants,)
    for ant in range(nants):
        assert test[ant]["antenna_name"] == "a" + str(ant + 1)
        assert test[ant]["last_scan_index"] == 12345
        assert tuple(test[ant])[2:14] == tuple(numpy.arange(0, 120, 10) / 10.0)


def test_convert_to_structured_invalid_data():
    """
    Test attempted conversion of data not in required format - it has 9 data
    values for each antenna
    """
    nants = 4
    ants = numpy.array(["a1", "a2", "a3", "a4"]).reshape(nants, 1)
    values = [numpy.arange(0, 90, 10) / 10.0] * nants
    data = numpy.column_stack((ants, values))
    last_scan_index = 12345
    with pytest.raises(ValueError) as excinfo:
        convert_to_structured(data, last_scan_index)
    assert "could not assign tuple of length 11" in str(excinfo.value)


def test_get_parameters_for_gaussian_fits_plot_fullband(
    two_dish_on_sky_offsets,
    fullband_two_dish_fitted_beams,
):
    """
    Test function that generates parameters for plotting Gaussian fits
    when beams are fitted to the visibility amplitudes (the two-dish mode)
    across the whole band.
    """
    y_per_scan_list = [
        TWO_DISH_Y_PER_SCAN.mean(axis=1),
        TWO_DISH_Y_PER_SCAN.mean(axis=1),
    ]
    fitted_beams_list = [
        fullband_two_dish_fitted_beams[0],
        fullband_two_dish_fitted_beams[1],
    ]
    x_per_scan, y_per_scan, merged_fitted_beams = (
        get_parameters_for_gaussian_fits_plot(
            two_dish_on_sky_offsets, y_per_scan_list, fitted_beams_list
        )
    )

    # Note that in the first set of observations, antenna 1 tracked the
    # source and antenna 2 performed a 5-point scan. In the second set of
    # observations, the antenna roles are swapped
    assert x_per_scan.shape == (NSCANS, TWO_DISH_NANTS, 2)
    assert (x_per_scan[:, 0] == two_dish_on_sky_offsets[0][:, 1]).all()
    assert (x_per_scan[:, 1] == two_dish_on_sky_offsets[1][:, 0]).all()

    assert y_per_scan.shape == (TWO_DISH_NANTS, NSCANS)
    assert (y_per_scan == TWO_DISH_Y_PER_SCAN.mean(axis=1)).all()

    assert isinstance(merged_fitted_beams, dict)
    assert list(merged_fitted_beams.keys()) == ["SKA036", "SKA001"]


def test_get_parameters_for_gaussian_fits_plot(
    two_dish_on_sky_offsets, two_dish_fitted_beams
):
    """
    Test function that generates parameters for plotting Gaussian
    fits when beams are fitted to the visibility amplitudes in
    multiple frequency chunks
    """
    y_per_scan_list = [TWO_DISH_Y_PER_SCAN, TWO_DISH_Y_PER_SCAN]
    fitted_beams_list = [
        two_dish_fitted_beams[0],
        two_dish_fitted_beams[1],
    ]
    x_per_scan, y_per_scan, merged_fitted_beams = (
        get_parameters_for_gaussian_fits_plot(
            two_dish_on_sky_offsets, y_per_scan_list, fitted_beams_list
        )
    )

    # Note that in the first set of observations, antenna 1 tracked the
    # source and antenna 2 performed a 5-point scan. In the second set of
    # observations, the antenna roles are swapped
    assert x_per_scan.shape == (NSCANS, TWO_DISH_NANTS, 2)
    assert (x_per_scan[:, 0] == two_dish_on_sky_offsets[0][:, 1]).all()
    assert (x_per_scan[:, 1] == two_dish_on_sky_offsets[1][:, 0]).all()

    assert y_per_scan.shape == (TWO_DISH_NANTS, NUM_CHUNKS, NSCANS)
    assert (y_per_scan[0] == TWO_DISH_Y_PER_SCAN).all()

    assert isinstance(merged_fitted_beams, dict)
    assert list(merged_fitted_beams.keys()) == ["SKA036", "SKA001"]


@pytest.mark.parametrize("nrec", [1, 2, 4])
def test_get_crosscorrelations(
    vis_linearpol, vis_array, total_intensity_vis, nrec
):
    """
    Unit test for extracting cross-correlation visibilities
    when fitting to the visibility amplitudes. The antenna
    pairs and visibility shapes before and after the cross-
    correlation visibilities are extracted and checked.
    len(BASELINES) refer to the number of baselines
    """
    # Check the vis objects for all three polarisation types
    # contain both auto and cross correlations
    assert (
        list(vis_linearpol.baselines.data)
        == list(vis_array[0][0].baselines.data)
        == list(total_intensity_vis.baselines.data)
        == [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 1),
            (1, 2),
            (2, 2),
        ]
    )

    assert (
        vis_linearpol.vis.data.shape
        and vis_linearpol.weight.data.shape
        and vis_linearpol.flags.data.shape
    ) == (len(VIS_TIMESTAMPS_MJD), len(BASELINES), len(FREQS), 4)

    assert (
        vis_array[0][0].vis.data.shape
        and vis_array[0][0].weight.data.shape
        and vis_array[0][0].flags.data.shape
    ) == (len(VIS_TIMESTAMPS_MJD), len(BASELINES), len(FREQS), 2)

    assert (
        total_intensity_vis.vis.data.shape
        and total_intensity_vis.weight.data.shape
        and total_intensity_vis.flags.data.shape
    ) == (len(VIS_TIMESTAMPS_MJD), len(BASELINES), len(FREQS), 1)

    # Extract the cross-correlations
    if nrec == 4:
        vis_linearpol = get_crosscorrelations([vis_linearpol])

        # Check the vis contains auto-correlations only
        assert list(vis_linearpol[0].baselines.data) == [
            (0, 1),
            (0, 2),
            (1, 2),
        ]

        # Check only the parallel-hands vis, weights, and flags are
        # extracted
        assert (
            vis_linearpol[0].vis.data.shape
            and vis_linearpol[0].weight.data.shape
            and vis_linearpol[0].flags.data.shape
            == (
                len(VIS_TIMESTAMPS_MJD),
                3,
                len(FREQS),
                2,
            )
        )

    elif nrec == 2:
        vis_linearnp = get_crosscorrelations(vis_array[0])

        # Check the vis contains cross-correlations only
        assert list(vis_linearnp[0].baselines.data) == [(0, 1), (0, 2), (1, 2)]

        # Check  the parallel-hands vis, weights, and flags are
        # kept
        assert (
            vis_linearnp[0].vis.data.shape
            and vis_linearnp[0].weight.data.shape
            and vis_linearnp[0].flags.data.shape
            == (
                len(VIS_TIMESTAMPS_MJD),
                3,
                len(FREQS),
                2,
            )
        )
    else:
        with pytest.raises(ValueError):
            # StokesI Visibility object is not supported
            get_crosscorrelations([total_intensity_vis])


@pytest.mark.parametrize("num_chunks", [3, 4, 9, 16])
def test_split_array_into_chunks(num_chunks):
    """
    Unit test for testing the function that splits an array
    into equal or unequal chunks
    """
    array = split_array_into_chunks(numpy.zeros(2064), num_chunks)
    if num_chunks == 3:
        assert array.shape == (1, 688)
    elif num_chunks == 4:
        assert array.shape == (2, 516)
    elif num_chunks == 9:
        assert len(array) == 7
        for i, _ in enumerate(array):
            if i in (0, 1):
                assert array[i].shape == (230,)
            else:
                assert array[i].shape == (229,)
    elif num_chunks == 16:
        assert array.shape == (14, 129)


def test_select_channels():
    """
    Unit test for select_channels
    """
    result_freqs, result_channels = select_channels(
        FREQS, numpy.arange(len(FREQS)), 1.3e9, 1.33e9
    )
    assert (
        result_freqs.all()
        == numpy.array(
            [
                1.32600000e09,
                1.32665079e09,
                1.32730159e09,
                1.32795238e09,
                1.32860317e09,
                1.32925397e09,
                1.32990476e09,
            ]
        ).all()
    )
    assert result_channels.all() == numpy.array([0, 1, 2, 3, 4, 5, 6]).all()


@pytest.mark.parametrize("num_chunks", [3, 7])
def test_select_channels_and_split(num_chunks):
    """
    Unit test for select_channels_and_split. We test equal and
    un-equal splitting
    """
    result_freqs, result_channels = select_channels_and_split(
        FREQS, numpy.arange(len(FREQS)), 1.3e9, 1.33e9, num_chunks
    )
    if num_chunks == 3:
        numpy.testing.assert_allclose(
            result_freqs,
            [numpy.array([1.32795238e09, 1.32860317e09])],
            rtol=1e-6,
        )
        assert (result_channels == numpy.array([3, 4])).all()
    else:
        numpy.testing.assert_allclose(
            result_freqs,
            numpy.array(
                [
                    [1.32665079e09],
                    [1.32730159e09],
                    [1.32795238e09],
                    [1.32860317e09],
                    [1.32925397e09],
                ]
            ),
            rtol=1e-6,
        )

        assert (
            result_channels == numpy.array([[1], [2], [3], [4], [5]])
        ).all()
