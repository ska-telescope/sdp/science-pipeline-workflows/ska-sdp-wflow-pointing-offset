"""
Unit test for plotting functions.
Currently test only asserts plotting is successful.
To further test, we need to assert number of plot calls.
"""

import logging
import os
import tempfile

import numpy
import pytest

from ska_sdp_wflow_pointing_offset.plotting.gain_amp_plot import plot_gain_amp
from ska_sdp_wflow_pointing_offset.plotting.gaussian_fits_plot import (
    GenerateGaussianFitsPlot,
    _check_amplitudes_dim,
    _discard_points_near_zero,
    _discard_points_near_zero_on_sky_offsets,
    _gauss_func,
)
from ska_sdp_wflow_pointing_offset.plotting.on_sky_offsets_plot import (
    plot_on_sky_offsets,
)
from ska_sdp_wflow_pointing_offset.plotting.pointing_offsets_plot import (
    plot_offsets,
)
from ska_sdp_wflow_pointing_offset.plotting.vis_amp_plot import plot_vis_amp
from tests.utils_three_dish import X_PER_SCAN
from tests.utils_two_dish import TWO_DISH_ON_SKY_OFFSETS, TWO_DISH_Y_PER_SCAN

logger = logging.getLogger("ska-sdp-pointing-offset")


@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_check_gain_amplitudes_dim(y_per_scan_gains, fit_to_sep_pol):
    """Test for function that checks the dimensions of the gain amplitudes"""
    with pytest.raises(ValueError):
        if fit_to_sep_pol:
            # Pass a 2D gain amplitude
            y_per_scan_gains = y_per_scan_gains[:, 10]
        else:
            # Pass a 4D gain amplitude
            y_per_scan_gains = numpy.expand_dims(
                y_per_scan_gains, axis=0
            ).repeat(2, axis=0)
        _check_amplitudes_dim(y_per_scan_gains, fit_to_sep_pol)


def test_check_vis_amplitudes_dim():
    """Test for function that checks the dimensions of the visibility
    amplitudes"""
    with pytest.raises(ValueError):
        # Pass a 4D gain amplitude
        _check_amplitudes_dim(
            numpy.expand_dims(TWO_DISH_Y_PER_SCAN, axis=0).repeat(2, axis=0)
        )


def test_plot_on_sky_offsets_vis_fitting(
    two_dish_on_sky_offsets, two_dish_ants
):
    """
    Test for on-sky-offsets plot
    """
    with tempfile.TemporaryDirectory() as tempdir:
        plot_on_sky_offsets(
            two_dish_on_sky_offsets, two_dish_ants, tempdir + "/"
        )

        assert os.path.exists(f"{tempdir}/on_sky_offsets.png")


def test_plot_on_sky_offsets_gain_fitting(on_sky_offsets):
    with tempfile.TemporaryDirectory() as tempdir:
        ants = None
        plot_on_sky_offsets(on_sky_offsets, ants, tempdir + "/")

        assert os.path.exists(f"{tempdir}/on_sky_offsets.png")


@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_plot_offsets_gain_fitting(pointing_table, fit_to_sep_pol):
    """
    Test for plot_offsets when beams are fitted to the gain
    amplitudes
    """
    fit_to_vis = False
    with tempfile.TemporaryDirectory() as tempdir:
        plot_offsets(
            pointing_table,
            ["XX", "XY", "YX", "YY"],
            fit_to_sep_pol,
            fit_to_vis,
            tempdir + "/",
        )
        assert os.path.exists(f"{tempdir}/offsets.png")


def test_plot_offsets_vis_fitting(two_dish_pointing_table):
    """
    Test for plot_offsets when beams are fitted to the visibility
    amplitudes
    """
    fit_to_vis = True
    fit_to_sep_pol = False
    with tempfile.TemporaryDirectory() as tempdir:
        plot_offsets(
            two_dish_pointing_table,
            ["XX", "XY", "YX", "YY"],
            fit_to_sep_pol,
            fit_to_vis,
            tempdir + "/",
        )
        assert os.path.exists(f"{tempdir}/offsets.png")


def test_plot_gain_amp_stokesI_3d(y_per_scan_gains):
    """
    Test for plot_gain_amp for 3D Stokes I gain amplitudes
    """
    with tempfile.TemporaryDirectory() as tempdir:
        plot_gain_amp(
            gain_sol=y_per_scan_gains,
            polarisation=["XX", "XY", "YX", "YY"],
            prefix=tempdir + "/",
        )

        assert os.path.exists(f"{tempdir}/gain_amplitude.png")


def test_plot_gain_amp_sep_stokes_4d(y_per_scan_gains):
    """
    Test for plot_gain_amp for 4D Stokes (XX and YY) or
    (RR, LL) gain amplitudes
    """
    with tempfile.TemporaryDirectory() as tempdir:
        plot_gain_amp(
            gain_sol=numpy.expand_dims(y_per_scan_gains, axis=0).repeat(
                2, axis=0
            ),
            polarisation=["XX", "XY", "YX", "YY"],
            fit_to_sep_pol=True,
            prefix=tempdir + "/",
        )

        assert os.path.exists(f"{tempdir}/gain_amplitude.png")


def test_plot_vis_amp():
    """
    Test for plot_vis_amp
    """

    with tempfile.TemporaryDirectory() as tempdir:
        plot_vis_amp(
            TWO_DISH_Y_PER_SCAN,
            tempdir + "/",
        )

        assert os.path.exists(f"{tempdir}/vis_amplitude.png")


def test_gauss_func(on_sky_offsets):
    """
    Unit test for the Gaussian distribution
    """
    amp = 1.0
    width = numpy.array([-0.1, 0.5])
    sigma = numpy.array([97.0, 110.0])

    results = _gauss_func(on_sky_offsets, amp, width, sigma)
    assert results.shape == on_sky_offsets.shape

    numpy.testing.assert_allclose(
        results,
        numpy.array(
            [
                [
                    [0.99999949, 0.9997217],
                    [0.99999949, 0.99972489],
                    [0.99999948, 0.99985563],
                    [0.99999949, 0.99972168],
                    [0.99999949, 0.99972478],
                ],
                [
                    [0.99999948, 0.99985554],
                    [0.9999995, 0.99984637],
                    [0.99999949, 0.99984863],
                    [0.99999948, 0.99994028],
                    [0.99999949, 0.99984629],
                ],
                [
                    [0.99999949, 0.99984861],
                    [0.99999948, 0.99994021],
                    [0.99999949, 0.99993419],
                    [0.99999948, 0.99993572],
                    [0.99999948, 0.99998811],
                ],
            ]
        ),
        rtol=1e-3,
    )


@pytest.mark.parametrize("fit_to_vis", [True, False])
def test_discard_points_near_zero_on_sky_offsets(y_per_scan_gains, fit_to_vis):
    """
    Unit test for the function that excludes zero amplitudes
    and amplitudes close to approximately zero on-sky offsets
    """
    if fit_to_vis:
        for i in range(TWO_DISH_ON_SKY_OFFSETS[0].shape[1]):
            (x_per_scan_xel, x_per_scan_el, y_per_scan_xel, y_per_scan_el) = (
                _discard_points_near_zero_on_sky_offsets(
                    TWO_DISH_ON_SKY_OFFSETS[0][:, i], TWO_DISH_Y_PER_SCAN[i, 0]
                )
            )

            # Before the zero data points are excluded
            assert TWO_DISH_ON_SKY_OFFSETS[0][:, i].shape == (5, 2)
            assert y_per_scan_gains[:, i].shape == (3, 5)

            # After the zero data points are excluded
            if i == 0:
                assert (
                    x_per_scan_xel.shape
                    == x_per_scan_el.shape
                    == y_per_scan_xel.shape
                    == y_per_scan_el.shape
                    == (1,)
                )
            else:
                assert x_per_scan_xel.shape == y_per_scan_xel.shape == (3,)
                assert x_per_scan_el.shape == y_per_scan_el.shape == (2,)
    else:
        for i in range(X_PER_SCAN.shape[1]):
            x_per_scan_xel, x_per_scan_el, y_per_scan_xel, y_per_scan_el = (
                _discard_points_near_zero_on_sky_offsets(
                    X_PER_SCAN[:, i], y_per_scan_gains[i, 0]
                )
            )

            # Before the zero data points are excluded
            assert X_PER_SCAN[:, i].shape == (5, 2)
            assert y_per_scan_gains[:, i].shape == (3, 5)

            # After the zero data points are excluded
            assert x_per_scan_xel.shape == (3,)
            assert x_per_scan_el.shape == (4,)
            assert y_per_scan_xel.shape == (3,)
            assert y_per_scan_el.shape == (4,)


@pytest.mark.parametrize("num_chunks", [1, 3, 16])
def test_discard_points_near_zero_gains(y_per_scan_gains, num_chunks):
    """Unit test for the function that excludes zero gain amplitudes"""
    for antenna_index in range(3):
        if num_chunks in (1, 3):
            if num_chunks == 1:
                (
                    updated_x_per_scan,
                    updated_y_per_scan,
                ) = _discard_points_near_zero(
                    X_PER_SCAN,
                    numpy.expand_dims(y_per_scan_gains[:, 1], axis=1),
                    antenna_index,
                )
            else:
                (
                    updated_x_per_scan,
                    updated_y_per_scan,
                ) = _discard_points_near_zero(
                    X_PER_SCAN, y_per_scan_gains[:, 1:4], antenna_index
                )
            # After the zero data points are excluded
            assert updated_x_per_scan.shape == (5, 2)
            assert updated_y_per_scan.shape == (5,)
        else:
            for chunk in range(num_chunks):
                (
                    updated_x_per_scan,
                    updated_y_per_scan,
                ) = _discard_points_near_zero(
                    X_PER_SCAN,
                    y_per_scan_gains[antenna_index, chunk],
                    antenna_index,
                )
                # After the zero data points are excluded
                assert updated_x_per_scan.shape == (5, 2)
                assert updated_y_per_scan.shape == (5,)


@pytest.mark.parametrize("num_chunks", [1, 3, 16])
def test_discard_points_near_zero_vis(num_chunks):
    """Unit test for the function that excludes zero visibility amplitudes"""
    for antenna_index in range(TWO_DISH_ON_SKY_OFFSETS[0].shape[1]):
        if num_chunks in (1, 3):
            if num_chunks == 1:
                (
                    updated_x_per_scan,
                    updated_y_per_scan,
                ) = _discard_points_near_zero(
                    TWO_DISH_ON_SKY_OFFSETS[0],
                    numpy.expand_dims(TWO_DISH_Y_PER_SCAN[:, 1], axis=1),
                    antenna_index,
                )
            else:
                (
                    updated_x_per_scan,
                    updated_y_per_scan,
                ) = _discard_points_near_zero(
                    TWO_DISH_ON_SKY_OFFSETS[0],
                    TWO_DISH_Y_PER_SCAN[:, 1:4],
                    antenna_index,
                )
            # After the zero data points are excluded
            assert updated_x_per_scan.shape == (5, 2)
            assert updated_y_per_scan.shape == (5,)
        else:
            for chunk in range(num_chunks):
                (
                    updated_x_per_scan,
                    updated_y_per_scan,
                ) = _discard_points_near_zero(
                    TWO_DISH_ON_SKY_OFFSETS[0],
                    TWO_DISH_Y_PER_SCAN[antenna_index, chunk],
                    antenna_index,
                )
                # After the zero data points are excluded
                assert updated_x_per_scan.shape == (5, 2)
                assert updated_y_per_scan.shape == (5,)


@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_plot_fits_gains_fullband(
    y_per_scan_gains,
    frequency_per_chunk,
    ants,
    fullband_fitted_beams_from_gains,
    fit_to_sep_pol,
):
    """
    Plots the Gaussian fits when the beams are fitted to the
    gain amplitudes across the whole frequency band
    """
    with tempfile.TemporaryDirectory() as tempdir:
        if fit_to_sep_pol:
            GenerateGaussianFitsPlot(
                X_PER_SCAN,
                numpy.expand_dims(
                    [y_per_scan_gains[:, 10], y_per_scan_gains[:, 10]], axis=2
                ),
                frequency_per_chunk[10],
                ants,
                [
                    fullband_fitted_beams_from_gains,
                    fullband_fitted_beams_from_gains,
                ],
                tempdir + "/",
            ).generate_parallel_hands_pol_amp_fits(["XX", "XY", "YX", "YY"])

            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M001.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M002.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M003.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M001.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M002.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M003.png"
            )
        else:
            GenerateGaussianFitsPlot(
                X_PER_SCAN,
                numpy.expand_dims(y_per_scan_gains[:, 10], axis=1),
                frequency_per_chunk[10],
                ants,
                fullband_fitted_beams_from_gains,
                tempdir + "/",
            ).generate_total_intensity_amp_fits()
            assert os.path.exists(f"{tempdir}/fitting_results_M001.png")
            assert os.path.exists(f"{tempdir}/fitting_results_M002.png")
            assert os.path.exists(f"{tempdir}/fitting_results_M003.png")


@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_plot_fits_gains_three_chunks(
    y_per_scan_gains,
    frequency_per_chunk,
    ants,
    fitted_beams_from_gains,
    fit_to_sep_pol,
):
    """
    Plots the Gaussian fits when the beams are fitted to the
    gain amplitudes in 3 frequency chunks
    """
    if fit_to_sep_pol:
        with tempfile.TemporaryDirectory() as tempdir:
            GenerateGaussianFitsPlot(
                X_PER_SCAN,
                numpy.array(
                    [y_per_scan_gains[:, 10:13], y_per_scan_gains[:, 10:13]]
                ),
                frequency_per_chunk[10:13],
                ants,
                [fitted_beams_from_gains, fitted_beams_from_gains],
                tempdir + "/",
            ).generate_parallel_hands_pol_amp_fits(["XX", "XY", "YX", "YY"])
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M001.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M002.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M003.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M001.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M002.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M003.png"
            )
    else:
        with tempfile.TemporaryDirectory() as tempdir:
            GenerateGaussianFitsPlot(
                X_PER_SCAN,
                y_per_scan_gains[:, 10:13],
                frequency_per_chunk[10:13],
                ants,
                fitted_beams_from_gains,
                tempdir + "/",
            ).generate_total_intensity_amp_fits()
            assert os.path.exists(f"{tempdir}/fitting_results_M001.png")
            assert os.path.exists(f"{tempdir}/fitting_results_M002.png")
            assert os.path.exists(f"{tempdir}/fitting_results_M003.png")


@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_plot_fits_gains_sixteen_chunks(
    y_per_scan_gains,
    frequency_per_chunk,
    ants,
    fitted_beams_from_gains,
    fit_to_sep_pol,
):
    """
    Plots the Gaussian fits when the beams are fitted to the
    gain amplitudes in 16 frequency chunks
    """
    if fit_to_sep_pol:
        with tempfile.TemporaryDirectory() as tempdir:
            GenerateGaussianFitsPlot(
                X_PER_SCAN,
                numpy.array((y_per_scan_gains, y_per_scan_gains)),
                frequency_per_chunk,
                ants,
                [fitted_beams_from_gains, fitted_beams_from_gains],
                tempdir + "/",
            ).generate_parallel_hands_pol_amp_fits(["XX", "XY", "YX", "YY"])
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M001.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M002.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesXX_M003.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M001.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M002.png"
            )
            assert os.path.exists(
                f"{tempdir}/fitting_results_stokesYY_M003.png"
            )
    else:
        with tempfile.TemporaryDirectory() as tempdir:
            GenerateGaussianFitsPlot(
                X_PER_SCAN,
                y_per_scan_gains,
                frequency_per_chunk,
                ants,
                fitted_beams_from_gains,
                tempdir + "/",
            ).generate_total_intensity_amp_fits()
            assert os.path.exists(f"{tempdir}/fitting_results_M001.png")
            assert os.path.exists(f"{tempdir}/fitting_results_M002.png")
            assert os.path.exists(f"{tempdir}/fitting_results_M003.png")


def test_plot_fits_vis_fullband(
    frequency_per_chunk,
    two_dish_ants,
    fullband_two_dish_merged_fitted_beams,
):
    """
    Plots the Gaussian fits when the beams are fitted to
    the visibility amplitudes across the whole frequency
    band
    """
    with tempfile.TemporaryDirectory() as tempdir:
        GenerateGaussianFitsPlot(
            TWO_DISH_ON_SKY_OFFSETS[0],
            numpy.expand_dims(TWO_DISH_Y_PER_SCAN[:, 10], axis=1),
            frequency_per_chunk[10],
            two_dish_ants,
            fullband_two_dish_merged_fitted_beams,
            tempdir + "/",
        ).generate_total_intensity_amp_fits()

        assert os.path.exists(f"{tempdir}/fitting_results_SKA001.png")
        assert os.path.exists(f"{tempdir}/fitting_results_SKA036.png")


def test_plot_fits_vis_three_chunks(
    frequency_per_chunk,
    two_dish_ants,
    two_dish_merged_fitted_beams,
):
    """
    Plots the Gaussian fits when the beams are fitted to
    the visibility amplitudes in 3 frequency chunks
    """
    with tempfile.TemporaryDirectory() as tempdir:
        GenerateGaussianFitsPlot(
            TWO_DISH_ON_SKY_OFFSETS[0],
            TWO_DISH_Y_PER_SCAN[:, 10:13],
            frequency_per_chunk[10:13],
            two_dish_ants,
            two_dish_merged_fitted_beams,
            tempdir + "/",
        ).generate_total_intensity_amp_fits()

        assert os.path.exists(f"{tempdir}/fitting_results_SKA001.png")
        assert os.path.exists(f"{tempdir}/fitting_results_SKA036.png")


def test_plot_fits_vis_sixteen_chunks(
    frequency_per_chunk,
    two_dish_ants,
    two_dish_merged_fitted_beams,
):
    """
    Plots the Gaussian fits when the beams are fitted to
    the visibility amplitudes in 16 frequency chunks
    """
    with tempfile.TemporaryDirectory() as tempdir:
        GenerateGaussianFitsPlot(
            TWO_DISH_ON_SKY_OFFSETS[0],
            TWO_DISH_Y_PER_SCAN,
            frequency_per_chunk,
            two_dish_ants,
            two_dish_merged_fitted_beams,
            tempdir + "/",
        ).generate_total_intensity_amp_fits()

        assert os.path.exists(f"{tempdir}/fitting_results_SKA001.png")
        assert os.path.exists(f"{tempdir}/fitting_results_SKA036.png")


def test_exception_handling(caplog):
    """
    Test exception handling of plotting functions
    with plot_offsets as an example
    """

    plot_offsets("random_str", "random_dir")

    # Assert error message
    caplog.set_level(logging.ERROR)
    err_msg = (
        "Plotting failed with error(s): "
        "'str' object has no attribute 'pointing'"
    )
    assert err_msg in caplog.text
