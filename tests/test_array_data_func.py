"""
Unit tests for frequency selection functions
"""

import logging

import numpy
import pytest

from ska_sdp_wflow_pointing_offset.array_data_func import (
    ExtractPerScan,
    _avg_amp,
    _check_matching_freqs,
    _check_obs_mask_freqs,
    _create_arrays_for_fitting,
    _remove_bad_channels,
    _select_mask_and_split,
    create_offset_pointing_table,
    fitted_offsets_weighted_average,
    interp_timestamps,
)
from ska_sdp_wflow_pointing_offset.utils import merge_output_offsets
from tests.utils_three_dish import (
    NANTS,
    NPOL,
    NSCANS,
    OUTPUT_POINTING_OFFSETS,
    POINTING_TIMESTAMPS_MJD,
    REFERENCE_COMMANDED_AZEL,
    REFERENCE_MJD,
    VIS_TIMESTAMPS_MJD,
    X_PER_SCAN,
)
from tests.utils_two_dish import TWO_DISH_ANTS, TWO_DISH_FREQS

log = logging.getLogger("ska-sdp-pointing-offset")


@pytest.mark.parametrize("indices", [numpy.array([]), numpy.array([0, 1, 9])])
def test_check_matching_freqs_nomatch(indices):
    """
    Checks when there is no match or more than two matches between observing
    in frequencies and static RFI mask frequencies
    """
    with pytest.raises(ValueError):
        _check_matching_freqs(indices)


def test_check_obs_mask_freqs_more_obs_freq_than_mask_array(
    frequency_per_chunk,
):
    """
    Check when observing frequencies are more than the frequencies of
    the static RFI mask. This is the case where the frequencies are
    split into equal chunks
    """
    obs_freq = frequency_per_chunk.reshape(4, -1)
    mask_freq = frequency_per_chunk[:8].reshape(4, -1)
    with pytest.raises(IndexError):
        _check_obs_mask_freqs(obs_freq, mask_freq)


def test_check_obs_mask_freqs_more_obs_freq_than_mask_list(
    frequency_per_chunk,
):
    """
    Check when observing frequencies are more than the frequencies of
    the static RFI mask. This is the case where the frequencies are
    split into non-equal chunks
    """
    obs_freq = numpy.array_split(frequency_per_chunk, 3)
    mask_freq = numpy.array_split(frequency_per_chunk[:8], 3)
    with pytest.raises(IndexError):
        _check_obs_mask_freqs(obs_freq, mask_freq)


def test_check_obs_mask_freqs_fewer_obs_freq_than_mask_array(
    frequency_per_chunk,
):
    """
    Check when observing frequencies are fewer than the frequencies of
    the static RFI mask. This is the case where the frequencies are
    split into equal chunks
    """
    obs_freq = numpy.array_split(frequency_per_chunk[:8], 4)
    mask_freq = numpy.array_split(frequency_per_chunk, 4)

    assert _check_obs_mask_freqs(obs_freq, mask_freq) is None


def test_check_obs_mask_freqs_fewer_obs_freq_than_mask_list(
    frequency_per_chunk,
):
    """
    Check when observing frequencies are fewer than the frequencies of
    the static RFI mask. This is the case where the frequencies are
    split into non-equal chunks
    """
    obs_freq = numpy.array_split(frequency_per_chunk[:8], 3)
    mask_freq = numpy.array_split(frequency_per_chunk, 3)
    assert _check_obs_mask_freqs(obs_freq, mask_freq) is None


def test_select_mask_and_split(frequency_per_chunk):
    """
    Unit test for function that selects static mask for user-defined
    frequency range and splits the mask and frequencies into chunks
    """
    result_rfi_mask, result_mask_freq = _select_mask_and_split(
        frequency_per_chunk,
        numpy.array([1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0]),
        1.0000e8,
        1.3600e9,
        5,
    )

    assert len(result_rfi_mask) == len(result_mask_freq) == 3

    assert (result_rfi_mask[0] == numpy.array([1, 0, 1])).all()
    assert (result_rfi_mask[1] == numpy.array([1, 0, 1])).all()
    assert (result_rfi_mask[2] == numpy.array([1, 0])).all()

    numpy.testing.assert_allclose(
        result_mask_freq[0],
        numpy.array([1.33478571e09, 1.33738889e09, 1.33999206e09]),
        rtol=1e-6,
    )
    numpy.testing.assert_allclose(
        result_mask_freq[1],
        numpy.array([1.34259524e09, 1.34519841e09, 1.34780159e09]),
        rtol=1e-6,
    )
    numpy.testing.assert_allclose(
        result_mask_freq[2],
        numpy.array([1.35040476e09, 1.35300794e09]),
        rtol=1e-6,
    )


def test_remove_bad_channels_equal_obs_mask_freqs():
    """
    Unit test for function that removes bad channels. This is the
    case where the observing and static RFI mask frequencies have
    equal length
    """
    obs_freqs = numpy.array(
        [
            [1326.00585938, 1326.21484375, 1326.42382812, 1326.6328125],
            [1326.84179688, 1327.05078125, 1327.25976562, 1327.46875],
            [1327.67773438, 1327.88671875, 1328.09570312, 1328.3046875],
            [1328.51367188, 1328.72265625, 1328.93164062, 1329.140625],
            [1329.34960938, 1329.55859375, 1329.76757812, 1329.9765625],
        ]
    )
    obs_channels = numpy.array(
        [
            [2249, 2250, 2251, 2252],
            [2253, 2254, 2255, 2256],
            [2257, 2258, 2259, 2260],
            [2261, 2262, 2263, 2264],
            [2265, 2266, 2267, 2268],
        ]
    )

    # The bad channels are at the end with mask labels 1
    rfi_mask = numpy.array(
        [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]
    )
    freq_per_chunk, chan_per_chunk = _remove_bad_channels(
        obs_freqs, obs_channels, obs_freqs, rfi_mask
    )

    assert (freq_per_chunk == obs_freqs[:4]).all()
    assert (chan_per_chunk == obs_channels[:4]).all()


def test_remove_bad_channels_unequal_obs_mask_freqs():
    """
    Unit test for function that removes bad channels. This is the
    case where the static RFI mask has more frequency channels than
    the observing frequencies
    """
    obs_freqs = numpy.array(
        [
            [1326.00585938, 1326.21484375],
            [1326.84179688, 1327.05078125],
            [1327.67773438, 1327.88671875],
            [1328.51367188, 1328.72265625],
            [1329.34960938, 1329.55859375],
        ]
    )
    obs_channels = numpy.array(
        [
            [2249, 2250],
            [2253, 2254],
            [2257, 2258],
            [2261, 2262],
            [2265, 2266],
        ]
    )
    mask_freqs = numpy.array(
        [
            [1326.00585938, 1326.21484375, 1326.42382812, 1326.6328125],
            [1326.84179688, 1327.05078125, 1327.25976562, 1327.46875],
            [1327.67773438, 1327.88671875, 1328.09570312, 1328.3046875],
            [1328.51367188, 1328.72265625, 1328.93164062, 1329.140625],
            [1329.34960938, 1329.55859375, 1329.76757812, 1329.9765625],
        ]
    )

    # The bad channels are at the end with mask labels 1
    rfi_mask = numpy.array(
        [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]
    )

    freq_per_chunk, chan_per_chunk = _remove_bad_channels(
        obs_freqs, obs_channels, mask_freqs, rfi_mask
    )

    numpy.testing.assert_allclose(
        freq_per_chunk[0],
        numpy.array([1326.00585938, 1326.21484375]),
        rtol=1e-6,
    )
    numpy.testing.assert_allclose(
        freq_per_chunk[1],
        numpy.array([1326.84179688, 1327.05078125]),
        rtol=1e-6,
    )
    numpy.testing.assert_allclose(
        freq_per_chunk[2],
        numpy.array([1327.67773438, 1327.88671875]),
        rtol=1e-6,
    )
    numpy.testing.assert_allclose(
        freq_per_chunk[3],
        numpy.array([1328.51367188, 1328.72265625]),
        rtol=1e-6,
    )

    assert (chan_per_chunk[0] == numpy.array([2249, 2250])).all()
    assert (chan_per_chunk[1] == numpy.array([2253, 2254])).all()
    assert (chan_per_chunk[2] == numpy.array([2257, 2258])).all()
    assert (chan_per_chunk[3] == numpy.array([2261, 2262])).all()


def test_interp_timestamps(on_sky_offsets, ants):
    """
    Unit test for interp_timestamps. Aligns the pointing and
    visibility datasets
    """
    on_sky_offsets_mjd = POINTING_TIMESTAMPS_MJD.reshape(
        len(ants), len(POINTING_TIMESTAMPS_MJD) // len(ants)
    )[0]
    interp_on_sky_offsets = interp_timestamps(
        on_sky_offsets, on_sky_offsets_mjd, VIS_TIMESTAMPS_MJD
    )

    # Check the difference between the input and interpolated
    # on-sky offsets
    assert interp_on_sky_offsets.shape == (3, 5, 2)
    assert (interp_on_sky_offsets - on_sky_offsets).all() == numpy.array(
        [
            [
                [1.36389313e-04, 1.33335973e00],
                [1.14795022e-05, 1.33352334e00],
                [-3.31205758e-04, 1.33287054e00],
            ],
            [
                [1.20693110e-05, 1.33328686e00],
                [-1.54975444e-04, 1.33364019e00],
                [-2.76368593e-04, 1.33320572e00],
            ],
            [
                [6.14735528e-04, 6.66660650e-01],
                [-1.63083117e-04, 6.67139080e-01],
                [4.49686800e-06, 6.66231690e-01],
            ],
            [
                [-3.22148111e-04, 6.66702820e-01],
                [2.49142762e-04, 6.66808780e-01],
                [-2.31034968e-04, 6.66342000e-01],
            ],
            [
                [0.00000000e00, 0.00000000e00],
                [0.00000000e00, 0.00000000e00],
                [0.00000000e00, 0.00000000e00],
            ],
        ]
    ).all()


@pytest.mark.parametrize("fit_to_vis", [True, False])
@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_avg_amp(fit_to_vis, fit_to_sep_pol):
    """
    Unit test for averaging of visibility or gain amplitudes in
    time only or in time and polarisation
    """
    data = numpy.array(
        [
            [
                [[0.785, 1.0], [0.675, 2.989]],
                [[0.745, 0.784], [0.888, 2.090]],
                [[0.999, 0.668], [0.478, 0.555]],
            ],
            [
                [[0.999, 1.0], [0.443, 1.0]],
                [[1.0, 0.779], [0.678, 0.987]],
                [[0.999, 1.798], [1.267, 1.908]],
            ],
        ]
    )
    weights = numpy.array(
        [
            [
                [[0.001, 1.0], [0.040, 0.001]],
                [[0.022, 0.346], [0.003, 0.001]],
                [[0.010, 0.002], [0.125, 0.231]],
            ],
            [
                [[0.016, 0.011], [0.011, 0.017]],
                [[0.043, 0.022], [0.023, 0.015]],
                [[0.017, 0.033], [0.001, 0.004]],
            ],
        ]
    )

    if fit_to_vis:
        # Fitting to visibility amplitudes with dimensions (ntimes, nants,
        # nchans, npols)
        wt_avg_vis, stddev_vis = _avg_amp(
            data, weights, fit_to_vis=True, fit_to_sep_pol=False
        )
        assert wt_avg_vis.shape == stddev_vis.shape == (3,)
        numpy.testing.assert_almost_equal(
            wt_avg_vis, numpy.array([0.984167, 0.806200, 0.672570]), decimal=6
        )
        numpy.testing.assert_almost_equal(
            stddev_vis, numpy.array([0.249041, 0.143251, 0.479963]), decimal=6
        )
    else:
        # Fitting to gain amplitudes with dimensions (ntimes, nants, npols)
        wt_avg_gains, stddev_gains = _avg_amp(
            data.mean(axis=2),
            weights.mean(axis=2),
            fit_to_vis=False,
            fit_to_sep_pol=fit_to_sep_pol,
        )
        if fit_to_sep_pol:
            assert wt_avg_gains.shape == stddev_gains.shape == (3, 2)
            numpy.testing.assert_almost_equal(
                wt_avg_gains,
                numpy.array(
                    [
                        [0.726426, 1.967439],
                        [0.832819, 1.383620],
                        [0.784912, 0.781631],
                    ]
                ),
                decimal=6,
            )

            numpy.testing.assert_almost_equal(
                stddev_gains,
                numpy.array(
                    [
                        [0.006364, 0.703218],
                        [0.015910, 0.391737],
                        [0.278954, 0.877873],
                    ]
                ),
                decimal=6,
            )

        else:
            assert wt_avg_gains.shape == stddev_gains.shape == (3,)
            numpy.testing.assert_almost_equal(
                wt_avg_gains,
                numpy.array([1.890512, 1.278098, 0.782818]),
                decimal=6,
            )
            numpy.testing.assert_almost_equal(
                stddev_gains,
                numpy.array([0.832334, 0.395697, 0.456920]),
                decimal=6,
            )


@pytest.mark.parametrize("num_chunks", [1, 16])
@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_create_arrays_for_fitting(num_chunks, fit_to_sep_pol):
    """
    Unit test for the function that creates empty
    arrays for collecting the parameters required for
    fitting the beams
    """
    y_per_scan, weights_per_scan, freq_per_chunk = _create_arrays_for_fitting(
        NSCANS, NANTS, num_chunks, fit_to_sep_pol
    )

    # Check the output shapes and whether all elements are zeros
    assert ~y_per_scan.all() == ~weights_per_scan.all()

    assert freq_per_chunk.size == num_chunks

    if num_chunks == 1:
        assert freq_per_chunk == numpy.zeros(num_chunks)
    else:
        assert (freq_per_chunk == numpy.zeros(num_chunks)).all()

    if fit_to_sep_pol:
        # Fitting to the parallel-hands gain amplitudes
        assert (
            y_per_scan.shape
            == weights_per_scan.shape
            == (NPOL, NANTS, num_chunks, NSCANS)
        )
    else:
        # Fitting to visibility amplitude or Stokes I gain amplitudes options
        assert (
            y_per_scan.shape
            == weights_per_scan.shape
            == (NANTS, num_chunks, NSCANS)
        )


def test_from_vis(two_dish_vis_array, two_dish_on_sky_offsets_list):
    """
    Unit test for extracting parameters from visibilities for fitting.
    These parameters are the commanded antenna pointings relative
    to the target, visibility amplitudes, weights for each scan, and
    frequencies
    """
    # There are 10 scans (5 scans per set of observation) with each scan
    # having the full-band visibilities i.e, num_chunks=1
    fit_to_sep_pol = False
    params_for_fitting = ExtractPerScan(
        two_dish_vis_array[:5],
        two_dish_on_sky_offsets_list[:5],
        TWO_DISH_ANTS,
        fit_to_sep_pol,
    ).from_vis()

    assert params_for_fitting[0].shape == (NSCANS, len(TWO_DISH_ANTS), 2)
    assert (
        params_for_fitting[1].shape
        == params_for_fitting[2].shape
        == (len(TWO_DISH_ANTS), 1, 5)
    )

    # We use the frequency at the higher end of the band for
    # better pointing accuracy
    assert params_for_fitting[3] == TWO_DISH_FREQS[-1]


@pytest.mark.parametrize("fit_to_sep_pol", [True, False])
def test_from_gains(
    vis_array,
    on_sky_offsets_list,
    ants,
    frequency_per_chunk,
    fit_to_sep_pol,
):
    """
    Unit test for extracting parameters from gains for fitting.
    These parameters are the commanded antenna pointings
    relative to the target, gain amplitudes, weights for each
    scan, and frequencies
    """
    # There are 5 scans with each scan having the full-band
    # visibilities i.e, num_chunks=1
    params_for_fitting = ExtractPerScan(
        vis_array,
        on_sky_offsets_list,
        ants,
        fit_to_sep_pol,
    ).from_gains()

    assert params_for_fitting[0].shape == X_PER_SCAN.shape
    if fit_to_sep_pol:
        assert (
            params_for_fitting[1].shape
            == params_for_fitting[2].shape
            == (NPOL, NANTS, 1, NSCANS)
        )
    else:
        assert (
            params_for_fitting[1].shape
            == params_for_fitting[2].shape
            == (NANTS, 1, 5)
        )

    assert params_for_fitting[3] == numpy.mean(frequency_per_chunk)


def test_fitted_offsets_weighted_average_fullband_gains(
    ants, fullband_fitted_beams_from_gains
):
    """
    Unit test for the fitted_offsets_weighted_average function when the
    beams are fitted to the gain amplitudes across the whole frequency
    band. The outputs of the function are the pointing offsets and other
    fitted parameters of interest. For each dish, nine set of parameters
    are generated.
    """
    fitted_parameters_gains = fitted_offsets_weighted_average(
        ants,
        fullband_fitted_beams_from_gains,
        fit_to_vis=False,
    )

    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["xel_offset"],
        numpy.array([5.149257e-04, 3.809170e-04, 6.345088e-04]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["xel_offset_std"],
        numpy.array([2.24412, 1.813196, 2.092332]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["el_offset"],
        numpy.array([-1.938090e-03, -1.992886e-03, -1.933418e-03]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["el_offset_std"],
        numpy.array([2.09327, 1.578328, 1.812873]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["expected_width"],
        numpy.array(
            [
                [2.239066e-02, 2.560931e-02],
                [2.239066e-02, 2.560931e-02],
                [2.239066e-02, 2.560931e-02],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_width"],
        numpy.array(
            [
                [2.431056e-02, 2.688286e-02],
                [2.463903e-02, 2.672609e-02],
                [2.438769e-02, 2.697285e-02],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_width_std"],
        numpy.array(
            [
                [17.338725, 18.299531],
                [14.223818, 13.73948],
                [15.647273, 15.883737],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_height"],
        numpy.array([2.798372, 2.881141, 2.766241]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_height_std"],
        numpy.array([501.131918, 393.722105, 426.090149]),
        decimal=6,
    )


def test_fitted_offsets_weighted_average_sixteen_chunks_gains(
    ants, fitted_beams_from_gains
):
    """
    Unit test for the fitted_offsets_weighted_average function when the beams
    are fitted to the gain amplitudes in 16 frequency chunks. The outputs
    of the function are the pointing offsets and other fitted parameters
    of interest. For each dish, nine set of parameters are generated.
    """
    fitted_parameters_gains = fitted_offsets_weighted_average(
        ants, fitted_beams_from_gains, fit_to_vis=False
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["xel_offset"],
        numpy.array([0.000479, 0.000384, 0.000453]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["xel_offset_std"],
        numpy.array([5.842320e-05, 8.977521e-05, 5.856534e-05]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["el_offset"],
        numpy.array([-0.002038, -0.002053, -0.001954]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["el_offset_std"],
        numpy.array([6.118740e-05, 8.645018e-05, 7.397598e-05]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["expected_width"],
        numpy.array(
            [
                [2.221926e-02, 2.541328e-02],
                [2.221926e-02, 2.541328e-02],
                [2.221926e-02, 2.541328e-02],
            ],
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_width"],
        numpy.array(
            [
                [0.023046, 0.024647],
                [0.022839, 0.024703],
                [0.022645, 0.024708],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_width_std"],
        numpy.array(
            [
                [0.000492, 0.000788],
                [0.000626, 0.000791],
                [0.000639, 0.000836],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_height"],
        numpy.array([2.30628, 2.608719, 2.356144]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_gains["fitted_height_std"],
        numpy.array([0.198954, 0.167535, 0.172679]),
        decimal=6,
    )


def test_fitted_offsets_weighted_average_fullband_vis(
    fullband_two_dish_fitted_beams,
):
    """
    Unit test for `fitted_offsets_weighted_average` function when the beams
    are fit to the visibility amplitudes which outputs the pointing
    offsets and other fitted parameters of interest. For each antenna,
    thirteen set of parameters are generated.
    """
    fitted_beams_list = [
        fitted_offsets_weighted_average(
            TWO_DISH_ANTS,
            fullband_two_dish_fitted_beams[0],
            fit_to_vis=True,
        ),
        fitted_offsets_weighted_average(
            TWO_DISH_ANTS,
            fullband_two_dish_fitted_beams[1],
            fit_to_vis=True,
        ),
    ]
    fitted_parameters_vis = merge_output_offsets(fitted_beams_list)
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["xel_offset"],
        numpy.array([-3.3937415e-05, -3.3937415e-05]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["xel_offset_std"],
        numpy.array([0.636857, 0.636857]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["el_offset"],
        numpy.array([-2.18170588e-05, -2.18170588e-05]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["el_offset_std"],
        numpy.array([0.631541, 0.631541]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["expected_width"],
        numpy.array(
            [
                [0.01351378, 0.01351378],
                [0.01351378, 0.01351378],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_width"],
        numpy.array(
            [
                [0.01267775, 0.01267755],
                [0.01267775, 0.01267755],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_width_std"],
        numpy.array(
            [
                [2.354241, 1.755322],
                [2.354241, 1.755322],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_height"],
        numpy.array([1.0, 1.0]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_height_std"],
        numpy.array([100.000285, 100.000285]),
        decimal=6,
    )


def test_fitted_offsets_weighted_average_sixteen_chunks_vis(
    two_dish_fitted_beams,
):
    """
    Unit test for the fitted_offsets_weighted_average function when the
    beams are fitted to the visibility amplitudes in 16 frequency chunks.
    The outputs of the function are the pointing offsets and other fitted
    parameters of interest. For each dish, nine set of parameters are
    generated.
    """
    fitted_beams_list = [
        fitted_offsets_weighted_average(
            TWO_DISH_ANTS,
            two_dish_fitted_beams[0],
            fit_to_vis=True,
        ),
        fitted_offsets_weighted_average(
            TWO_DISH_ANTS,
            two_dish_fitted_beams[1],
            fit_to_vis=True,
        ),
    ]
    fitted_parameters_vis = merge_output_offsets(fitted_beams_list)
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["xel_offset"],
        numpy.array([-3.3937415e-05, -3.3937415e-05]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["xel_offset_std"],
        numpy.array([4.440892e-16, 4.440892e-16]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["el_offset"],
        numpy.array([-2.18170588e-05, -2.18170588e-05]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["el_offset_std"],
        numpy.array([1.693135e-11, 1.693135e-11]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["expected_width"],
        numpy.array(
            [
                [0.013979, 0.013979],
                [0.013979, 0.013979],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_width"],
        numpy.array(
            [
                [0.01267775, 0.01267755],
                [0.01267775, 0.01267755],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_width_std"],
        numpy.array(
            [
                [7.505108e-14, 3.665646e-11],
                [7.505108e-14, 3.665646e-11],
            ]
        ),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_height"],
        numpy.array([1.0, 1.0]),
        decimal=6,
    )
    numpy.testing.assert_almost_equal(
        fitted_parameters_vis["fitted_height_std"],
        numpy.array([1.269734e-11, 1.269734e-11]),
        decimal=6,
    )


def test_create_offset_pointing_table(frequency_per_chunk, vis_array):
    """
    Unit test for create_offset_pointing_table which stores
    the fitted offsets and other parameters into the
    ska-sdp-datamodels PointingTable. All the fitted parameters
    have units of radians except the fitted height which is
    dimensionless
    """
    # Note that we only need the on-sky offsets for just one
    # antenna to extract the discrete offsets used in the
    # reference pointing observation
    x_per_scan = X_PER_SCAN[:, 0]
    track_duration = [5.0, 5.0, 5.0, 5.0, 5.0]  # for 5 scans
    pt_table = create_offset_pointing_table(
        OUTPUT_POINTING_OFFSETS,
        frequency_per_chunk,
        REFERENCE_MJD,
        REFERENCE_COMMANDED_AZEL,
        vis_array,
        x_per_scan,
        track_duration,
    )

    assert pt_table.frequency.data == 1.3465e9
    numpy.testing.assert_almost_equal(
        numpy.squeeze(pt_table.pointing.data),
        numpy.column_stack(
            (
                OUTPUT_POINTING_OFFSETS["xel_offset"],
                OUTPUT_POINTING_OFFSETS["el_offset"],
            )
        ),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        1.0 / numpy.sqrt(numpy.squeeze(pt_table.weight.data)),
        numpy.column_stack(
            (
                OUTPUT_POINTING_OFFSETS["xel_offset_std"],
                OUTPUT_POINTING_OFFSETS["el_offset_std"],
            )
        ),
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        numpy.squeeze(pt_table.expected_width.data),
        OUTPUT_POINTING_OFFSETS["expected_width"],
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        numpy.squeeze(pt_table.fitted_width.data),
        OUTPUT_POINTING_OFFSETS["fitted_width"],
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        numpy.squeeze(pt_table.fitted_width_std.data),
        OUTPUT_POINTING_OFFSETS["fitted_width_std"],
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        numpy.squeeze(pt_table.fitted_height.data),
        OUTPUT_POINTING_OFFSETS["fitted_height"],
        decimal=6,
    )

    numpy.testing.assert_almost_equal(
        numpy.squeeze(pt_table.fitted_height_std.data),
        OUTPUT_POINTING_OFFSETS["fitted_height_std"],
        decimal=6,
    )

    # Check observing band
    assert pt_table.band_type == "Band 2"
    assert pt_table.scan_mode == "5-point"
    assert pt_table.track_duration == 5.0
    numpy.testing.assert_almost_equal(
        pt_table.discrete_offset,
        numpy.array(
            [
                [-0.0, -0.47],
                [-0.0, 0.26],
                [-0.27, 0.09],
                [0.27, 0.15],
                [0.0, 0.0],
            ]
        ),
        decimal=6,
    )
