# pylint: disable=too-many-function-args
"""
Unit test for fitted pointing parameters saved
to text file
"""

import os
import tempfile

import numpy
import pytest

from ska_sdp_wflow_pointing_offset.export_data import (
    create_result_dir,
    export_pointing_offset_data,
)
from tests.utils_three_dish import OUTPUT_POINTING_OFFSETS
from tests.utils_two_dish import TWO_DISH_OUTPUT_POINTING_OFFSETS_FLIPPED


@pytest.mark.parametrize("fit_to_vis", [True, False])
def test_export_pointing_data_file(fit_to_vis, two_dish_vis_array, vis_array):
    """
    Unit test for exporting the fitted parameters to text file
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        filename = f"{temp_dir}/test_offset.csv"
        if fit_to_vis:
            export_pointing_offset_data(
                filename,
                TWO_DISH_OUTPUT_POINTING_OFFSETS_FLIPPED,
                two_dish_vis_array[0][0],
            )
            assert os.path.exists(filename)
            read_data_once = numpy.genfromtxt(filename, delimiter=",")
            assert read_data_once.shape[0] == len(
                TWO_DISH_OUTPUT_POINTING_OFFSETS_FLIPPED["antenna_names"]
            )
        else:
            export_pointing_offset_data(
                filename,
                OUTPUT_POINTING_OFFSETS,
                vis_array[0][0],
            )
            assert os.path.exists(filename)
            read_data_once = numpy.genfromtxt(filename, delimiter=",")
            assert read_data_once.shape[0] == len(
                OUTPUT_POINTING_OFFSETS["antenna_names"]
            )


def test_create_result_dir():
    """Test creating result directory based on the scan ids"""
    with tempfile.TemporaryDirectory() as tempdir:
        scan_ids = [1, 3, 5, 6, 7]
        os.makedirs(tempdir, exist_ok=True)

        result_dir = create_result_dir(tempdir, scan_ids)
        expected_dir = os.path.join(tempdir, "scan1-7")
        assert result_dir == expected_dir
