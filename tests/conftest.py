"""Pytest fixtures"""

import os
import tempfile
from unittest.mock import patch

import numpy
import pytest
from astropy.coordinates import SkyCoord
from ska_sdp_config import Config, entity
from ska_sdp_datamodels.calibration import PointingTable
from ska_sdp_datamodels.configuration.config_model import Configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    ReceptorFrame,
)
from ska_sdp_datamodels.visibility.vis_model import Visibility

from ska_sdp_wflow_pointing_offset import construct_antennas
from ska_sdp_wflow_pointing_offset.beam_fitting import SolveForOffsets
from ska_sdp_wflow_pointing_offset.export_data import create_metadata
from ska_sdp_wflow_pointing_offset.read_data import _get_pointing_unix
from ska_sdp_wflow_pointing_offset.utils import (
    get_parameters_for_gaussian_fits_plot,
    observing_band,
)
from tests.test_pointing_pipeline_sdp import EB_ID, PB_ID, SCRIPT
from tests.utils_three_dish import (
    ANTENNA_NAME,
    BASELINES,
    BEAMWIDTH_FACTOR,
    CHANNEL_BANDWIDTH,
    COMMANDED_POINTING_AZ,
    COMMANDED_POINTING_EL,
    DIAMETER,
    FLAGS,
    FREQS,
    GAINS_WEIGHTS_PER_SCAN,
    INTEGRATION_TIME,
    LINEAR_POLFRAME,
    LINEARNP_POLFRAME,
    LOCATION,
    MOUNT,
    NSCANS,
    OFFSET,
    ON_SKY_OFFSETS,
    OUTPUT_POINTING_OFFSETS,
    PHASECENTRE,
    POINTING_TIMESTAMPS_MJD,
    PREFIX,
    SOURCE_NAME,
    STOKESI_POLFRAME,
    TARGET,
    THRESH_WIDTH,
    UVW,
    VIS,
    VIS_TIMESTAMPS_MJD,
    VIS_WEIGHTS,
    X_PER_SCAN,
    XYZ,
    Y_PER_SCAN_GAINS,
)
from tests.utils_two_dish import (
    TWO_DISH_ANTENNA_NAME,
    TWO_DISH_ANTS,
    TWO_DISH_BASELINES,
    TWO_DISH_CHANNEL_BANDWIDTH,
    TWO_DISH_DIAMETER,
    TWO_DISH_FLAGS,
    TWO_DISH_FREQS,
    TWO_DISH_INTEGRATION_TIME,
    TWO_DISH_LINEARNP_POLFRAME,
    TWO_DISH_LOCATION,
    TWO_DISH_MOUNT,
    TWO_DISH_NSCANS,
    TWO_DISH_OFFSET,
    TWO_DISH_ON_SKY_OFFSETS,
    TWO_DISH_ON_SKY_OFFSETS_LIST,
    TWO_DISH_PHASECENTRE,
    TWO_DISH_SOURCE_NAME,
    TWO_DISH_UVW,
    TWO_DISH_VIS,
    TWO_DISH_VIS_TIMESTAMPS_MJD,
    TWO_DISH_VIS_WEIGHTS,
    TWO_DISH_WEIGHTS_PER_SCAN,
    TWO_DISH_XYZ,
    TWO_DISH_Y_PER_SCAN,
)

NUM_CHUNKS = 16


# ==============
#
# SHARED FIXTURES
#
# ==============
@pytest.fixture(name="total_intensity_vis")
def total_intensity_vis_fixture(configuration):
    """StokesI polarisation frame Visibility fixture"""
    return Visibility.constructor(
        frequency=FREQS,
        channel_bandwidth=CHANNEL_BANDWIDTH,
        phasecentre=PHASECENTRE,
        configuration=configuration,
        uvw=UVW,
        time=VIS_TIMESTAMPS_MJD,
        vis=VIS.mean(axis=3, keepdims=True),
        weight=VIS_WEIGHTS.mean(axis=3, keepdims=True),
        integration_time=INTEGRATION_TIME,
        flags=FLAGS.mean(axis=3, keepdims=True),
        baselines=BASELINES,
        polarisation_frame=STOKESI_POLFRAME,
        source=SOURCE_NAME,
        meta={"MSV2": {"FIELD_ID": 0, "DATA_DESC_ID": 0}},
    )


@pytest.fixture(name="vis_linearpol")
def linear_pol_vis_fixture(configuration):
    """Linear polarisation frame Visibility fixture"""
    return Visibility.constructor(
        frequency=FREQS,
        channel_bandwidth=CHANNEL_BANDWIDTH,
        phasecentre=PHASECENTRE,
        configuration=configuration,
        uvw=UVW,
        time=VIS_TIMESTAMPS_MJD,
        vis=VIS.repeat(2, axis=3),
        weight=VIS_WEIGHTS.repeat(2, axis=3),
        integration_time=INTEGRATION_TIME,
        flags=FLAGS.repeat(2, axis=3),
        baselines=BASELINES,
        polarisation_frame=LINEAR_POLFRAME,
        source=SOURCE_NAME,
        meta={"MSV2": {"FIELD_ID": 0, "DATA_DESC_ID": 0}},
    )


@pytest.fixture(name="frequency_per_chunk")
def freq_per_chunk_fixture():
    """The averaged-frequency per sub-band fixture"""
    return numpy.mean(FREQS.reshape(NUM_CHUNKS, -1), axis=1)


@pytest.fixture(name="observing_bands")
def observing_bands_fixture():
    """The averaged-frequency per sub-band fixture"""
    return numpy.mean(FREQS.reshape(NUM_CHUNKS, -1), axis=1)


# =====================
#
# 2-DISH MODE FIXTURES
#
# ======================
@pytest.fixture(name="two_dish_ants")
def two_dish_ants_fixture():
    """Antennas fixture for two-dish mode"""
    return construct_antennas(
        TWO_DISH_XYZ, TWO_DISH_DIAMETER, TWO_DISH_ANTENNA_NAME
    )


@pytest.fixture(name="two_dish_on_sky_offsets")
def two_dish_on_sky_offsets_fixture():
    """Two-dish on-sky offsets fixture"""
    return TWO_DISH_ON_SKY_OFFSETS


@pytest.fixture(name="two_dish_configuration")
def two_dish_configuration_fixture():
    """Configuration fixture for the two-dish observing scenario"""
    return Configuration.constructor(
        name="SKAMID",
        location=TWO_DISH_LOCATION,
        names=numpy.array(TWO_DISH_ANTENNA_NAME),
        xyz=TWO_DISH_XYZ,
        mount=TWO_DISH_MOUNT,
        frame="ITRF",
        receptor_frame=ReceptorFrame("linear"),
        diameter=TWO_DISH_DIAMETER,
        offset=TWO_DISH_OFFSET,
        stations=numpy.array(TWO_DISH_ANTENNA_NAME),
    )


@pytest.fixture(name="two_dish_vis_array")
def two_dish_vis_array_fixture(two_dish_configuration):
    """Visibility fixture for the two-dish observing scenario"""
    vis_list = []
    for scan_id in range(TWO_DISH_NSCANS):
        vis = Visibility.constructor(
            frequency=TWO_DISH_FREQS,
            channel_bandwidth=TWO_DISH_CHANNEL_BANDWIDTH,
            phasecentre=TWO_DISH_PHASECENTRE,
            configuration=two_dish_configuration,
            uvw=TWO_DISH_UVW[scan_id],
            time=TWO_DISH_VIS_TIMESTAMPS_MJD[scan_id],
            vis=TWO_DISH_VIS[scan_id],
            weight=TWO_DISH_VIS_WEIGHTS[scan_id],
            integration_time=TWO_DISH_INTEGRATION_TIME[scan_id],
            flags=TWO_DISH_FLAGS[scan_id],
            baselines=TWO_DISH_BASELINES,
            polarisation_frame=TWO_DISH_LINEARNP_POLFRAME,
            source=TWO_DISH_SOURCE_NAME,
            meta={"MSV2": {"FIELD_ID": 0, "DATA_DESC_ID": 0}},
        )
        vis_list.append([vis])
    return vis_list


@pytest.fixture(name="two_dish_on_sky_offsets_list")
def two_dish_on_sky_offsets_list_fixture():
    """
    On-sky offsets fixture extracted from all scans. This is the list
    of commanded pointings relative to the target in cross-elevation
    and elevation
    """
    offset_list = []
    for onescan_offsets in TWO_DISH_ON_SKY_OFFSETS_LIST:
        on_sky_offsets = numpy.squeeze(onescan_offsets)
        on_sky_offsets = numpy.degrees(
            on_sky_offsets.reshape(
                len(TWO_DISH_ANTS),
                on_sky_offsets.shape[0] // len(TWO_DISH_ANTS),
                2,
            )
        )
        offset_list.append(on_sky_offsets)
    return offset_list


@pytest.fixture(name="fullband_two_dish_fitted_beams")
def two_dish_fitted_beams_fullband_fixture():
    """Fitted beams from visibility amplitudes over the whole
    frequency band fixture"""
    beamwidth_factor = [0.95, 0.95]

    fitted_beams = []
    for x_per_scan in TWO_DISH_ON_SKY_OFFSETS:
        beams = SolveForOffsets(
            x_per_scan,
            numpy.expand_dims(TWO_DISH_Y_PER_SCAN[:, 10], axis=1),
            numpy.expand_dims(TWO_DISH_WEIGHTS_PER_SCAN[:, 10], axis=1),
            TWO_DISH_FREQS[-1].ravel(),
            beamwidth_factor,
            TWO_DISH_ANTS,
            THRESH_WIDTH,
        ).fit_to_vis()
        fitted_beams.append(beams)

    return fitted_beams


@pytest.fixture(name="two_dish_fitted_beams")
def two_dish_fitted_beams_fixture(frequency_per_chunk):
    """Fitted beams from visibility amplitudes over 16
    frequency chunks fixture"""
    beamwidth_factor = [0.95, 0.95]

    fitted_beams = []
    for x_per_scan in TWO_DISH_ON_SKY_OFFSETS:
        beams = SolveForOffsets(
            x_per_scan,
            TWO_DISH_Y_PER_SCAN,
            TWO_DISH_WEIGHTS_PER_SCAN,
            frequency_per_chunk,
            beamwidth_factor,
            TWO_DISH_ANTS,
            THRESH_WIDTH,
        ).fit_to_vis()
        fitted_beams.append(beams)

    return fitted_beams


@pytest.fixture(name="fullband_two_dish_merged_fitted_beams")
def two_dish_merged_fitted_beams_fullband_fixture(
    fullband_two_dish_fitted_beams,
):
    """Merged fitted beams from visibility amplitudes over the whole
    frequency band fixture"""
    return get_parameters_for_gaussian_fits_plot(
        TWO_DISH_ON_SKY_OFFSETS,
        TWO_DISH_Y_PER_SCAN,
        fullband_two_dish_fitted_beams,
    )[2]


@pytest.fixture(name="two_dish_merged_fitted_beams")
def two_dish_merged_fitted_beams_fixture(two_dish_fitted_beams):
    """Merged fitted beams from visibility amplitudes over 16
    frequency chunks fixture"""
    return get_parameters_for_gaussian_fits_plot(
        TWO_DISH_ON_SKY_OFFSETS,
        TWO_DISH_Y_PER_SCAN,
        two_dish_fitted_beams,
    )[2]


@pytest.fixture(name="two_dish_pointing_table")
def two_dish_pointing_table_fixture(pointing_table):
    """The PointingTable fixture for the two-dish mode"""

    return PointingTable.constructor(
        pointing=pointing_table.pointing.data.repeat(2, axis=0),
        expected_width=pointing_table.expected_width.data.repeat(2, axis=0),
        fitted_width=pointing_table.fitted_width.data.repeat(2, axis=0),
        fitted_width_std=pointing_table.fitted_width_std.data.repeat(
            2, axis=0
        ),
        fitted_height=pointing_table.fitted_height.data.repeat(2, axis=0),
        fitted_height_std=pointing_table.fitted_height_std.data.repeat(
            2, axis=0
        ),
        time=pointing_table.time.data.repeat(2, axis=0),
        weight=pointing_table.weight.data.repeat(2, axis=0),
        frequency=pointing_table.frequency.data,
        receptor_frame=ReceptorFrame("stokesI"),
        pointing_frame="xel-el",
        band_type=pointing_table.band_type,
        scan_mode=pointing_table.scan_mode,
        track_duration=pointing_table.track_duration,
        discrete_offset=pointing_table.discrete_offset.data,
        pointingcentre=pointing_table.pointingcentre.data,
        configuration=pointing_table.configuration,
    )


# =====================
#
# 3+ DISH MODE FIXTURES
#
# ======================
@pytest.fixture(name="ants")
def ants_fixture():
    """Antennas fixture"""
    return construct_antennas(XYZ, DIAMETER, ANTENNA_NAME)


@pytest.fixture(name="configuration")
def configuration_fixture():
    """Configuration fixture"""
    return Configuration.constructor(
        name="SKAMID",
        location=LOCATION,
        names=numpy.array(ANTENNA_NAME),
        xyz=XYZ,
        mount=MOUNT,
        frame="ITRF",
        receptor_frame=ReceptorFrame("linear"),
        diameter=DIAMETER,
        offset=OFFSET,
        stations=numpy.array(ANTENNA_NAME),
    )


@pytest.fixture(name="vis_array")
def vis_array_fixture(configuration):
    """Visibility fixture"""
    vis_list = []
    for _ in range(NSCANS):
        vis = Visibility.constructor(
            frequency=FREQS,
            channel_bandwidth=CHANNEL_BANDWIDTH,
            phasecentre=PHASECENTRE,
            configuration=configuration,
            uvw=UVW,
            time=VIS_TIMESTAMPS_MJD,
            vis=VIS,
            weight=VIS_WEIGHTS,
            integration_time=INTEGRATION_TIME,
            flags=FLAGS,
            baselines=BASELINES,
            polarisation_frame=LINEARNP_POLFRAME,
            source=SOURCE_NAME,
            meta={"MSV2": {"FIELD_ID": 0, "DATA_DESC_ID": 0}},
        )
        vis_list.append([vis])
    return vis_list


@pytest.fixture(name="pointing_timestamps_mjd")
def on_sky_offsets_timestamps_fixture(ants):
    """On-sky offsets timestamps fixture"""
    on_sky_offsets_timestamps = numpy.zeros(
        (NSCANS, len(POINTING_TIMESTAMPS_MJD) // len(ants))
    )

    pointing_times = POINTING_TIMESTAMPS_MJD.reshape(
        len(ants), len(POINTING_TIMESTAMPS_MJD) // len(ants)
    )[0]

    for i in range(NSCANS):
        pointing_times += INTEGRATION_TIME
        on_sky_offsets_timestamps[i] = pointing_times

    return on_sky_offsets_timestamps


@pytest.fixture(name="on_sky_offsets")
def on_sky_offsets_fixture(
    ants, pointing_timestamps_mjd, use_source_offset_column=False
):
    """
    On-sky offsets fixture. This is the commanded pointing
    relative to the target in cross-elevation and elevation
    for a single scan.
    """
    if use_source_offset_column:
        # Read commanded pointings relative to target
        # from SOURCE_OFFSET column of the pointing table
        on_sky_offsets = numpy.squeeze(ON_SKY_OFFSETS)
        on_sky_offsets = numpy.degrees(
            on_sky_offsets.reshape(
                len(ants), on_sky_offsets.shape[0] // len(ants), 2
            )
        )
    else:
        # Read commanded antenna pointings from the TARGET
        # column of the pointing table
        pointing_time_unix = _get_pointing_unix(pointing_timestamps_mjd[0])
        commanded_azel = numpy.squeeze(
            numpy.dstack((COMMANDED_POINTING_AZ, COMMANDED_POINTING_EL))
        )
        commanded_azel = numpy.degrees(
            commanded_azel.reshape(
                len(ants), commanded_azel.shape[0] // len(ants), 2
            )
        )
        on_sky_offsets = numpy.zeros(commanded_azel.shape)
        for i, antenna in enumerate(ants):
            # Compute relative azel
            target_azel = numpy.degrees(
                TARGET.azel(pointing_time_unix, antenna)
            )
            target_pos = SkyCoord(
                az=target_azel[0],
                alt=target_azel[1],
                unit="deg",
                frame="altaz",
            )
            commanded_pos = SkyCoord(
                az=commanded_azel[i, :, 0],
                alt=commanded_azel[i, :, 1],
                unit="deg",
                frame="altaz",
            )
            dcross_el = (commanded_pos.az.deg - target_pos.az.deg) * numpy.cos(
                target_pos.alt.rad
            )
            delev = commanded_pos.alt.deg - target_pos.alt.deg

            on_sky_offsets[i] = numpy.column_stack((dcross_el, delev))

    return on_sky_offsets


@pytest.fixture(name="on_sky_offsets_list")
def _on_sky_offsets_list_fixture(on_sky_offsets):
    """
    On-sky offsets fixture extracted from all scans. This is the list
    of commanded pointings relative to the target in cross-elevation
    and elevation
    """
    return [
        on_sky_offsets,
        on_sky_offsets,
        on_sky_offsets,
        on_sky_offsets,
        on_sky_offsets,
    ]


@pytest.fixture(name="y_per_scan_gains")
def y_per_scan_gains_fixture():
    """The gain amplitudes of all antennas for each scan fixture"""
    return Y_PER_SCAN_GAINS


@pytest.fixture(name="gains_weights_per_scan")
def weights_per_scan_fixture():
    """The weights of all antennas for each scan fixture"""
    return GAINS_WEIGHTS_PER_SCAN


@pytest.fixture(name="fullband_fitted_beams_from_gains")
def fitted_beams_from_gains_for_fullband_fixture(
    y_per_scan_gains, gains_weights_per_scan, ants
):
    """Fitted beams from gain amplitudes over the fullband fixture"""
    return SolveForOffsets(
        X_PER_SCAN,
        numpy.expand_dims(y_per_scan_gains[:, 10], axis=1),
        numpy.expand_dims(gains_weights_per_scan[:, 10], axis=1),
        numpy.mean(FREQS).ravel(),
        BEAMWIDTH_FACTOR,
        ants,
        THRESH_WIDTH,
    ).fit_to_gains()


@pytest.fixture(name="fitted_beams_from_gains")
def fitted_beams_from_gains_fixture(
    y_per_scan_gains,
    gains_weights_per_scan,
    frequency_per_chunk,
    ants,
):
    """Fitted beams from gain amplitudes fixture"""
    return SolveForOffsets(
        X_PER_SCAN,
        y_per_scan_gains,
        gains_weights_per_scan,
        frequency_per_chunk,
        BEAMWIDTH_FACTOR,
        ants,
        THRESH_WIDTH,
    ).fit_to_gains()


@pytest.fixture(name="pointing_table")
def total_intensity_pointing_table_fixture(
    vis_array,
    pointing_timestamps_mjd,
):
    """The PointingTable fixture for the three-dish mode"""

    freqs = numpy.mean(FREQS).ravel()
    time = numpy.median(pointing_timestamps_mjd[0]).ravel()

    # Extract the cross-elevation and elevation offsets
    xel_el_offsets = numpy.column_stack(
        (
            OUTPUT_POINTING_OFFSETS["xel_offset"],
            OUTPUT_POINTING_OFFSETS["el_offset"],
        )
    )
    xel_el_offsets = xel_el_offsets[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis, :
    ]

    # Extract the uncertainties on the cross-elevation and
    # elevation offsets
    xel_el_offsets_std = numpy.column_stack(
        (
            OUTPUT_POINTING_OFFSETS["xel_offset_std"],
            OUTPUT_POINTING_OFFSETS["el_offset_std"],
        )
    )
    xel_el_offsets_std = xel_el_offsets_std[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis, :
    ]

    # Extract the expected width
    expected_width = OUTPUT_POINTING_OFFSETS["expected_width"]
    expected_width = expected_width[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis, :
    ]

    # Extract the fitted width
    gaussian_width = OUTPUT_POINTING_OFFSETS["fitted_width"]
    gaussian_width = gaussian_width[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis, :
    ]

    # Extract the uncertainties on the fitted width
    gaussian_width_std = OUTPUT_POINTING_OFFSETS["fitted_width_std"]
    gaussian_width_std = gaussian_width_std[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis, :
    ]

    # Extract the fitted height
    gaussian_height = OUTPUT_POINTING_OFFSETS["fitted_height"]
    gaussian_height = gaussian_height[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis
    ]

    # Extract the uncertainties on the fitted height
    gaussian_height_std = OUTPUT_POINTING_OFFSETS["fitted_height_std"]
    gaussian_height_std = gaussian_height_std[
        numpy.newaxis, :, numpy.newaxis, numpy.newaxis
    ]

    return PointingTable.constructor(
        pointing=xel_el_offsets,
        expected_width=expected_width,
        fitted_width=gaussian_width,
        fitted_width_std=gaussian_width_std,
        fitted_height=gaussian_height,
        fitted_height_std=gaussian_height_std,
        time=time,
        weight=1 / xel_el_offsets_std**2,
        frequency=freqs,
        receptor_frame=ReceptorFrame("stokesI"),
        pointing_frame="xel-el",
        band_type=observing_band(freqs),
        scan_mode=f"{X_PER_SCAN.shape[0]}-point",
        track_duration=None,
        discrete_offset=numpy.radians(
            numpy.unique(numpy.round(X_PER_SCAN, 2))
        ),
        pointingcentre=vis_array[0][0].phasecentre,
        configuration=vis_array[0][0].configuration,
    )


# ==============
#
# SDP FIXTURES
#
# ==============
@pytest.fixture(scope="function", name="cfg")
def cfg_fix():
    """
    Fixture to set up configuration.
    """

    host = os.getenv("SDP_TEST_HOST", "127.0.0.1")
    port = os.getenv("SDP_CONFIG_PORT", "2379")
    with Config(
        global_prefix=PREFIX, host=host, port=port, backend="etcd3"
    ) as config:
        config.backend.delete(PREFIX, must_exist=False, recursive=True)
        config.backend.delete("/pb", must_exist=False, recursive=True)
        config.backend.delete("/eb", must_exist=False, recursive=True)

        # define eb and state
        eb = entity.ExecutionBlock(key=EB_ID)
        eb_state = {
            "scan_type": None,
            "scan_id": None,
            "scans": [],
            "status": "ACTIVE",
        }

        # define script
        script_image = {"image": f"{SCRIPT['name']}:{SCRIPT['version']}"}
        script_key = entity.Script.Key(**SCRIPT)
        script = entity.Script(key=script_key, **script_image)

        # define pb and state
        processing_block = entity.ProcessingBlock(
            key=PB_ID, eb_id=EB_ID, script=script_key
        )
        pb_state = {"status": "RUNNING"}

        # generate PB and EB in Config DB for metadata code to use
        for txn in config.txn():
            txn.execution_block.create(eb)
            txn.execution_block.state(EB_ID).create(eb_state)

            txn.processing_block.create(processing_block)
            txn.processing_block.state(PB_ID).create(pb_state)
            txn.script.create(script)

        yield config
        config.backend.delete(PREFIX, must_exist=False, recursive=True)
        config.backend.delete("/pb", must_exist=False, recursive=True)
        config.backend.delete("/eb", must_exist=False, recursive=True)


@pytest.fixture(name="metadata_object")
def create_metadata_fixture(cfg):
    """Test generating an initial metadata file with data"""

    with patch(
        "ska_sdp_dataproduct_metadata.metadata.new_config_client"
    ) as mock_config, patch.dict(os.environ, {"SDP_PB_ID": PB_ID}, clear=True):
        mock_config.return_value = cfg
        with tempfile.TemporaryDirectory() as temp_dir:
            filename = f"{temp_dir}/test-offset-metadata.yaml"
            metadata = create_metadata(filename)
            yield metadata, filename


@pytest.fixture(name="pointing_numpy")
def pointing_numpy_fixture():
    """Generate a pointing numpy array"""

    nants = 4
    ants = numpy.array(["a1", "a2", "a3", "a4"]).reshape(nants, 1)
    values = [numpy.arange(0, 120, 10) / 10.0] * nants
    data = numpy.column_stack((ants, values))

    return data
