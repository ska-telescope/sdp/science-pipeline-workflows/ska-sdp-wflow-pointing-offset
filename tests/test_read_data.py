# pylint: disable=import-error,import-outside-toplevel

"""
Unit Tests to read data
from CASA Measurement Tables
"""

import glob
import logging
import os
import tempfile
from unittest.mock import patch

import numpy
import pytest
from casacore.tables import makescacoldesc, maketabdesc, table

from ska_sdp_wflow_pointing_offset.read_data import (
    _calc_on_sky_offsets_from_target_column,
    _calc_track_duration,
    _fix_antenna_timestamp_pointing_table,
    _get_middle_timestamp,
    _get_pointing_unix,
    _split_two_dish_modes_ms,
    read_batch_visibilities,
)
from tests.utils_three_dish import (
    CORR_TYPE,
    FREQS,
    MockDataDescriptionTable,
    MockPointingTable,
    MockSpectralWindowTable,
)

casacore = pytest.importorskip("casacore")

log = logging.getLogger("ska-sdp-pointing-offset")


def test_get_pointing_unix(pointing_timestamps_mjd):
    """
    Unit test for computing the median of the concatenated
    pointing timestamps and then converting it from MJD to
    UTC seconds after Unix epoch
    """
    pointing_time_unix = _get_pointing_unix(
        numpy.array([numpy.median(pointing_timestamps_mjd)])
    )
    numpy.testing.assert_allclose(
        pointing_time_unix, 1672728499.780131, rtol=1e-3
    )


def test_get_middle_timestamp():
    """
    Unit test for extracting the reference time in MJD and reshaping
    the commanded pointings and then converting them to degrees
    """
    commanded_azel_rad = numpy.dstack(
        (
            [1.22, 1.23, 1.24, 1.25, 1.26, 1.27, 1.28, 1.29, 1.3, 1.31],
            [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        )
    )
    pointing_time = numpy.array(
        [
            5.178e09,
            5.179e09,
            5.1795e09,
            5.181e09,
            5.182e09,
        ]
    )
    nants = 2
    middle_timestamp_index, middle_timestamp, commanded_azel_deg = (
        _get_middle_timestamp(commanded_azel_rad, pointing_time, nants)
    )

    # check the index of the middle timestamp
    assert middle_timestamp_index == 2

    # expected time is the central value of pointing_time
    assert middle_timestamp == 5.1795e09

    # equivalent az/el in degrees
    assert commanded_azel_deg.shape == (nants, len(pointing_time), 2)
    numpy.testing.assert_almost_equal(
        commanded_azel_deg,
        numpy.array(
            [
                [
                    [69.90085101, 5.72957795],
                    [70.4738088, 11.4591559],
                    [71.0467666, 17.18873385],
                    [71.61972439, 22.91831181],
                    [72.19268219, 28.64788976],
                ],
                [
                    [72.76563998, 34.37746771],
                    [73.33859778, 40.10704566],
                    [73.91155557, 45.83662361],
                    [74.48451337, 51.56620156],
                    [75.05747116, 57.29577951],
                ],
            ]
        ),
        decimal=6,
    )


def test_fix_antenna_timestamp_pointing_table():
    """
    Unit test for fix_corrupted_pointing_table function
    """
    pointing_mjds = numpy.array([40, 41, 41])
    pointing_antid = numpy.array([0, 0, 1])

    column_ant = makescacoldesc("ANTENNA_ID", 1)
    column_time = makescacoldesc("TIME", 4234124.0)
    table_desc = maketabdesc([column_ant, column_time])
    pt_table = table("__mytables", table_desc, nrow=3, ack=False)

    pt_table.putcol("ANTENNA_ID", pointing_antid)
    pt_table.putcol("TIME", pointing_mjds)

    point_table = _fix_antenna_timestamp_pointing_table(pt_table)
    time = point_table.getcol("TIME").tolist()
    ntime = len(point_table.getcol("TIME"))
    pt_table.close()
    assert time == [41, 41]
    assert ntime == 2


@patch("ska_sdp_wflow_pointing_offset.read_data.create_visibility_from_ms")
@patch("ska_sdp_wflow_pointing_offset.read_data._load_ms_tables")
@patch("glob.glob", return_value=["fake_ms"])
def test_read_batch_visibilities(mock_dir, mock_tables, mock_ms, vis_array):
    """
    Unit test for read_visibilities function
    """
    mock_dir.return_value = ["fake_ms"]
    mock_tables.return_value = (
        MockDataDescriptionTable(),
        MockSpectralWindowTable(),
        MockPointingTable(),
    )
    mock_ms.return_value = vis_array[0]
    (
        vis,
        on_sky_offsets,
        ants,
        reference_timestamp,
        reference_commanded_azel,
        track_duration_lists,
    ) = read_batch_visibilities("test_dir")

    # Specific attributes
    assert vis[0][0][0].vis.data.shape == (5, 6, 64, 2)
    assert vis[0][0][0].weight.data.shape == (5, 6, 64, 2)
    assert (vis[0][0][0].frequency.data == FREQS).all()
    assert (vis[0][0][0].polarisation.data == CORR_TYPE).all()
    assert on_sky_offsets[0][0].shape == (3, 5, 2)
    assert numpy.array(ants[0]).shape == (3,)
    assert ants[0][0].name == "M001"
    assert ants[0][0].diameter == 13.5

    numpy.testing.assert_allclose(
        reference_timestamp[0], 5179444879.966191, rtol=1e-3
    )
    assert reference_commanded_azel[0] is None
    assert track_duration_lists == [
        [49.99992847442627] * len(on_sky_offsets[0])
    ]


@patch("ska_sdp_wflow_pointing_offset.read_data.create_visibility_from_ms")
@patch("ska_sdp_wflow_pointing_offset.read_data._load_ms_tables")
@patch("glob.glob", return_value=["fake_ms"])
def test_read_batch_visibilities_apply_mask(
    mock_dir, mock_tables, mock_ms, vis_array
):
    """
    Unit test for read_visibilities function with RFI mask applied
    """
    rfi_file = "instrument/ska1_mid/static-rfi/rfi_mask.h5"
    mock_dir.return_value = ["fake_ms"]
    mock_tables.return_value = (
        MockDataDescriptionTable(),
        MockSpectralWindowTable(),
        MockPointingTable(),
    )
    mock_ms.return_value = vis_array

    try:
        (
            vis,
            on_sky_offsets,
            ants,
            reference_timestamp,
            reference_commanded_azel,
            track_duration_lists,
        ) = read_batch_visibilities(
            "test_dir", apply_mask=True, rfi_filename=rfi_file
        )

        # Specific attributes
        assert vis[0][0].vis.data.shape == (5, 6, 64, 2)
        assert vis[0][0].weight.data.shape == (5, 6, 64, 2)
        assert (vis[0][0].frequency.data == FREQS).all()
        assert (vis[0][0].polarisation.data == CORR_TYPE).all()
        assert on_sky_offsets[0][0].shape == (3, 5, 2)
        assert numpy.array(ants[0]).shape == (3,)
        assert ants[0][0].name == "M001"
        assert ants[0][0].diameter == 13.5

        numpy.testing.assert_allclose(
            reference_timestamp[0], 5179444879.966191, rtol=1e-3
        )
        assert reference_commanded_azel[0] is None
        assert track_duration_lists == [
            [49.99992847442627] * len(on_sky_offsets[0])
        ]
    except FileNotFoundError:
        log.error(
            "RFI mask is not available in the Telescope Model Data Store."
            "Current test requires it to fully function."
        )


@patch("ska_sdp_wflow_pointing_offset.read_data.create_visibility_from_ms")
@patch("ska_sdp_wflow_pointing_offset.read_data._load_ms_tables")
@patch("glob.glob", return_value=["fake_ms"])
def test_read_batch_visibilities_wrong_rfi(
    mock_dir, mock_tables, mock_ms, vis_array
):
    """
    Unit test for read_visibilities function with wrong rfi file
    """
    with pytest.raises(FileNotFoundError):
        mock_dir.return_value = ["fake_ms"]
        mock_tables.return_value = (
            MockDataDescriptionTable(),
            MockSpectralWindowTable(),
            MockPointingTable(),
        )
        mock_ms.return_value = vis_array
        read_batch_visibilities(
            "test_dir", apply_mask=True, rfi_filename="some_path/fake_file.h5"
        )


def _prepare_ms_files(tempdir):
    """
    Prepare 4 MS files
    :param tempdir: Temp directory
    """
    timestamp = numpy.array([10.0, 11.0, 12.0])

    for i in range(4):
        file1 = f"{tempdir}/t{i}.ms"
        os.makedirs(f"{file1}", exist_ok=True)
        # Add data to the right columns
        column_def1 = makescacoldesc("TIME", 10.0)
        table_desc = maketabdesc([column_def1])

        pointing = table(
            f"{file1}/POINTING", table_desc, nrow=3, readonly=False
        )

        pointing.putcol("TIME", timestamp)
        pointing.flush()
        pointing.close()

        timestamp = timestamp + 10.0


def test_split_two_dish_ms():
    """
    Test _split_two_dish_modes_ms function
    """
    with tempfile.TemporaryDirectory() as tempdir:
        # Prepare MS files
        _prepare_ms_files(tempdir)

        # The time started from 10.0
        # expected results: [ [t0.ms, t1.ms],[t2.ms, t3.ms]]
        ms_all_files = glob.glob(tempdir + "/*.ms")
        ms_all_files = _split_two_dish_modes_ms(ms_all_files)
        assert ms_all_files[0][0] == f"{tempdir}/t0.ms"
        assert ms_all_files[0][1] == f"{tempdir}/t1.ms"
        assert ms_all_files[1][0] == f"{tempdir}/t2.ms"
        assert ms_all_files[1][1] == f"{tempdir}/t3.ms"


@patch("ska_sdp_wflow_pointing_offset.read_data._load_ms_tables")
def test_calc_on_sky_offsets_from_target_column(
    mock_tables, ants, pointing_timestamps_mjd
):
    """Unit test for_calc_on_sky_offsets_from_target_column"""
    mock_tables.return_value = (
        MockDataDescriptionTable(),
        MockSpectralWindowTable(),
        MockPointingTable(),
    )

    result = _calc_on_sky_offsets_from_target_column(
        MockPointingTable(),
        ants,
        pointing_timestamps_mjd[0],
        source_ra=5.14618,
        source_dec=-1.112,
    )

    # Check the on-sky offsets
    numpy.testing.assert_allclose(
        result,
        numpy.array(
            (
                [
                    [-0.02796802, -2.75814051],
                    [-0.02845286, -2.76173736],
                    [-0.0298191, -2.08755133],
                    [-0.03158732, -2.83232067],
                    [-0.03304526, -2.85480321],
                ],
                [
                    [-0.02640472, -2.03263654],
                    [-0.02938701, -2.10954792],
                    [-0.03049394, -2.13237765],
                    [-0.03102428, -1.43907621],
                    [-0.03366928, -2.20264354],
                ],
                [
                    [-0.02815011, -2.07692622],
                    [-0.02760814, -1.38426246],
                    [-0.03082217, -1.48039297],
                    [-0.03096043, -1.48413136],
                    [-0.03283081, -0.8103871],
                ],
            )
        ),
        rtol=1e-3,
    )


def test_calc_track_duration(pointing_timestamps_mjd):
    """Unit test for_calc_track_duration"""
    pointing_mjds, track_duration = _calc_track_duration(
        pointing_timestamps_mjd
    )

    numpy.testing.assert_allclose(
        pointing_mjds,
        numpy.array(
            [
                5.17944478e09,
                5.17944479e09,
                5.17944481e09,
                5.17944482e09,
                5.17944484e09,
            ]
        ),
        rtol=1e-3,
    )
    numpy.testing.assert_almost_equal(track_duration, 49.999928, decimal=6)
