"""
End to end test with sample data.
Sample data is taken from MeerKAT observation with 4 antennas
and 4096 frequency channels.
The execution is handled with test_with_data.sh
Which downloads data from GCP and execute the CLI.
"""

import logging
import os
import subprocess

import numpy
import pytest

log = logging.getLogger("pointing-offset-logger")
log.setLevel(logging.WARNING)

TEST_PATH = "/test_results/outer_scans/"


@pytest.mark.datatest
def test_check_outputs():
    """
    Check outputs from the bash script
    """
    root_dir = os.getcwd()

    cmd = [
        "pointing-offset",
        "compute",
        "--msdir=" + root_dir + TEST_PATH,
        "--use_source_offset_column",
        "--thresh_width=1.15",
        "--num_chunks=16",
    ]
    result = subprocess.run(
        cmd,
        check=True,
        capture_output=True,
        text=True,
    )
    log.info(result.stdout)

    ref_file = root_dir + TEST_PATH + "reference_offsets.txt"
    out_file = (
        root_dir + TEST_PATH + "1672727931_sdp_l0_scan_pointing_offsets.txt"
    )
    assert os.path.exists(out_file)

    out_data = numpy.loadtxt(out_file, delimiter=",", dtype=object)
    ants = out_data[:, 0]
    assert (ants == ["m000", "m001", "m002", "m003"]).all()

    offset_data = numpy.array(out_data[:, 1:].astype(float))
    assert offset_data.shape == (4, 12)

    # Check if the testing result has changed
    ref_data = numpy.array(
        numpy.loadtxt(ref_file, delimiter=",", dtype=object)[:, 1:].astype(
            float
        )
    )
    numpy.testing.assert_array_almost_equal(offset_data, ref_data, decimal=6)

    # Assert the right plots are outputted
    prefix = root_dir + TEST_PATH + "1672727931_sdp_l0_scan_"
    assert os.path.exists(prefix + "gain_amplitude.png")
    for i in range(4):
        assert os.path.exists(prefix + f"fitting_results_m00{i}.png")

    assert os.path.exists(prefix + "offsets.png")
