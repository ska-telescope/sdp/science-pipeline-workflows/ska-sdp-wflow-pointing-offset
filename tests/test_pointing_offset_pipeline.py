"""
Tests for `pointing_offset_pipeline` module
"""

import pytest

from ska_sdp_wflow_pointing_offset.pointing_pipeline import InitPipeline

DEFAULT_ARGS = {
    "--apply_mask": False,
    "--fit_to_sep_pol": False,
    "--bw_factor": False,
    "--end_freq": None,
    "--fit_to_vis": False,
    "--msdir": "some-dir",
    "--num_chunks": "16",
    "--results_dir": None,
    "--rfi_file": None,
    "--start_freq": None,
    "--thresh_width": "1.15",
    "--use_modelvis": False,
    "--use_source_offset_column": False,
    "<bw_factor>": [],
    "COMMAND": "compute",
}


def test_pointing_pipeline_init():
    """Pipeline class initializes correctly"""
    pipeline = InitPipeline(DEFAULT_ARGS)

    assert pipeline.num_chunks == 16
    assert pipeline.thresh_width == 1.15
    assert pipeline.apply_mask is False
    assert pipeline.fit_to_sep_pol is False


def test_pointing_pipeline_init_num_chunks_two():
    """
    Error is raised with correct message if
    --num_chunks is set to two.
    """
    args = DEFAULT_ARGS.copy()
    args["--num_chunks"] = 2

    with pytest.raises(ValueError) as err:
        InitPipeline(args)

    expected_msg = "num_chunks cannot be 2 for num_chunks > 1."
    # err.value.args[0] is the error message
    assert expected_msg in err.value.args[0]


def test_pointing_pipeline_init_changed_bool():
    """
    Boolean init values of the pipeline are correctly
    changed based on input arguments.
    """
    args = DEFAULT_ARGS.copy()
    args["--use_modelvis"] = True
    args["--fit_to_sep_pol"] = True

    pipeline = InitPipeline(args)
    assert pipeline.use_modelvis is True
    assert pipeline.fit_to_sep_pol is True


def test_pointing_pipeline_beamwidth_factor_default():
    """
    Default beamwidth_factor property is correctly set.
    """
    pipeline = InitPipeline(DEFAULT_ARGS)
    assert pipeline.beamwidth_factor == [0.976, 1.098]


def test_pointing_pipeline_beamwidth_factor_custom():
    """
    beamwidth_factor property is correctly updated
    based on input arguments
    """
    args = DEFAULT_ARGS.copy()
    args["<bw_factor>"] = [0.1, 0.2]
    pipeline = InitPipeline(args)
    assert pipeline.beamwidth_factor == [0.1, 0.2]


def test_pointing_pipeline_beamwidth_factor_single_value():
    """
    beamwidth_factor property is correctly set when a
    single value is provided as input.
    """
    args = DEFAULT_ARGS.copy()
    args["<bw_factor>"] = [0.12]
    pipeline = InitPipeline(args)
    assert pipeline.beamwidth_factor == [0.12, 0.12]


def test_pointing_pipeline_beamwidth_factor_exist():
    """
    beamwidth_factor property is not updated when
    it has been already set once.
    """
    args = DEFAULT_ARGS.copy()
    args["<bw_factor>"] = [0.1, 0.2]
    pipeline = InitPipeline(args)
    assert pipeline.beamwidth_factor == [0.1, 0.2]

    # args["<bw_factor>"] is loaded into self._bw_factor
    # even if we manually overwrite it, it does not affect
    # self.beamwidth_factor anymore
    # pylint: disable-next=protected-access
    pipeline._bw_factor = [1.0, 1.0]
    assert pipeline.beamwidth_factor == [0.1, 0.2]
