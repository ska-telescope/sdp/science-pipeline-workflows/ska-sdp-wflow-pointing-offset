"""Unit tests for gain calibration functions"""

import numpy
import pytest
from astropy import units
from astropy.coordinates import SkyCoord

from ska_sdp_wflow_pointing_offset.calibration.gt_solve import (
    _get_modelvis,
    compute_gains,
    extract_parallel_hand_gains,
)
from tests.utils_three_dish import NANTS, NSCANS

ska_sdp_func = pytest.importorskip("ska_sdp_func")


SOURCE = SkyCoord(
    ra=294.869 * units.deg, dec=-63.1682 * units.deg, frame="icrs"
)


def test_get_modelvis(vis_array):
    """
    Unit test for solving for model visibilities in gain calibration
    """
    for vis in vis_array:
        model_vis = _get_modelvis(SOURCE, vis[0])

        assert vis[0].vis.data.shape == model_vis.vis.data.shape
        assert (
            numpy.abs(vis[0].vis.data) != numpy.abs(model_vis.vis.data)
        ).all()


@pytest.mark.parametrize("source", [None, SOURCE])
def test_compute_gains(vis_array, source):
    """
    Unit test for compute_gains, the function that
    uses the gain solver in ska-sdp-func-python to
    derive the un-normalised G terms
    """
    for vis in vis_array:
        gt_list = compute_gains(vis, source=None)

        assert gt_list[0].attrs["data_model"] == "GainTable"
        assert gt_list[0]["gain"].data.shape == (NSCANS, NANTS, 1, 2, 2)


@pytest.mark.parametrize("source", [None, SOURCE])
def test_extract_parallel_hand_gains(vis_array, source):
    """
    Unit test for extracting the parallel-hands polarisation
    gain solutions
    """
    gt_list = compute_gains(vis_array[0], source)
    gt, gt_weights = extract_parallel_hand_gains(gt_list[0])

    # gt_list is already tested in the test_compute_gains function
    assert gt.shape == gt_weights.shape != (NSCANS, NANTS, 1, 2, 2)
    assert gt.shape == gt_weights.shape == (NSCANS, NANTS, 2)
    assert (
        gt
        == numpy.dstack(
            (
                numpy.abs(gt_list[0].gain.data[:, :, :, 0, 0]),
                numpy.abs(gt_list[0].gain.data[:, :, :, 1, 1]),
            )
        )
    ).all()
    assert (
        gt_weights
        == numpy.dstack(
            (
                gt_list[0].weight.data[:, :, :, 0, 0],
                gt_list[0].weight.data[:, :, :, 1, 1],
            )
        )
    ).all()
