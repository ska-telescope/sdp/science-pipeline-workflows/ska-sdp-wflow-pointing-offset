# pylint: disable=protected-access,too-many-lines

"""
SDP Pointing Pipeline tests
"""

import datetime
import logging
import os
import time
from threading import Thread
from unittest.mock import Mock, patch

import numpy
import pytest
from benedict import benedict
from katpoint import lightspeed
from ska_sdp_config.entity import ExecutionBlock, flow
from ska_sdp_config.entity.common import PVCPath
from ska_sdp_dataqueues import DataQueueConsumer

from ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp import SDPPipeline
from ska_sdp_wflow_pointing_offset.utils import convert_to_structured
from tests.test_pointing_offset_pipeline import DEFAULT_ARGS
from tests.utils_generic import assert_json_data

SCRIPT = {"kind": "realtime", "name": "test_rt_script", "version": "0.0.1"}
EB_ID = "eb-test-20230714-00000"
PB_ID = "pb-test-20230714-00000"

SDP_ARGS = DEFAULT_ARGS.copy()
SDP_ARGS["--num_scans"] = "5"


def test_pipeline_sdp_init_missing_eb():
    """SDP pipeline raises ValueError if eb ID is not set."""
    with pytest.raises(ValueError) as err:
        SDPPipeline(SDP_ARGS)

    expected_msg = "Provide an execution block id for processing in SDP"
    # err.value.args[0] is the error message
    assert expected_msg in err.value.args[0]


@patch.dict(os.environ, {"SDP_PB_ID": ""})
def test_pipeline_sdp_init_missing_pb():
    """SDP pipeline raises ValueError if pb ID is not set."""
    args = SDP_ARGS.copy()
    args["--eb_id"] = "some-eb-123"
    with pytest.raises(ValueError) as err:
        SDPPipeline(args)

    expected_msg = (
        "Processing block ID is not set in SDP_PB_ID environment variable"
    )
    # err.value.args[0] is the error message
    assert expected_msg in err.value.args[0]


@patch.dict(os.environ, {"SDP_PB_ID": "pb-some-12345678-1"})
def test_pipeline_sdp_init_missing_results_dir():
    """SDP pipeline raises ValueError if --results_dir is not set."""
    args = SDP_ARGS.copy()
    args["--eb_id"] = "some-eb-123"
    with pytest.raises(FileNotFoundError) as err:
        sdp_pipeline = SDPPipeline(args)
        _ = sdp_pipeline.results_dir

    expected_msg = "Need to set the directory for results via --results_dir"
    # err.value.args[0] is the error message
    assert expected_msg in err.value.args[0]


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch(
    "ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp."
    "SDPPipeline.set_scan_ids_and_last_index",
    Mock(),
)
def test_pipeline_sdp_results_dir():
    """SDP pipeline sets results_dir property correctly based on scan_ids."""
    args = SDP_ARGS.copy()
    args["--eb_id"] = "some-eb-123"
    args["--results_dir"] = "bla/"
    args["--eb_id"] = EB_ID

    result = SDPPipeline(args)
    result.scan_ids = [1, 2, 3, 4, 6]
    assert result.results_dir == "bla/scan1-6"


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch(
    "ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp."
    "SDPPipeline.set_scan_ids_and_last_index",
    Mock(),
)
def test_pipeline_sdp_results_dir_not_reset():
    """
    The results_dir property will not reset on its own
    even if the scan_ids are different.
    The SDPPipeline.close() method makes sure it's
    set to None at the end of each run in order to
    trigger a new directory set up.
    """
    args = SDP_ARGS.copy()
    args["--eb_id"] = "some-eb-123"
    args["--results_dir"] = "bla/"
    args["--eb_id"] = EB_ID

    result = SDPPipeline(args)
    result.scan_ids = [1, 2, 3, 4, 6]
    assert result.results_dir == "bla/scan1-6"

    result.scan_ids = [121, 32, 223, 442, 5]
    assert result.results_dir == "bla/scan1-6"


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_pipeline_sdp_data_flow_data_product_found(mock_config, cfg):
    """
    Unit test for storing data products using data_flow entry
    when the data product is found
    """
    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "/bla"
    pipeline = SDPPipeline(args)
    pipeline.scan_ids = [1, 2, 3, 4, 6]
    pvc_path = PVCPath(
        k8s_namespaces="test-namespace",
        k8s_pvc_name="SDP_DATA_PVC_NAME",
        pvc_mount_path="/mnt/data",
        pvc_subpath=f"product/{EB_ID}/ska-sdp/{PB_ID}",
    )
    added_flow = flow.Flow(
        key=flow.Flow.Key(
            pb_id=PB_ID,
            kind="data-product",
            name="pointing-offset",
        ),
        sink=flow.DataProduct(
            kind="data-product",
            data_dir=pvc_path,
            paths=[],
        ),
        sources=[],
        data_model="PointingTable",
    )

    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)

    assert (
        pipeline.common_prefix
        == f"/mnt/data/product/{EB_ID}/ska-sdp/{PB_ID}/scan1-6/"
    )


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_pipeline_sdp_data_flow_vis_receive_found(mock_config, cfg):
    """
    Unit test for storing data products using data_flow entry
    when the data product is found
    """
    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    pipeline = SDPPipeline(args)
    pipeline.scan_ids = [1, 2, 3, 4, 6]

    added_flow = flow.Flow(
        key=flow.Flow.Key(
            pb_id=PB_ID,
            kind="data-product",
            name="pointing-offset",
        ),
        sink=flow.DataProduct(
            kind="data-product",
            data_dir=PVCPath(
                k8s_namespaces="test-namespace",
                k8s_pvc_name="DATA_PRODUCT_PVC_NAME",
                pvc_mount_path="/data",
                pvc_subpath=f"product/{EB_ID}/ska-sdp/{PB_ID}",
            ),
            paths=[],
        ),
        sources=[],
        data_model="PointingTable",
    )

    vis_receive_pb_id = "pb-test-20241015-00001"
    pvc_path = PVCPath(
        k8s_namespaces="test-namespace",
        k8s_pvc_name="SDP_DATA_PVC_NAME",
        pvc_mount_path="/mnt/data",
        pvc_subpath=f"product/{EB_ID}/ska-sdp/{vis_receive_pb_id}",
    )
    added_vis_flow = flow.Flow(
        key=flow.Flow.Key(
            pb_id=vis_receive_pb_id,
            kind="data-product",
            name="vis-receive-mswriter-processor",
        ),
        sink=flow.DataProduct(
            kind="data-product",
            data_dir=pvc_path,
            paths=[],
        ),
        sources=[],
        data_model="PointingTable",
    )
    added_flow.sources.append(
        flow.FlowSource(
            uri=added_vis_flow.key,
            function="ska-sdp-wflow-pointing-offset:pointing-offset",
        )
    )
    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_vis_flow)
        txn.flow.create(added_flow)

    # Retrieve flow
    assert (
        pipeline.msdir == f"/data/product/{EB_ID}/ska-sdp/{vis_receive_pb_id}"
    )


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_pipeline_sdp_data_flow_data_product_notfound(mock_config, cfg):
    """
    Unit test for storing data products using data_flow entry
    when the data product is not found
    """
    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "/foo"
    pipeline = SDPPipeline(args)
    pipeline.scan_ids = [1, 2, 3, 4, 6]

    assert pipeline.common_prefix == "/foo/scan1-6/"


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_pipeline_sdp_receptors(mock_config, cfg):
    """receptors property returned correctly."""
    mock_config.return_value = cfg
    eb = ExecutionBlock(
        key=EB_ID,
        resources={"receptors": ["SKA1", "SKA2"]},
    )

    for txn in cfg.txn():
        txn.execution_block.update(eb)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    result = SDPPipeline(args)
    assert result.receptors == ["SKA1", "SKA2"]


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_pipeline_sdp_receptors_no_receptors(mock_config, cfg):
    """receptors property returns None when no receptors are assigned."""
    mock_config.return_value = cfg

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    result = SDPPipeline(args)
    assert result.receptors is None


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_pipeline_sdp_receptors_no_resources(mock_config, cfg):
    """receptors property returns None when no resources are available."""
    mock_config.return_value = cfg

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    result = SDPPipeline(args)
    assert result.receptors is None


@pytest.mark.parametrize(
    "scan_list",
    [
        [],
        [
            {
                "scan_ids": [65, 138],
                "error": "none",
                "traceback": "none",
                "status": "success",
            },
            {
                "scan_ids": [16, 21],
                "error": "none",
                "traceback": "none",
                "status": "success",
            },
        ],
    ],
)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_update_processing_block_state_no_processed(
    mock_config, scan_list, cfg
):
    """
    Unit test for update_processing_block_state.
    In case no entry of "processed" are present yet.
    """
    mock_config.return_value = cfg

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    pipeline = SDPPipeline(args)
    pipeline.update_processing_block_state(scan_list)

    for txn in cfg.txn():
        result = txn.processing_block.state(PB_ID).get()

    assert list(result.keys()) == ["processed", "status"]
    assert result["processed"] == scan_list


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_update_data_flow_state_init(mock_config, cfg):
    """
    Unit test for update_data_flow_state.
    When state is initialised as None
    """
    mock_config.return_value = cfg
    error_data = {
        "scan_ids": [1, 18],
        "error": "mock-error",
        "traceback": "mock-traceback",
        "status": "fail",
    }
    key = flow.Flow.Key(
        pb_id=PB_ID,
        kind="data-queue",
        name="pointing-offset",
    )
    added_flow = flow.Flow(
        key=key,
        sink=flow.DataQueue(
            topics="some-kafka-topic",
            host="some-kafka-host:9092",
            format="json",
        ),
        sources=[],
        data_model="PointingTable",
    )
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    pipeline = SDPPipeline(args)

    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)

    pipeline.update_data_flow_state(error_data, kind="data-queue")

    for txn in cfg.txn():
        result = txn.flow.state(key).get()

    assert result["status"] == "FAULT"
    assert result["internal"] == [[1, 18]]
    assert result["log"][0]["message"] == "mock-error"
    assert result["log"][0]["traceback"] == "mock-traceback"


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_update_data_flow_state_noerror(mock_config, cfg):
    """
    Unit test for update_data_flow_state.
    In case no error provided
    """
    mock_config.return_value = cfg

    key = flow.Flow.Key(
        pb_id=PB_ID,
        kind="data-queue",
        name="pointing-offset",
    )
    added_flow = flow.Flow(
        key=key,
        sink=flow.DataQueue(
            topics="some-kafka-topic",
            host="some-kafka-host:9092",
            format="json",
        ),
        sources=[],
        data_model="PointingTable",
    )
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    pipeline = SDPPipeline(args)

    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)
        txn.flow.state(key).create({"status": "PENDING"})

    pipeline.update_data_flow_state(None, kind="data-queue")

    for txn in cfg.txn():
        result = txn.flow.state(key).get()

    assert result["status"] == "COMPLETE"
    assert result["log"] == []


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_update_data_flow_state_full(mock_config, cfg):
    """
    Unit test for update_data_flow_state.
    In case an error has happened
    There has already been previous error messages stored.
    """
    mock_config.return_value = cfg
    error_data = {
        "scan_ids": [1, 18],
        "error": "mock-error",
        "traceback": "mock-traceback",
        "status": "fail",
    }
    key = flow.Flow.Key(
        pb_id=PB_ID,
        kind="data-queue",
        name="pointing-offset",
    )
    added_flow = flow.Flow(
        key=key,
        sink=flow.DataQueue(
            topics="some-kafka-topic",
            host="some-kafka-host:9092",
            format="json",
        ),
        sources=[],
        data_model="PointingTable",
    )
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    pipeline = SDPPipeline(args)

    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)
        txn.flow.state(key).create(
            {
                "status": "pending",
                "internal": [[2, 5]],
                "log": ["Random log from previous batch"],
            }
        )

    pipeline.update_data_flow_state(error_data, kind="data-queue")

    for txn in cfg.txn():
        result = txn.flow.state(key).get()

    assert result["status"] == "FAULT"
    assert result["internal"] == [[2, 5], [1, 18]]
    assert result["log"][1]["message"] == "mock-error"
    assert result["log"][1]["traceback"] == "mock-traceback"


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_update_data_flow_state_partial(mock_config, cfg):
    """
    Unit test for update_data_flow_state.
    In case an error has happened
    There hasn't been full error messages stored yet
    """
    mock_config.return_value = cfg
    error_data = {
        "scan_ids": [1, 18],
        "error": "mock-error",
        "traceback": "mock-traceback",
        "status": "fail",
    }
    key = flow.Flow.Key(
        pb_id=PB_ID,
        kind="data-queue",
        name="pointing-offset",
    )
    added_flow = flow.Flow(
        key=key,
        sink=flow.DataQueue(
            topics="some-kafka-topic",
            host="some-kafka-host:9092",
            format="json",
        ),
        sources=[],
        data_model="PointingTable",
    )
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    pipeline = SDPPipeline(args)

    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)
        txn.flow.state(key).create(
            {
                "status": "PENDING",
                "log": [],
            }
        )

    pipeline.update_data_flow_state(error_data, kind="data-queue")

    for txn in cfg.txn():
        result = txn.flow.state(key).get()

    assert result["status"] == "FAULT"
    assert result["internal"] == [[1, 18]]
    assert result["log"][0]["message"] == "mock-error"
    assert result["log"][0]["traceback"] == "mock-traceback"


@pytest.mark.parametrize(
    "scan_list",
    [
        [],
        [
            {
                "scan_ids": [65, 138],
                "error": "none",
            },
            {
                "scan_ids": [16, 21],
                "error": "none",
            },
        ],
    ],
)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_update_processing_block_state_append_processed(
    mock_config, scan_list, cfg
):
    """
    Unit test for update_processing_block_state.
    In case there are already "processed" entries.
    """
    mock_config.return_value = cfg
    initial_scans = [
        {
            "scan_ids": [1, 18],
            "error": "none",
            "traceback": "none",
            "status": "success",
        },
        {
            "scan_ids": [23, 25, 33],
            "error": "none",
            "traceback": "none",
            "status": "success",
        },
    ]

    pb_state = {"status": "RUNNING", "processed": initial_scans}

    for txn in cfg.txn():
        txn.processing_block.state(PB_ID).update(pb_state)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    pipeline = SDPPipeline(args)
    pipeline.update_processing_block_state(scan_list)

    for txn in cfg.txn():
        result = txn.processing_block.state(PB_ID).get()

    assert list(result.keys()) == ["processed", "status"]
    assert result["processed"] == initial_scans + scan_list


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_find_last_scan_index_scenario_1(mock_config, cfg, caplog):
    """
    Test for find_last_scan_index method.

    Scenario 1:
    We are in the middle of the first batch,
    nothing has been processed yet.
    """
    mock_config.return_value = cfg
    eb_state = {
        "scans": [
            {"scan_id": 34, "scan_type": "science", "status": "FINISHED"},
            {
                "scan_id": 65,
                "scan_type": "pointing-abc-123",
                "status": "FINISHED",
            },
        ],
    }

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    pipeline = SDPPipeline(args)
    result = pipeline.find_last_scan_index()

    assert result == -1

    # Assert log message
    caplog.set_level(logging.INFO)
    assert "No information from the processing block state." in caplog.text


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_find_last_scan_index_scenario_2(mock_config, cfg):
    """
    Test for find_last_scan_index method.

    Scenario 2:
    When there exists processed list in the pb
    It should read the entries and return last processed
    (It compares the scan ids in the eb_state and in the processed
    list and returns the index in eb_state of the last scan id
    in processed.)
    """

    eb_state = {
        "scans": [
            {
                "scan_id": 138,
                "scan_type": "pointing-abc-212",
                "status": "FINISHED",
            },
            {
                "scan_id": 21,
                "scan_type": "pointing-abc-212",
                "status": "FINISHED",
            },
            {
                "scan_id": 16,
                "scan_type": "pointing-abc-212",
                "status": "FINISHED",
            },
            {
                "scan_id": 53,
                "scan_type": "pointing-abc-212",
                "status": "FINISHED",
            },
            {
                "scan_id": 82,
                "scan_type": "pointing-abc-212",
                "status": "FINISHED",
            },
        ],
    }
    mock_config.return_value = cfg
    pb_state = {
        "processed": [
            {
                "scan_ids": [138, 21, 16, 53, 82],
                "error": "none",
                "traceback": "none",
                "status": "success",
            },
        ],
    }

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)
        txn.processing_block.state(PB_ID).update(pb_state)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    pipeline = SDPPipeline(args)
    result = pipeline.find_last_scan_index()

    # Last index is the 9th scan (index 8)
    assert result == 4


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_find_last_scan_index_scenario_3(mock_config, cfg, caplog):
    """
    Test for find_last_scan_index

    Scenario 3:
    Processed scan from PB is not in list of scans from EB.
    In this case, start counting from the beginning, i.e
    index is -1.
    """
    mock_config.return_value = cfg
    eb_state = {
        "scans": [
            {"scan_id": 34, "scan_type": "science", "status": "FINISHED"},
            {
                "scan_id": 65,
                "scan_type": "pointing-xcd-655",
                "status": "FINISHED",
            },
            {
                "scan_id": 4,
                "scan_type": "pointing-bcd-444",
                "status": "FINISHED",
            },
        ],
    }
    pb_state = {
        "processed": [
            {
                # these are not in eb_state
                "scan_ids": [500, 501, 502],
                "error": "none",
                "traceback": "none",
                "status": "success",
            }
        ],
    }

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)
        txn.processing_block.state(PB_ID).update(pb_state)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--num_scans"] = 2
    args["--results_dir"] = "bla/"

    pipeline = SDPPipeline(args)
    result = pipeline.find_last_scan_index()

    # Last index is the 9th scan (index 8)
    assert result == -1

    # Assert log message
    caplog.set_level(logging.ERROR)
    assert "Processed scan not in list of scans." in caplog.text


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_wait_for_pointing_scan_type(mock_config, cfg):
    """
    The eb's "scan_type" is None, we update the
    value to "pointing" after 1 second (in a thread)
    at that point the watcher iterates and finds the new scan type
    and returns True.
    """
    mock_config.return_value = cfg

    for txn in cfg.txn():
        eb_state = txn.execution_block.state(EB_ID).get()

    def update_after_delay():
        time.sleep(1)
        new_eb_state = eb_state.copy()
        new_eb_state["scan_type"] = "pointing-abc-123"
        for txn in cfg.txn():
            txn.execution_block.state(EB_ID).update(new_eb_state)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"

    pipeline = SDPPipeline(args)

    # Set up thread to update key after delay
    thread = Thread(target=update_after_delay)
    start = datetime.datetime.now()
    thread.start()

    result = pipeline.wait_for_pointing_scan_type()

    # Make sure to wait for thread to finish
    thread.join()
    finish = datetime.datetime.now() - start

    assert result is True

    # the update happens 1s after start
    assert finish.total_seconds() >= 1


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_get_pointing_scan_ids_start(mock_config, cfg):
    """
    When a scan is missing, the watcher keeps watching and nothing
    will be returned, until all the scans are in place.

    We expect 3 scans, and the code will only return when the 3rd
    scan with pointing type has finished.
    Here it is started from last_index=-1 (first scan).
    """
    eb_state = {
        "scan_type": "calibration",
        "scan_id": 8,
        "scans": [
            {
                "scan_id": 2,
                "scan_type": "pointing-xcd-222",
                "status": "FINISHED",
            },
            {
                "scan_id": 3,
                "scan_type": "pointing-xcd-222",
                "status": "FINISHED",
            },
        ],
    }

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    def update_after_delay():
        time.sleep(1)
        new_eb_state = eb_state.copy()
        new_eb_state["scans"].append(
            {
                "scan_id": 7,
                "scan_type": "pointing-xcd-222",
                "status": "FINISHED",
            }
        )
        for txn in cfg.txn():
            txn.execution_block.state(EB_ID).update(new_eb_state)

    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)

    # Set up thread to update key after delay
    thread = Thread(target=update_after_delay)
    start = datetime.datetime.now()
    thread.start()

    result, index = pipeline.get_pointing_scan_ids(last_index=-1)

    # Make sure to wait for thread to finish
    thread.join()
    finish = datetime.datetime.now() - start

    assert result == [2, 3, 7]
    assert index == 2
    # the update happens 1s after start
    assert finish.total_seconds() >= 1


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_get_pointing_scan_ids_with_last_index(mock_config, cfg):
    """
    Assume we already processed/skipped the first two in eb_state scans.
    Start searching from "last_index": 1.
    Process the next three scans of the same type.
    """
    eb_state = {
        "scans": [
            {
                "scan_id": 65,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {
                "scan_id": 4,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {  # start processing/waiting from this scan
                "scan_id": 138,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 21,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
        ],
    }

    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    def update_after_delay():
        time.sleep(1)
        new_eb_state = eb_state.copy()
        new_eb_state["scans"].append(
            {
                "scan_id": 53,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            }
        )
        for txn in cfg.txn():
            txn.execution_block.state(EB_ID).update(new_eb_state)

    # Set up thread to update key after delay
    thread = Thread(target=update_after_delay)

    start = datetime.datetime.now()
    thread.start()

    result, index = pipeline.get_pointing_scan_ids(last_index=1)

    # Make sure to wait for thread to finish
    thread.join()
    finish = datetime.datetime.now() - start

    assert result == [138, 21, 53]
    assert index == 4
    assert finish.total_seconds() >= 1


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_get_pointing_scan_ids_more_scans(mock_config, cfg):
    """
    Test for get_pointing_scan_ids, when the pipeline is off for some time
    And more than one batch of scans become available.
    It returns the first batch for processing, even if there is a second
    one ready, since the first hasn't been processed either
    (this is because we start with last_index=-1).
    """
    eb_state = {
        "scans": [
            {
                "scan_id": 24,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 221,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 216,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 253,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {
                "scan_id": 249,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {
                "scan_id": 250,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
        ],
    }
    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    result, index = pipeline.get_pointing_scan_ids(last_index=-1)

    # returns the first batch belonging to scan_type "pointing-abc-444"
    assert result == [24, 221, 216]
    assert index == 2


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_get_pointing_scan_ids_skip_aborted(mock_config, cfg):
    """
    get_pointing_scan_ids skips aborted scans and processes
    non-aborted ones of the same scan type.
    """
    eb_state = {
        "scans": [
            {
                "scan_id": 24,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 221,
                "scan_type": "pointing-abc-444",
                "status": "ABORTED",
            },
            {
                "scan_id": 216,
                "scan_type": "pointing-abc-444",
                "status": "ABORTED",
            },
            {
                "scan_id": 253,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 249,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
        ],
    }
    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    result, index = pipeline.get_pointing_scan_ids(last_index=-1)

    # returns the first batch belonging to scan_type "pointing-abc-444"
    assert result == [24, 253, 249]
    assert index == 4


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_get_pointing_scan_ids_skip_non_pointing(mock_config, cfg):
    """
    get_pointing_scan_ids skips non-pointing scans and processes
    pointing ones of the same scan type.
    """
    eb_state = {
        "scans": [
            {
                "scan_id": 24,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 221,
                "scan_type": "science",
                "status": "FINISHED",
            },
            {
                "scan_id": 216,
                "scan_type": "science",
                "status": "FINISHED",
            },
            {
                "scan_id": 253,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {
                "scan_id": 249,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
        ],
    }
    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    result, index = pipeline.get_pointing_scan_ids(last_index=-1)

    # returns the first batch belonging to scan_type "pointing-abc-444"
    assert result == [24, 253, 249]
    assert index == 4


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_get_pointing_scan_ids_skip_pointing_missing(mock_config, cfg, caplog):
    """
    get_pointing_scan_ids only processes three consecutive scans
    of the same pointing type. If any pointing types are interrupted
    by another pointing type, those will not be processed.
    """
    eb_state = {
        "scans": [
            {  # not processed (we change type after the second)
                "scan_id": 65,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {  # not processed (we change type after the this one)
                "scan_id": 4,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {  # new type starts here;
                # also skipped because we are going back to old type
                "scan_id": 138,
                "scan_type": "pointing-abc-444",
                "status": "FINISHED",
            },
            {  # going back to old type; will be processed with the
                # following two scans, not the first two ones.
                "scan_id": 1,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {  # staying with old type; will be processed
                "scan_id": 21,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
            {  # last old type to be processed
                "scan_id": 53,
                "scan_type": "pointing-xyz-655",
                "status": "FINISHED",
            },
        ],
    }

    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)

    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    result, index = pipeline.get_pointing_scan_ids(last_index=-1)

    assert result == [1, 21, 53]
    assert index == 5

    caplog.set_level(logging.WARNING)
    # although at the end it is processed,
    # this will be logged for the first time it is being skipped
    assert (
        "scans with type pointing-xyz-655 for ids [65, 4] are being skipped"
        in caplog.text
    )
    assert (
        "scans with type pointing-abc-444 for ids [138] are being skipped"
        in caplog.text
    )


@pytest.mark.timeout(3)
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_set_scan_ids_and_last_index(mock_config, cfg):
    """
    set_scan_ids_and_last_index method correctly updates the last_index
    and scan_ids variables at each call.
    """
    mock_config.return_value = cfg

    eb_state = {
        "scans": [
            {
                "scan_id": 434,
                "scan_type": "pointing-abc-344",
                "status": "FINISHED",
            },
            {
                "scan_id": 465,
                "scan_type": "pointing-abc-344",
                "status": "FINISHED",
            },
            {
                "scan_id": 534,
                "scan_type": "pointing-abc-344",
                "status": "FINISHED",
            },
            {
                "scan_id": 565,
                "scan_type": "pointing-abc-344",
                "status": "FINISHED",
            },
        ],
    }
    for txn in cfg.txn():
        txn.execution_block.state(EB_ID).update(eb_state)

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "bla/"
    args["--num_scans"] = 3

    pipeline = SDPPipeline(args)
    assert pipeline.last_index is None
    assert pipeline.scan_ids is None

    pipeline.set_scan_ids_and_last_index()

    assert pipeline.last_index == 2
    assert pipeline.scan_ids == [434, 465, 534]

    # ===========================================
    # for new batch of scans the method correctly
    # updates last_index and scan_ids

    def update_after_delay():
        time.sleep(1)
        new_eb_state = eb_state.copy()
        scans = new_eb_state["scans"]
        new_eb_state["scans"] = scans + [
            {
                "scan_id": 651,
                "scan_type": "pointing-abc-344",
                "status": "FINISHED",
            },
            {
                "scan_id": 652,
                "scan_type": "pointing-abc-344",
                "status": "FINISHED",
            },
        ]

        for txn in cfg.txn():
            txn.execution_block.state(EB_ID).update(new_eb_state)

    thread = Thread(target=update_after_delay)
    thread.start()

    pipeline.set_scan_ids_and_last_index()

    # Make sure to wait for thread to finish
    thread.join()

    assert pipeline.last_index == 5
    assert pipeline.scan_ids == [565, 651, 652]


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
@patch(
    "ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.create_metadata"
)  # pylint: disable-next=too-many-arguments,too-many-positional-arguments
def test_update_metadata_file(
    mock_metadata,
    mock_config,
    cfg,
    metadata_object,
    pointing_table,
    vis_array,
):
    """
    update_metadata_file method correctly adds additional obscore
    data to metadata file as well as information about the
    dataproduct file.
    """
    mock_metadata.return_value = metadata_object[0]
    file_name = metadata_object[1].split("/")[-1]
    mock_config.return_value = cfg
    expected_obscore = {
        "obs_id": "",
        "access_estsize": 0,
        "target_name": vis_array[0][0].source,
        "s_ra": vis_array[0][0].phasecentre.ra.deg,
        "s_dec": vis_array[0][0].phasecentre.dec.deg,
        "t_min": vis_array[0][0].time.data[0],
        "t_max": vis_array[0][0].time.data[-1],
        "em_min": lightspeed / (vis_array[0][0].frequency.data[0]),
        "em_max": lightspeed / (vis_array[0][0].frequency.data[-1]),
        "pol_states": pointing_table.receptor_frame.type,
        "pol_xel": pointing_table.receptor_frame.nrec,
    }

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "/bla"

    pipeline = SDPPipeline(args)
    pipeline._results_dir = metadata_object[1].split(file_name)[0]
    pipeline.scan_ids = [1, 2, 3]

    with patch(
        "ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp."
        "POINTING_METADATA_FILE",
        file_name,
    ):
        pipeline._update_metadata_file(
            "/mnt/data/product/eb123/ska-sdp/pb123/some_file.hdf5",
            vis_array[0][0],
            pointing_table,
        )

    result = benedict(metadata_object[1], format="yaml")
    result_obscore = result["obscore"]
    result_file_data = result["files"]

    for key, value in expected_obscore.items():
        assert result_obscore[key] == value

    # it correctly adds the path to the result HDF5 file starting at /product
    assert (
        result_file_data[0]["path"]
        == "/product/eb123/ska-sdp/pb123/some_file.hdf5"
    )
    assert (
        result_file_data[0]["description"]
        == "Pointing offsets for scans: [1, 2, 3]"
    )


def test_create_metadata(metadata_object):
    """Test generating an initial metadata file with data"""

    expected_metadata = {
        "config": {
            "image": SCRIPT["name"],
            "processing_block": PB_ID,
            "processing_script": SCRIPT["name"],
            "version": SCRIPT["version"],
        },
        "execution_block": EB_ID,
        "obscore": {
            "access_format": "application/x-hdf5",
            "calib_level": 0,
            "dataproduct_type": "POINTING-OFFSETS",
            "facility_name": "SKA-Observatory",
            "instrument_name": "SKA-MID",
            "obs_collection": "SKA-Observatory/SKA-MID/POINTING-OFFSETS",
        },
    }

    result = benedict(metadata_object[0].output_path, format="yaml")

    assert_json_data(expected_metadata, result)


@pytest.mark.asyncio
@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
@patch(
    "ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp."
    "SDPPipeline.set_scan_ids_and_last_index",
    Mock(),
)
async def test_send_numpy_to_kafka(mock_config, cfg, pointing_numpy):
    """
    Unit test for sending numpy array to kafka
    """

    expected_output = convert_to_structured(pointing_numpy, 5)

    mock_config.return_value = cfg
    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "/bla"

    pipeline = SDPPipeline(args)
    pipeline.scan_ids = [1, 2, 3, 4, 5]
    pipeline._data_model = "PointingNumpyArray"
    pipeline._kafka_server = "localhost:9092"
    pipeline._kafka_topic = "test-pointing"

    await pipeline.send_data_to_kafka(pointing_numpy)

    # Read the data back
    encoding = "msgpack_numpy"
    consumer = DataQueueConsumer(
        pipeline._kafka_server,
        topics=[pipeline._kafka_topic],
        encoding=encoding,
    )
    async with consumer:
        async for _, message in consumer:
            assert numpy.array_equal(message, expected_output)
            break


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_configure_kafka_from_dataqueue(mock_config, cfg):
    """
    Unit test for configure_kafka_from_dataqueue
    """
    mock_config.return_value = cfg

    added_flow = flow.Flow(
        key=flow.Flow.Key(
            pb_id=PB_ID,
            kind="data-queue",
            name="pointing-offset",
        ),
        sink=flow.DataQueue(
            topics="some-kafka-topic",
            host="some-kafka-host:9092",
            format="json",
        ),
        sources=[],
        data_model="PointingTable",
    )

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "/bla"
    pipeline = SDPPipeline(args)

    # Now add a working data flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)

    pipeline.configure_kafka_from_dataqueue()

    assert pipeline._data_model == "PointingTable"
    assert pipeline._kafka_topic == "some-kafka-topic"
    assert pipeline._kafka_server == "some-kafka-host:9092"


@patch.dict(os.environ, {"SDP_PB_ID": PB_ID})
@patch("ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp.Config")
def test_configure_kafka_from_dataqueue_notfound(mock_config, cfg):
    """
    Unit test for configure_kafka_from_dataqueue,
    when there are flows in config DB but it's not
    a dataqueue type
    """
    mock_config.return_value = cfg

    added_flow = flow.Flow(
        key=flow.Flow.Key(
            pb_id=PB_ID,
            kind="sharedmem",
            name="bla",
        ),
        sink=flow.SharedMem(
            impl="plasma",
            host_path="/tmp/plasma/socket",
        ),
        sources=[],
        data_model="Visibility",
    )

    args = SDP_ARGS.copy()
    args["--eb_id"] = EB_ID
    args["--results_dir"] = "/bla"
    pipeline = SDPPipeline(args)

    # Add a non queue flow
    for txn in cfg.txn():
        txn.flow.create(added_flow)

    pipeline.configure_kafka_from_dataqueue()

    # These should stay the default
    assert pipeline._data_model == "PointingNumpyArray"
    assert pipeline._kafka_topic == "pointing_offset"
    assert pipeline._kafka_server == "localhost:9092"
