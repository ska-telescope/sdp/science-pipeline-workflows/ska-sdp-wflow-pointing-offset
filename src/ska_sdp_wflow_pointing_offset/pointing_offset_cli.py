# pylint: disable=too-many-locals,too-many-statements

"""
Program with many options using docopt for computing pointing offsets.
Note: to run it as an SDP processing script set the SDP_PROCESSING_SCRIPT
      environment variable to "True"

Usage:
  pointing-offset COMMAND [--msdir=DIR] [--fit_to_sep_pol]
                          [--fit_to_vis] [--apply_mask]
                          [--use_source_offset_column]
                          [--rfi_file=FILE] [--results_dir=None]
                          [--start_freq=None] [--end_freq=None]
                          [(--bw_factor <bw_factor>) [<bw_factor>...]]
                          [--num_chunks=<int>] [--thresh_width=<float>]
                          [--eb_id=None] [--num_scans=<int>]
                          [--use_modelvis]

Commands:
  compute   Runs all required routines for computing the
  pointing offsets.

Options:
  -h --help             show this help message and exit
  -q --quiet            report only file names

  --msdir=DIR           Directory including Measurement set files
  --fit_to_sep_pol      Fit primary beams to the parallel-hands gain
                        amplitudes instead of the Stokes I gain amplitudes
                        (Optional) [default: False]
  --fit_to_vis          Fit primary beams to the cross-correlation visibility
                        amplitudes instead of the antenna gain amplitudes
                        (Optional) [default: False]
  --apply_mask          Apply mask (Optional) [default: False]
  --use_source_offset_column  Read on-sky offsets in cross-elevation and
                        elevation from the SOURCE_OFFSET column of the pointing
                        sub-tables? If False, antenna pointings in azimuth
                        and elevation are read from the TARGET column of
                        the pointing table and the on-sky offsets in
                        cross-elevation and elevation are computed thereafter
                        (Optional) [default: False]
  --rfi_file=FILE       RFI file (Optional). If not None, the expected static
                        RFI mask is an HDF5 file located in the SKA Telescope
                        Model Data Repository. Refer to the documentation for
                        how to upload the file to the repository for use if not
                        already in there
  --results_dir=None    Directory where the results (data products) need to be
                        saved (Optional). If None, they are stored in the
                        directory containing the Measurement Sets
  --start_freq=None     Start frequency in MHz (Optional)
  --end_freq=None       End frequency in MHz (Optional)
  --bw_factor           Beamwidth factor for the horizontal and vertical
                        co-polarisations respectively. The constant of
                        proportionality, k when computing theoretical
                        beamwidth (k*lambda/D) [default: 0.976, 1.098]
  --num_chunks=<int>    Number of frequency chunks for calibration
                        [default: 16]
  --thresh_width=<float>  The maximum ratio of the fitted to expected beamwidth
                        [default: 1.15]
  --eb_id=None          Execution block ID (Processing via SDP only)
  --num_scans=<int>     How many scans we expect for a single pointing
                        observation [default: 5] (Processing via SDP only)
  --use_modelvis        Use modelvis for gain solver (Optional)
                        Needs ska-sdp-func to be installed.
                        [default: False]
"""
import asyncio
import datetime
import logging
import os
import sys
import time

from docopt import docopt

from ska_sdp_wflow_pointing_offset.array_data_func import (
    ExtractPerScan,
    create_offset_pointing_table,
    fitted_offsets_weighted_average,
)
from ska_sdp_wflow_pointing_offset.beam_fitting import SolveForOffsets
from ska_sdp_wflow_pointing_offset.plotting.gain_amp_plot import plot_gain_amp
from ska_sdp_wflow_pointing_offset.plotting.gaussian_fits_plot import (
    GenerateGaussianFitsPlot,
)
from ska_sdp_wflow_pointing_offset.plotting.on_sky_offsets_plot import (
    plot_on_sky_offsets,
)
from ska_sdp_wflow_pointing_offset.plotting.pointing_offsets_plot import (
    plot_offsets,
)
from ska_sdp_wflow_pointing_offset.plotting.vis_amp_plot import plot_vis_amp
from ska_sdp_wflow_pointing_offset.pointing_pipeline_sdp import SDPPipeline
from ska_sdp_wflow_pointing_offset.pointing_pipeline_standalone import (
    StandAlonePipeline,
)
from ska_sdp_wflow_pointing_offset.read_data import read_batch_visibilities
from ska_sdp_wflow_pointing_offset.utils import (
    average_output_offsets,
    create_empty_array,
    get_off_source_antenna_index,
    get_parameters_for_gaussian_fits_plot,
    handle_exception,
    merge_output_offsets,
)

log_level = os.getenv("LOG_LEVEL", "INFO")
log = logging.getLogger("ska-sdp-pointing-offset")
log.setLevel(getattr(logging, log_level.upper()))

RUN_AS_SCRIPT = os.getenv("SDP_PROCESSING_SCRIPT") == "True"


def main():
    """
    Run pointing offset calibration routines
    """
    args = docopt(__doc__)

    if RUN_AS_SCRIPT:
        pointing_pipeline = SDPPipeline(args)

        pointing_pipeline.update_processing_block_state([])
        log.info("Initial processing block state added.")

        # Currently set as infinite loop
        while True:
            # this will only move on if the scan_type is 'pointing'
            pointing_pipeline.wait_for_pointing_scan_type()
            log.info("Current scan type is pointing, waiting for scans.")

            pointing_pipeline.set_scan_ids_and_last_index()
            log.info(
                "The following scan IDs are to be processed for pointing: %s",
                pointing_pipeline.scan_ids,
            )

            pointing_pipeline.create_metadata_file()
            log.info("Initial metadata file created.")

            # Run the pipeline after update last_index
            try:
                compute_offset(pointing_pipeline)
                log.info("Finished processing this batch of pointing scans.")
                scan_list = [
                    {
                        "scan_ids": pointing_pipeline.scan_ids,
                        "status": "success",
                    }
                ]
                pointing_pipeline.update_processing_block_state(scan_list)
                log.info(
                    "Updated processing block state "
                    "for successfully processed scans: %s",
                    pointing_pipeline.scan_ids,
                )
                pointing_pipeline.update_data_flow_state(
                    None, kind="data-product"
                )
                pointing_pipeline.update_data_flow_state(
                    None, kind="data-queue"
                )
                log.info("Updated data flow state.")

            # pylint: disable-next=broad-exception-caught
            except Exception as err:
                traceback_frames = sys.exc_info()[2]
                exception_data = handle_exception(
                    err,
                    traceback_frames,
                    pointing_pipeline.scan_ids,
                )
                # Update data product & queue state
                pointing_pipeline.update_data_flow_state(
                    exception_data, kind="data-product"
                )
                pointing_pipeline.update_data_flow_state(
                    exception_data, kind="data-queue"
                )
                scan_list = [
                    {
                        "scan_ids": pointing_pipeline.scan_ids,
                        "status": "failed",
                    }
                ]
                pointing_pipeline.update_processing_block_state(scan_list)

                try:
                    pointing_pipeline.configure_kafka_from_dataqueue()
                    data = create_empty_array(
                        pointing_pipeline.receptors or [""]
                    )
                    log.info(
                        "Sending empty offsets data to Kafka "
                        "queue due to error."
                    )
                    asyncio.run(pointing_pipeline.send_data_to_kafka(data))

                # pylint: disable-next=broad-exception-caught
                except Exception as error:
                    traceback_frames = sys.exc_info()[2]
                    exception_data = handle_exception(
                        error,
                        traceback_frames,
                        pointing_pipeline.scan_ids,
                    )
                    # Update data queue state
                    pointing_pipeline.update_data_flow_state(
                        exception_data, kind="data-queue"
                    )
                    scan_list = [
                        {
                            "scan_ids": pointing_pipeline.scan_ids,
                            "status": "failed",
                        }
                    ]
                    pointing_pipeline.update_processing_block_state(scan_list)

            finally:
                pointing_pipeline.close()

    else:
        # Run pipeline independently
        pointing_pipeline = StandAlonePipeline(args)
        compute_offset(pointing_pipeline)


# pylint: disable=too-many-branches
def compute_offset(pipeline_class):
    """
    Main routine to run the pointing offset pipeline once
    with a list of scans to process.

    The pipeline reads visibilities from a measurement set and optionally
    applies RFI mask and selects some frequency ranges, and
    fits primary beams to the "RFI-free" visibilities to
    obtain the pointing offsets.

    :param pipeline_class: initialized StandAlonePipeline or PipelineInSDP
    """

    begin = time.time()

    log.info(
        "Beamwidth factor: %f %f",
        pipeline_class.beamwidth_factor[0],
        pipeline_class.beamwidth_factor[1],
    )
    log.info(
        "Maximum fitted beamwidth to expected beamwidth: %f",
        pipeline_class.thresh_width,
    )
    log.info(
        "Number of frequency chunks for calibration: %d",
        pipeline_class.num_chunks,
    )
    log.info(
        "Common prefix used in outputs and plots is:  %s",
        pipeline_class.common_prefix,
    )
    log.info("Looking for data in %s directory", pipeline_class.msdir)
    log.info(
        "The following MeasurementSets will be processed: %s",
        pipeline_class.ms_files,
    )

    (
        vis_lists,
        on_sky_offsets_lists,
        ants_lists,
        reference_mjd_lists,
        reference_commanded_azel_lists,
        track_duration_lists,
    ) = read_batch_visibilities(
        pipeline_class.ms_files,
        pipeline_class.apply_mask,
        pipeline_class.use_source_offset_column,
        pipeline_class.rfi_file,
        pipeline_class.start_freq,
        pipeline_class.end_freq,
        pipeline_class.fit_to_vis,
        pipeline_class.num_chunks,
    )

    if pipeline_class.fit_to_vis:
        if pipeline_class.fit_to_sep_pol:
            # To Do: To be implemented in a separate ticket
            raise NotImplementedError(
                "Fitting to parallel-hands of the "
                "visibility amplitudes not yet supported!"
            )
        x_per_scan_list = []
        y_per_scan_list = []
        reference_mjd = []
        reference_commanded_azel = []
        output_offsets_list = []
        fitted_beams_list = []
        for vis_num, vis_list in enumerate(vis_lists):
            # Get the datasets required for the fitting and fit for the
            # pointing offsets
            on_sky_offsets_list = on_sky_offsets_lists[vis_num]
            ants = ants_lists[vis_num]
            reference_mjd.append(reference_mjd_lists[vis_num])
            reference_commanded_azel.append(
                reference_commanded_azel_lists[vis_num]
            )
            x_per_scan, y_per_scan, stddev_per_scan, frequency_per_chunk = (
                ExtractPerScan(
                    vis_list,
                    on_sky_offsets_list,
                    ants,
                    pipeline_class.fit_to_sep_pol,
                ).from_vis()
            )

            fitted_beams = SolveForOffsets(
                x_per_scan,
                y_per_scan,
                stddev_per_scan,
                frequency_per_chunk,
                pipeline_class.beamwidth_factor,
                ants,
                pipeline_class.thresh_width,
            ).fit_to_vis()

            # Compute the weighted-average of the valid pointing offsets
            output_offsets = fitted_offsets_weighted_average(
                ants,
                fitted_beams,
                fit_to_vis=True,
            )
            x_per_scan_list.append(x_per_scan)
            y_per_scan_list.append(y_per_scan)
            output_offsets_list.append(output_offsets)
            fitted_beams_list.append(fitted_beams)

        # Merge the output offsets
        output_offsets = merge_output_offsets(output_offsets_list)

        if RUN_AS_SCRIPT:
            # Extract the on-sky offsets for just one antenna and send to the
            # PointingTable for the discrete offsets used in the reference
            # reference-pointing observation to be extracted. Note that the
            # same discrete offsets are used in both observations
            one_antenna_x_per_scan = x_per_scan[
                :, get_off_source_antenna_index(x_per_scan)[0]
            ]
        else:
            # Construct Gaussian fits parameters into the right dimensions
            # for plotting purposes
            x_per_scan, y_per_scan, fitted_beams = (
                get_parameters_for_gaussian_fits_plot(
                    x_per_scan_list, y_per_scan_list, fitted_beams_list
                )
            )

            # Extract the on-sky offsets for just one antenna and send to the
            # PointingTable for the discrete offsets used in the reference
            # reference-pointing observation to be extracted. Note that the
            # same discrete offsets are used in both observations
            one_antenna_x_per_scan = x_per_scan[:, 0]
    else:
        reference_mjd = reference_mjd_lists[0]
        reference_commanded_azel = reference_commanded_azel_lists[0]
        vis_list = vis_lists[0]
        on_sky_offsets_list = on_sky_offsets_lists[0]
        ants = ants_lists[0]

        # Solve for the complex gains
        (
            x_per_scan,
            y_per_scan,
            stddev_per_scan,
            frequency_per_chunk,
        ) = ExtractPerScan(
            vis_list,
            on_sky_offsets_list,
            ants,
            pipeline_class.fit_to_sep_pol,
        ).from_gains(
            pipeline_class.use_modelvis
        )

        # Fit the voltage beams
        if pipeline_class.fit_to_sep_pol:
            output_offsets_list = []
            fitted_beams = []
            for i in range(y_per_scan.shape[0]):
                if i == 0:
                    log.info(
                        "Fitting voltage beams to Stokes %s gain "
                        "amplitudes...",
                        vis_list[0][0].polarisation.data[0],
                    )
                else:
                    log.info(
                        "Fitting voltage beams to Stokes %s gain "
                        "amplitudes...",
                        vis_list[0][0].polarisation.data[-1],
                    )

                # Fit the beams and collect them
                beams = SolveForOffsets(
                    x_per_scan,
                    y_per_scan[i,],
                    stddev_per_scan[i,],
                    frequency_per_chunk,
                    pipeline_class.beamwidth_factor,
                    ants,
                    pipeline_class.thresh_width,
                ).fit_to_gains()
                fitted_beams.append(beams)

                # Compute the weighted average of the pointing offsets
                # in each frequency chunk
                output_offsets_pol = fitted_offsets_weighted_average(
                    ants,
                    beams,
                    fit_to_vis=False,
                )
                output_offsets_list.append(output_offsets_pol)

            # Compute a weighted average of the fitted offsets
            # for the parallel-hands polarisations
            output_offsets = average_output_offsets(output_offsets_list)
        else:
            fitted_beams = SolveForOffsets(
                x_per_scan,
                y_per_scan,
                stddev_per_scan,
                frequency_per_chunk,
                pipeline_class.beamwidth_factor,
                ants,
                pipeline_class.thresh_width,
            ).fit_to_gains()

            # Compute the weighted-average of the valid pointing offsets and
            # write them to a PointingTable
            output_offsets = fitted_offsets_weighted_average(
                ants,
                fitted_beams,
                fit_to_vis=False,
            )

        # Extract the on-sky offsets for just one antenna and send to the
        # PointingTable for the discrete offsets used in the reference
        # reference-pointing observation to be extracted
        one_antenna_x_per_scan = x_per_scan[:, 0]

    # Write the output offsets to a PointingTable
    pointing_table = create_offset_pointing_table(
        output_offsets,
        frequency_per_chunk,
        reference_mjd,
        reference_commanded_azel,
        vis_list,
        one_antenna_x_per_scan,
        track_duration_lists[0],
    )

    # Write the fitted offsets and their uncertainties to a CSV file
    pipeline_class.export_data(output_offsets, pointing_table, vis_list[0][0])

    # Generate diagnostic plots when running pipeline in standalone
    # mode only
    if not RUN_AS_SCRIPT:
        if pipeline_class.fit_to_vis:
            # Plot the visibility amplitudes for the two sets of
            # observations
            for i, y_per_scan in enumerate(y_per_scan_list):
                plot_vis_amp(
                    y_per_scan,
                    prefix=pipeline_class.common_prefix + f"group{i + 1}_",
                )

            # Plot the on-sky offsets for the two sets
            # observations
            plot_on_sky_offsets(
                x_per_scan_list,
                ants=ants,
                prefix=pipeline_class.common_prefix,
            )
        else:
            # Plot the on-sky-offsets
            plot_on_sky_offsets(
                x_per_scan,
                ants=None,
                prefix=pipeline_class.common_prefix,
            )

            # Plot the gain amplitudes
            plot_gain_amp(
                y_per_scan,
                vis_list[0][0].polarisation.data,
                pipeline_class.fit_to_sep_pol,
                prefix=pipeline_class.common_prefix,
            )

        # Send the PointingTable so the fitted pointing
        # offsets and other fitted parameters can be
        # plotted for inspection
        plot_offsets(
            pointing_table,
            vis_list[0][0].polarisation.data,
            pipeline_class.fit_to_sep_pol,
            pipeline_class.fit_to_vis,
            prefix=pipeline_class.common_prefix,
        )

        # Generate the Gaussian fits plot
        gaussian_fits_plot = GenerateGaussianFitsPlot(
            x_per_scan,
            y_per_scan,
            frequency_per_chunk,
            ants,
            fitted_beams,
            pipeline_class.common_prefix,
        )
        if pipeline_class.fit_to_sep_pol:
            gaussian_fits_plot.generate_parallel_hands_pol_amp_fits(
                vis_list[0][0].polarisation.data
            )
        else:
            gaussian_fits_plot.generate_total_intensity_amp_fits()

    end = time.time()
    log.info(
        "\nProcess finished in %s", (datetime.timedelta(seconds=end - begin))
    )


if __name__ == "__main__":
    main()
