"""
Pointing offset calibration pipeline - standalone version.
"""

import glob
import logging
import os

from ska_sdp_datamodels.calibration import export_pointingtable_to_hdf5

from ska_sdp_wflow_pointing_offset import export_pointing_offset_data
from ska_sdp_wflow_pointing_offset.pointing_pipeline import (
    DATA_COMMENT,
    InitPipeline,
)

LOG = logging.getLogger("ska-sdp-pointing-offset")


class StandAlonePipeline(InitPipeline):
    """
    Run the pointing offset calibration pipeline standalone.

    Exception raised if:
    - --msdir is empty

    Implements:
    - common_prefix
    - export_data
    """

    @property
    def results_dir(self):
        """Directory where results are stored."""
        return self._results_dir_base

    @property
    def msdir(self):
        """The path of measurement sets."""
        if not self._ms_dir_base:
            raise ValueError(
                "Directory containing measurement sets is required!"
            )
        return self._ms_dir_base

    @property
    def ms_files(self):
        """Collect measurement file paths."""
        if not self._ms_files:
            self._ms_files = sorted(glob.glob(self.msdir + "*.ms"))
            # pylint: disable=duplicate-code
            if not self._ms_files:
                raise FileNotFoundError(
                    f"Given directory ({self.msdir}) doesn't contain "
                    "the right (or any) MeasurementSets"
                )

        return self._ms_files

    @property
    def common_prefix(self):
        """Set the common_prefix for saving data."""
        if self._common_prefix is None:
            # prefix is not the whole path just the file prefix
            prefix = (
                os.path.commonprefix(self.ms_files).split("/")[-1].rstrip("-")
            )

            if not prefix:
                prefix = "test_"

            else:
                if not prefix.endswith("_"):
                    prefix += "_"

            if self.results_dir:
                self._common_prefix = os.path.join(
                    self.results_dir,
                    prefix,
                )
            else:
                self._common_prefix = os.path.join(self.msdir, prefix)

        return self._common_prefix

    def export_data(self, output_offsets, pointing_table, visibility):
        """
        Perform a set of data export operations:
            1. write data into .txt file
            2. write data into .hdf5 file
        """
        # pylint: disable=duplicate-code
        LOG.info("Writing fitted parameters and computed offsets to file...")
        results_file = self.common_prefix + "pointing_offsets.txt"
        export_pointing_offset_data(results_file, output_offsets, visibility)
        LOG.info(
            "Fitted parameters and computed offsets written to %s",
            results_file,
        )

        results_file_hdf = results_file.replace(".txt", ".hdf5")
        LOG.info("Write PointingTable into HDF5 file %s", results_file_hdf)
        export_pointingtable_to_hdf5(
            pointing_table, results_file_hdf, **DATA_COMMENT
        )
