"""
Contains the following gain calibration (G-terms) functions:

1. Solves for the complex un-normalised antenna gains (G-terms)

2. Optionally calculates model visibility when fitting beams to
   the gain amplitudes
"""

import numpy
from ska_sdp_datamodels.sky_model import SkyComponent
from ska_sdp_func_python.calibration import solve_gaintable
from ska_sdp_func_python.imaging import dft_skycomponent_visibility


def _get_modelvis(source, vis):
    """
    Get model visibility.

    :param source: Target SkyCoord
    :param vis: Visibility object

    :return: Visibility object
    """
    model_vis = vis.copy(deep=True, zero=True)
    flux = numpy.zeros(
        [len(model_vis.frequency.data), model_vis.visibility_acc.npol]
    )
    flux[:, 0] = 1.0

    original_components = [
        SkyComponent(
            direction=source,
            flux=flux,
            polarisation_frame=model_vis.visibility_acc.polarisation_frame,
            frequency=model_vis.frequency.data,
        )
    ]
    return dft_skycomponent_visibility(model_vis, original_components)


def compute_gains(vis, source=None):
    """
    Solves for the antenna gains for the parallel-hands only.

    :param vis: The list of observed Visibility
    :source: Target SkyCoord

    :return: GainTable containing solutions
    """
    gt_list = []
    for visibility in vis:
        if source is not None:
            modelvis = _get_modelvis(source, visibility)
        else:
            modelvis = None
        gt_list.append(
            solve_gaintable(
                vis=visibility,
                modelvis=modelvis,
                gain_table=None,
                phase_only=False,
                niter=200,
                tol=1e-06,
                crosspol=False,
                normalise_gains=None,
                solver="gain_substitution",
                jones_type="G",
                timeslice=None,
                refant=0,
            )
        )

    return gt_list


def extract_parallel_hand_gains(gaintable):
    """
    Extracts the parallel-hands polarisation gain solutions

    :param gaintable: The gain solutions for each scan with dimensions
        (time, antenna, frequency, receptor1, receptor2)

    :return: Gain amplitudes and weights for each scan
    """
    gt_amp = numpy.abs(gaintable.gain.data)
    gt_amp = numpy.dstack((gt_amp[:, :, :, 0, 0], gt_amp[:, :, :, 1, 1]))

    gt_weights = gaintable.weight.data
    gt_weights = numpy.dstack(
        (
            gt_weights[:, :, :, 0, 0],
            gt_weights[:, :, :, 1, 1],
        )
    )

    return gt_amp, gt_weights
