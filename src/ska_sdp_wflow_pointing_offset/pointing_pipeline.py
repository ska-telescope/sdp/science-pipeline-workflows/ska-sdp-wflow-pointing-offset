"""
Pointing offset calibration pipeline implementation.
This class implements the common interfaces.
"""

import logging

LOG = logging.getLogger("ska-sdp-pointing-offset")

DATA_COMMENT = {
    "time": "The middle timestamp in MJD of the central scan of "
    "the pointing observation. This corresponds to the time at "
    "which the commanded_pointing is calculated. If the central "
    "scan is not found then commanded_pointing cannot be "
    "calculated and the median of the middle timestamps from "
    "all scans is used. In the two-dish mode scenario, this "
    "timestamp for the two sets of observations are stored",
    "frequency": "The central frequency in Hz if fitting to gains "
    "and frequency at the higher end of the band if fitting to "
    "visibilities",
    "weight": "The inverse square of the standard error in the "
    "fitted pointing values in radians",
    "pointing": "The pointing offsets in cross-elevation and "
    " elevation in radians for all antennas in units of radians",
    "expected_width": "The theoretical voltage beam sizes for all "
    "antennas in radians in the horizontal and "
    "vertical co-polarisations",
    "fitted_width": "The fitted voltage beam sizes for all antennas "
    "in radians in the horizontal and vertical co-polarisations",
    "fitted_width_std": "The standard error on the fitted_width in radians",
    "fitted_height": "The fitted Gaussian height for all antennas "
    "in arbitrary units",
    "fitted_height_std": "The standard error on the "
    "fitted_height in arbitrary units",
    "band_type": "Observing band",
    "scan_mode": "Pointing observation mode",
    "track_duration": "How long each scan position was tracked for in seconds",
    "discrete_offset": "Discrete offset of each scan from the central scan, "
    "in degrees (xel-el)",
    "commanded_pointing": "The commanded pointings at the middle "
    "timestamp of the central scan of the pointing observation in radians. "
    "In the two-dish mode scenario, these commanded pointings for the two "
    "sets of observations are stored",
}


# pylint: disable-next=too-few-public-methods,too-many-instance-attributes
class InitPipeline:
    """
    Class that implements the common functionality between the
    StandAlone pipeline and the SDP-based pipeline.

    :param cli_args: command line arguments read by docopt from
        src/ska_sdp_wflow_pointing_offset/pointing_offset_cli.py

    For a full list and description, see the CLI docs in the file.

    Exception raised if:
    - --num_chunks is 2

    This class is not to be used on its own. Instead, use
    one of the subclasses:
    - ska_sdp_wflow_pointing_offset.pointing_offset_pipeline.StandAlonePipeline
    - ska_sdp_wflow_pointing_offset.pointing_offset_pipeline_sdp.PipelineInSDP
    """

    def __init__(self, cli_args):
        # data
        self._ms_dir_base = cli_args.get("--msdir")
        self._results_dir_base = cli_args["--results_dir"]
        self.rfi_file = cli_args["--rfi_file"]

        # frequency information
        self.start_freq = cli_args["--start_freq"]
        self.end_freq = cli_args["--end_freq"]
        self.num_chunks = int(cli_args["--num_chunks"])
        if self.num_chunks == 2:
            raise ValueError(
                "num_chunks cannot be 2 for num_chunks > 1. "
                "This is because the top and bottom band "
                "solutions/freqs/weights are discarded."
            )

        # fitting information
        self._bw_factor = cli_args.get("<bw_factor>")
        self.thresh_width = float(cli_args["--thresh_width"])

        # pipeline feature switches
        self.fit_to_sep_pol = cli_args["--fit_to_sep_pol"] is True
        self.fit_to_vis = cli_args["--fit_to_vis"] is True
        self.apply_mask = cli_args["--apply_mask"] is True
        self.use_source_offset_column = (
            cli_args["--use_source_offset_column"] is True
        )
        self.use_modelvis = cli_args["--use_modelvis"] is True

        # properties
        self._beamwidth_factor = None
        self._common_prefix = None
        self._ms_files = None

    @property
    def beamwidth_factor(self):
        """Beamwidth factor property."""
        if self._beamwidth_factor is None:

            def _safe_float(number):
                return float(number)

            if self._bw_factor:
                self._beamwidth_factor = list(
                    map(_safe_float, self._bw_factor)
                )
                if len(self._beamwidth_factor) == 1:
                    self._beamwidth_factor.append(self._beamwidth_factor[0])
            else:
                # We would use the values for the MeerKAT
                # as known in April 2023.
                self._beamwidth_factor = [0.976, 1.098]

        return self._beamwidth_factor
