"""Plot the visibility amplitudes for each discrete pointing scan"""

import matplotlib.pyplot as plt
import numpy

from .plot_exception_handler import exception_handler


# pylint: disable=duplicate-code
@exception_handler
def plot_vis_amp(amplitudes, prefix="test"):
    """
    Plot visibility amplitude for each scan over number of
    antennas with one curve per frequency chunk.

    :param amplitudes: Visibility amplitudes with dimensions
        (nants, num_chunks, nscans). If num_chunks=1, then
        has dimension (nants, nscans)
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """
    nants = amplitudes.shape[0]
    try:
        nscans = amplitudes.shape[2]
        num_chunks = amplitudes.shape[1]

    except IndexError:
        # If num_chunks=1, then amplitudes have two dimensions
        nscans = amplitudes.shape[1]
        num_chunks = 1
        amplitudes = amplitudes.reshape((nants, 1, nscans))

    fig, axes = plt.subplots(
        1, nscans, layout="constrained", sharey=True, figsize=(16, 5)
    )
    for i in range(nscans):
        axes[i].set_title(f"Scan {i+1}")
        plot_x = numpy.arange(nants)
        for chunk in range(num_chunks):
            plot_y = numpy.abs(amplitudes[:, chunk, i])
            axes[i].scatter(plot_x, plot_y, marker=".", label=f"Band {chunk}")

    axes[0].set_ylabel("Visibility amplitude")

    fig.supxlabel("Number of antennas")
    fig.subplots_adjust(bottom=0.2)
    # Plot legend (optionally shown)
    # Only turn it on if num_chunks is small
    # Otherwise plot is too crowded
    if num_chunks < 5:
        handles, labels = fig.axes[-1].get_legend_handles_labels()
        fig.legend(
            handles,
            labels,
            bbox_to_anchor=(0.5, 0.16),
            ncol=num_chunks,
            loc="lower center",
            bbox_transform=fig.transFigure,
        )

    plt.savefig(prefix + "vis_amplitude.png")
    plt.cla()
