"""Plots the on-sky offsets in cross-elevation and elevation"""

import matplotlib.pyplot as plt

from .plot_exception_handler import exception_handler


@exception_handler
def plot_on_sky_offsets(on_sky_offsets, ants, prefix="test"):
    """
    Plot the on-sky offsets which are the position coordinates
    for the beam fitting routine

    :param on_sky_offsets: The commanded pointings relative to the target
        in degrees in the reference pointing observation in xEl-El
        coordinates with dimensions (nscans, nants, 2)
    :param ants: A list of Katpoint Antenna objects [nants]
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """
    plt.figure(figsize=(18, 7), layout="constrained")
    if ants is None:
        plt.scatter(on_sky_offsets[:, :, 0], on_sky_offsets[:, :, 1])
        plt.title("Commanded pointings relative to target")
        plt.ylabel("On-sky offsets in elevation (degrees)")
    else:
        # Observation 1
        plt.suptitle("Commanded pointings relative to target")
        plt.subplot(221)
        plt.scatter(
            on_sky_offsets[0][:, 0, 0],
            on_sky_offsets[0][:, 0, 1],
            marker="o",
            label=ants[0].name,
        )
        plt.ylabel("On-sky offsets in elevation (degrees)")
        plt.legend()

        plt.subplot(222)
        plt.scatter(
            on_sky_offsets[0][:, 1, 0],
            on_sky_offsets[0][:, 1, 1],
            marker="o",
            label=ants[1].name,
        )
        plt.legend()

        # Observation 2
        plt.subplot(223)
        plt.scatter(
            on_sky_offsets[1][:, 0, 0],
            on_sky_offsets[1][:, 0, 1],
            marker="o",
            label=ants[0].name,
        )
        plt.ylabel("On-sky offsets in elevation (degrees)")
        plt.xlabel("On-sky offsets in cross-elevation (degrees)")
        plt.legend()

        plt.subplot(224)
        plt.scatter(
            on_sky_offsets[1][:, 1, 0],
            on_sky_offsets[1][:, 1, 1],
            marker="o",
            label=ants[1].name,
        )
        plt.legend()

    plt.xlabel("On-sky offsets in cross-elevation (degrees)")
    plt.tight_layout()
    plt.savefig(prefix + "on_sky_offsets.png")
    plt.cla()
