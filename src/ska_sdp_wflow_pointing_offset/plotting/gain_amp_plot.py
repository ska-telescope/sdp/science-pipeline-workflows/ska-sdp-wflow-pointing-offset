"""Plots the gain amplitudes for each discrete pointing scan"""

import matplotlib.pyplot as plt
import numpy

from .plot_exception_handler import exception_handler


class PlotGainAmpClass:
    """
    Class for plotting the gain amplitudes at each discrete offset pointing

    :param gain_sol: Gain amplitudes for all antennas for all scans with
        dimensions (nants, num_chunks, nscans) where num_chunks is the
        number of usable frequency chunks. Note that `num_chunks` equal to
        2 is not allowed since the bottom and top part of the band amplitudes
        are discarded.
        When fitting to the parallel-hands polarisation gain amplitudes, the
        amplitudes have dimensions (2, nants, num_chunks, nscans)
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """

    def __init__(self, gain_sol, prefix="test"):
        self.gain_sol = gain_sol
        self.prefix = prefix
        if self.gain_sol.ndim == 3:
            # stokesI gain amplitudes
            self.nants, self.num_chunks, self.nscans = self.gain_sol.shape
        else:
            #  Stokes (XX, YY) or (RR, LL) gain amplitudes
            self.npols, self.nants, self.num_chunks, self.nscans = (
                gain_sol.shape
            )

    def avg_pol_amp(self):
        """
        Direct scatter plot of the StokesI gain amplitudes at each
        discrete offset pointing
        """
        fig, axes = plt.subplots(
            1, self.nscans, layout="constrained", sharey=True, figsize=(16, 5)
        )
        for i in range(self.nscans):
            axes[i].set_title(f"Scan {i + 1}")
            plot_x = numpy.arange(self.nants)
            for chunk in range(self.num_chunks):
                plot_y = numpy.abs(self.gain_sol[:, chunk, i])
                axes[i].scatter(
                    plot_x, plot_y, marker=".", label=f"Band {chunk}"
                )

        axes[0].set_ylabel("Un-normalised StokesI G amplitude")
        fig.supxlabel("Number of dishes")
        fig.subplots_adjust(bottom=0.2)
        # Plot legend (optionally shown)
        # Only turn it on if num_chunks is small
        # Otherwise plot is too crowded
        if self.num_chunks < 5:
            handles, labels = fig.axes[-1].get_legend_handles_labels()
            fig.legend(
                handles,
                labels,
                bbox_to_anchor=(0.5, 0.16),
                ncol=self.num_chunks,
                loc="lower center",
                bbox_transform=fig.transFigure,
            )

        plt.savefig(self.prefix + "gain_amplitude.png")
        plt.cla()

    def parallel_hands_amp(self, polarisation):
        """
        Direct scatter plot of the gain amplitudes of the parallel-hands
        polarisations at each discrete offset pointing scan

        :param polarisation: Polarisation products
        """
        fig, axes = plt.subplots(
            self.npols,
            self.nscans,
            layout="constrained",
            sharey=True,
            figsize=(16, 8),
        )
        for i in range(self.npols):
            for j in range(self.nscans):
                axes[i][j].set_title(f"Scan {j + 1}")
                plot_x = numpy.arange(self.nants)
                for chunk in range(self.num_chunks):
                    plot_y = numpy.abs(self.gain_sol[i, :, chunk, j])
                    axes[i][j].scatter(
                        plot_x, plot_y, marker=".", label=f"Band {chunk}"
                    )

        axes[0][0].set_ylabel(
            f"Un-normalised Stokes{polarisation[0]} G amplitude"
        )
        axes[1][0].set_ylabel(
            f"Un-normalised Stokes{polarisation[-1]} G amplitude"
        )
        fig.supxlabel("Number of dishes")
        fig.subplots_adjust(bottom=0.2)
        # Plot legend (optionally shown)
        # Only turn it on if num_chunks is small
        # Otherwise plot is too crowded
        if self.num_chunks < 5:
            handles, labels = fig.axes[-1].get_legend_handles_labels()
            fig.legend(
                handles,
                labels,
                bbox_to_anchor=(0.5, 0.16),
                ncol=self.num_chunks,
                loc="lower center",
                bbox_transform=fig.transFigure,
            )

        plt.savefig(self.prefix + "gain_amplitude.png")
        plt.cla()


@exception_handler
def plot_gain_amp(gain_sol, polarisation, fit_to_sep_pol=False, prefix="test"):
    """
     Direct scatter plot of the Stokes I or parallel-hands
     gain amplitudes at each discrete offset pointing scan over
     number of antennas with a curve per frequency chunk

     :param gain_sol: Gain solutions with dimensions (nants, num_chunks,
        nscans) when fitting to Stokes I gain amplitudes or (2, nants,
        num_chunks, nscans) when fitting to the parallel-hands gain
        amplitudes
    :param polarisation: Polarisation products
    :param fit_to_sep_pol: Fit primary beams to the parallel-hands gain
        amplitudes
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """
    gain_amp_plot_init = PlotGainAmpClass(gain_sol, prefix)
    if fit_to_sep_pol:
        gain_amp_plot_init.parallel_hands_amp(polarisation)
    else:
        gain_amp_plot_init.avg_pol_amp()
