"""Error handling function"""

import logging

LOG = logging.getLogger("ska-sdp-pointing-offset")


# pylint:disable=broad-exception-caught
def exception_handler(plot_func):
    """
    Decorator for functions with try except
    """

    def inner_function(*args, **kwargs):
        try:
            plot_func(*args, **kwargs)
        except Exception as err:
            LOG.error("Plotting failed with error(s): %s", str(err))

    return inner_function
