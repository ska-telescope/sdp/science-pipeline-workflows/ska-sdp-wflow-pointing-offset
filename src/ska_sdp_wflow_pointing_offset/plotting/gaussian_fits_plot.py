# pylint:disable=too-many-arguments,too-many-positional-arguments
"""
Plots the Gaussian fits to the gain or visibility
amplitudes from which the fitted parameters are
extracted
"""

import matplotlib.pyplot as plt
import numpy

from .plot_exception_handler import exception_handler


def _gauss_func(xparam, a_fac, mu_fac, sigma):
    """
    A standard Gaussian distribution
    (This is a utility function)

    :param xparam: The on-sky offsets in arcminutes
    :param a_fac: The amplitude of the Gaussian distribution
        in arbitrary units
    :param mu_fac: The centre of the Gaussian distribution
        in arcminutes
    :param sigma: The width of the Gaussian distribution
        in arcminutes

    :return: A Gaussian distribution with parameters
        described by a_fac, mu_fac, and sigma
    """
    return a_fac * numpy.exp(-((xparam - mu_fac) ** 2) / (2 * sigma**2))


def _check_amplitudes_dim(y_per_scan, fit_to_sep_pol=False):
    """
    Checks the dimensions of the visibility or gain amplitudes

    :param y_per_scan: Gain amplitudes of each antenna for all discrete
        pointings with dimensions (nants, num_chunks, nscans). Note that
        `num_chunks` equal to 2 is not allowed since the bottom and top part
        of the band amplitudes are discarded
    :param fit_to_sep_pol: Fit primary beams to the parallel-hands gain
        amplitudes
    """
    if fit_to_sep_pol:
        if y_per_scan.ndim != 4:
            raise ValueError(
                "The gain amplitudes should have dimensions 4 when "
                "fitting to the parallel-hands polarisation amplitudes!"
            )
    else:
        if y_per_scan.ndim != 3:
            raise ValueError(
                "The visibility or gain amplitudes should have dimensions 3 "
                "when fitting to the parallel-hands polarisation amplitudes!"
            )


def _discard_points_near_zero_on_sky_offsets(x_per_scan, y_per_scan):
    """
    Excludes visibility or gain amplitudes near approximately zero
    on-sky offsets. It ensures the on-sky offset corresponding to
    the maximum visibility or gain amplitude is not excluded

    :param x_per_scan: The commanded pointings relative to the target
        in degrees in the reference pointing observation in xEl-El
        coordinates with dimensions (nscans, 2)
    :param y_per_scan: Visibility or gain amplitudes of each antenna
        for each discrete pointing scan with dimension (nscans, )

    :returns: The x_per_scan and y_per_scan in cross-elevation and
        elevation excluding values near approximately zero on-sky
        offsets
    """
    non_zero_offsets = abs(x_per_scan) > 0.1 * abs(x_per_scan.min())
    max_gains_index = numpy.argmax(y_per_scan)
    if x_per_scan[:, 0][max_gains_index] == 0:
        non_zero_offsets[:, 0][max_gains_index] = 1
    elif x_per_scan[:, 1][max_gains_index] == 0:
        non_zero_offsets[:, 1][max_gains_index] = 1
    y_per_scan_xel = y_per_scan[non_zero_offsets[:, 0]]
    y_per_scan_el = y_per_scan[non_zero_offsets[:, 1]]
    x_per_scan_xel = x_per_scan[non_zero_offsets[:, 0], 0]
    x_per_scan_el = x_per_scan[non_zero_offsets[:, 1], 1]

    # Replace any empty list with values in the other
    # pointing direction
    if x_per_scan_xel.size == 0 or x_per_scan_el.size == 0:
        if x_per_scan_xel.size == 0:
            x_per_scan_xel = x_per_scan_el
        else:
            x_per_scan_el = x_per_scan_xel

    if y_per_scan_xel.size == 0 or y_per_scan_el.size == 0:
        if y_per_scan_xel.size == 0:
            y_per_scan_xel = y_per_scan_el
        else:
            y_per_scan_el = y_per_scan_xel

    return x_per_scan_xel, x_per_scan_el, y_per_scan_xel, y_per_scan_el


def _discard_points_near_zero(x_per_scan, y_per_scan, antenna_index):
    """
    Discards zero visibility or gain amplitudes

    :param x_per_scan: The commanded pointings relative to the target
        in degrees in the reference pointing observation in xEl-El
        coordinates with dimensions (nscans, nants, 2)
    :param y_per_scan: Visibility or gain amplitudes of each antenna
        for all discrete pointings with dimensions (nants, num_chunks, nscans)
        for num_chunks less or equal to 3. For num_chunks > 3, the amplitudes
        have dimensions (nscans, )
    :param antenna_index: Index of antenna in list of katpoint Antennas

    :return: The x_per_scan and y_per_scan in cross-elevation and
        elevation excluding zero amplitudes
    """
    if y_per_scan.ndim == 1:
        # Discard zero gain amplitudes
        nonzero_gains_bool = y_per_scan > 0.0
        y_per_scan = y_per_scan[nonzero_gains_bool]
        x_per_scan = x_per_scan[nonzero_gains_bool, antenna_index]
    else:
        if y_per_scan.ndim == 3 or y_per_scan.shape[1] == 3:
            # Discard zero gain amplitudes
            if y_per_scan.shape[1] == 3:
                # If user specified num_chunks=3.
                # If user specified num_chunks=3. We discard
                # the top and bottom part of the band so we
                #  use the middle data
                nonzero_gains_bool = y_per_scan[antenna_index, 1] > 0.0
                y_per_scan = y_per_scan[antenna_index, 1, nonzero_gains_bool]
            else:
                # If user-specified num_chunks=1
                nonzero_gains_bool = y_per_scan[antenna_index, 0] > 0.0
                y_per_scan = y_per_scan[antenna_index, 0, nonzero_gains_bool]
            x_per_scan = x_per_scan[nonzero_gains_bool, antenna_index]
        else:
            raise ValueError(
                "Unexpected number of dimensions of visibility or "
                "gain amplitudes!"
            )

    return x_per_scan, y_per_scan


class PlotStokesGaussianFits:
    """
    A direct plot of the Gaussian fits from which the pointing offsets
    are extracted.

    :param num_beams: Number of fitted beams
    :param ant_name: Antenna name
    :param fig_label: Parallel-hand polarisation label for the plot
    """

    def __init__(self, num_beams, ant_name, fig_label):
        self.num_beams = num_beams
        if self.num_beams % 3 == 0:
            nrow = 3
            ncol = int(self.num_beams // 3)
        elif self.num_beams % 2 == 0:
            nrow = 2
            ncol = int(self.num_beams // 2)
        else:
            nrow = 1
            ncol = self.num_beams
        self.fig, self.axis = plt.subplots(
            nrows=nrow,
            ncols=ncol,
            sharex=True,
            sharey=True,
            layout="constrained",
            figsize=(ncol * 4, nrow * 4),
        )
        self.fig.suptitle(
            f"Dish: {ant_name} - Pointing fitting results "
            f"from fitting to {fig_label} gain amplitudes",
            fontsize=16,
        )

    def plot_single_beam(
        self, beam_per_ant, updated_x_per_scan, updated_y_per_scan
    ):
        """
        Plots single fitted Gaussian beam. This is the case when `num_chunks`
        of 1 or 3 is used. For `num_chunks` of 3, the top and bottom
        visibility or gain amplitudes are discarded so the beams are fitted
        to the remaining one set of amplitudes

        :param beam_per_ant: Fitted beam per antenna. Contains the fitted
            parameters with the centre and width in degrees, and height in
            arbitrary units
        :param updated_x_per_scan: The on-sky offsets corresponding to the
            non-zero visibility or gain amplitudes with dimensions (nscans, 2)
        :param updated_y_per_scan: Non-zero visibility or gain amplitudes
            with dimensions (nscans, )
        """
        try:
            # If user-specified num_chunks=1
            fitted_centre = 60.0 * beam_per_ant.centre
            fitted_width = 60.0 * beam_per_ant.width
            fitted_height = beam_per_ant.height
        except AttributeError:
            # If user-specified num_chunks=3. We need
            # to plot only the data points at the middle
            # band because the beam were fitted to only
            # those
            fitted_centre = 60.0 * beam_per_ant[0].centre
            fitted_width = 60.0 * beam_per_ant[0].width
            fitted_height = beam_per_ant[0].height

        (
            x_per_scan_xel,
            x_per_scan_el,
            y_per_scan_xel,
            y_per_scan_el,
        ) = _discard_points_near_zero_on_sky_offsets(
            updated_x_per_scan, updated_y_per_scan
        )

        # Plot the data points: gain amplitudes vs discrete
        # offset pointings
        self.axis.scatter(
            60.0 * x_per_scan_xel,
            y_per_scan_xel,
            marker="x",
            color="g",
            label="G vs on-sky offset in Cross-el",
        )
        self.axis.scatter(
            60.0 * x_per_scan_el,
            y_per_scan_el,
            marker="+",
            color="m",
            label="G vs on-sky offset in El",
        )

        # Plot the Gaussian fits
        self.axis.plot(
            numpy.arange(
                60.0 * x_per_scan_xel.min(), 60.0 * x_per_scan_xel.max(), 0.1
            ),
            _gauss_func(
                numpy.arange(
                    60.0 * x_per_scan_xel.min(),
                    60.0 * x_per_scan_xel.max(),
                    0.1,
                ),
                fitted_height,
                fitted_centre[0],
                fitted_width[0] / 2.35482,
            ),
            "r",
            label=f"Fitted centre in CrossEl: {fitted_centre[0]:.4f}",
        )

        self.axis.plot(
            numpy.arange(
                60.0 * x_per_scan_el.min(), 60.0 * x_per_scan_el.max(), 0.1
            ),
            _gauss_func(
                numpy.arange(
                    60.0 * x_per_scan_el.min(), 60.0 * x_per_scan_el.max(), 0.1
                ),
                fitted_height,
                fitted_centre[1],
                fitted_width[1] / 2.35482,
            ),
            "b",
            label=f"Fitted centre in El: {fitted_centre[1]:.4f}",
        )

        self.axis.legend()
        self.axis.set_xlabel("On-sky Offset (arcmin)")

    def plot_multiple_beams(
        self,
        frequency,
        updated_x_per_scan,
        updated_y_per_scan,
        beam_per_ant,
        axis,
    ):
        """
        Plots multiple fitted Gaussian beams. This is the case when
        `num_chunks` greater than one (except 1, 2 or 3) is used.
        Note that `num_chunks` of 2 is not allowed since the top and
        bottom visibility or gain amplitudes are discarded so there
        will be no remaining amplitudes to fit the beams to

        :param frequency: The frequency in each frequency chunk in Hz
        :param updated_x_per_scan: The on-sky offsets corresponding to the
            non-zero visibility or gain amplitudes with dimensions (nscans, 2)
        :param updated_y_per_scan: Non-zero visibility or gain amplitudes
            with dimensions (nscans, )
        :param beam_per_ant: Fitted beam per antenna. Contains the fitted
            parameters with the centre and width in degrees, and height in
            arbitrary units
        :param axis: Axis to make plot
        """
        # for chunk in range(num_beams):
        fitted_centre = 60.0 * beam_per_ant.centre
        fitted_width = 60.0 * beam_per_ant.width
        fitted_height = beam_per_ant.height

        # Discard amplitudes near ~ zero offsets
        (
            x_per_scan_xel,
            x_per_scan_el,
            y_per_scan_xel,
            y_per_scan_el,
        ) = _discard_points_near_zero_on_sky_offsets(
            updated_x_per_scan, updated_y_per_scan
        )

        # Plot the Gaussian fits
        axis.plot(
            numpy.arange(
                60.0 * x_per_scan_xel.min(),
                60.0 * x_per_scan_xel.max(),
                0.1,
            ),
            _gauss_func(
                numpy.arange(
                    60.0 * x_per_scan_xel.min(),
                    60.0 * x_per_scan_xel.max(),
                    0.1,
                ),
                fitted_height,
                fitted_centre[0],
                fitted_width[0] / 2.35482,
            ),
            "r",
            label=f"Fitted centre in CrossEl: {fitted_centre[0]:.4f}",
        )
        axis.plot(
            numpy.arange(
                60.0 * x_per_scan_el.min(),
                60.0 * x_per_scan_el.max(),
                0.1,
            ),
            _gauss_func(
                numpy.arange(
                    60.0 * x_per_scan_el.min(),
                    60.0 * x_per_scan_el.max(),
                    0.1,
                ),
                fitted_height,
                fitted_centre[1],
                fitted_width[1] / 2.35482,
            ),
            "b",
            label=f"Fitted centre in El: {fitted_centre[1]:.4f}",
        )

        # Plot the data points: gain amplitudes vs discrete
        # offset pointings
        axis.scatter(
            60.0 * x_per_scan_xel,
            y_per_scan_xel,
            marker="x",
            color="g",
            label="G vs on-sky offset in CrossEl",
        )

        axis.scatter(
            60.0 * x_per_scan_el,
            y_per_scan_el,
            marker="+",
            color="m",
            label="G vs on-sky offset in El",
        )
        axis.legend()

        axis.set_title(f"{int(frequency / 1e6)} MHz", fontsize=12)
        self.fig.supylabel("Gaussian Height")
        self.fig.supxlabel("On-sky Offset (arcmin)")


class GenerateGaussianFitsPlot:
    """
    Generates the plots of the Gaussian fits to the visibility or gain
    amplitudes

    :param x_per_scan: The commanded pointings relative to the target
        in degrees in the reference pointing observation in xEl-El
        coordinates with dimensions (nscans, nants, 2)
    :param y_per_scan: Gain amplitudes of each antenna for all discrete
        pointings with dimensions (nants, num_chunks, nscans). Note that
        `num_chunks` equal to 2 is not allowed since the bottom and top
        part of the band amplitudes are discarded.
        When fitting to the parallel-hands polarisation gain amplitudes,
        the amplitudes have dimensions (2, nants, num_chunks, nscans)
    :param frequency_per_chunk: 1D array of the averaged-frequency in
        each chunk in Hz with equal length as `num_chunks`
    :param ants: List of katpoint Antenna objects [nants]
    :param fitted_beams: A dictionary of the fitted beams obtained from
        :func:`beam_fitting.py.SolveForOffsets.fit_to_gains` or
        :func:`beam_fitting.py.SolveForOffsets.fit_to_vis`
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """

    def __init__(
        self,
        x_per_scan,
        y_per_scan,
        frequency_per_chunk,
        ants,
        fitted_beams,
        prefix="test",
    ):
        self.x_per_scan = x_per_scan
        self.y_per_scan = y_per_scan
        self.frequency_per_chunk = frequency_per_chunk
        self.ants = ants
        self.fitted_beams = fitted_beams
        self.prefix = prefix

    @exception_handler
    def generate_total_intensity_amp_fits(self):
        """
        Generates the Gaussian fits plots when the beams are fitted to the
        Stokes I gain or visibility amplitudes
        """
        _check_amplitudes_dim(self.y_per_scan, fit_to_sep_pol=False)
        for antenna_index, antenna in enumerate(self.ants):
            beam_per_ant = self.fitted_beams[antenna.name]
            num_beams = self.y_per_scan.shape[1]
            gain_amp_per_ant = self.y_per_scan[antenna_index]
            gaussian_plots = PlotStokesGaussianFits(
                num_beams, antenna.name, fig_label="stokesI"
            )
            if num_beams == 1:
                # Discards zero visibility or gain amplitudes
                updated_x_per_scan, updated_y_per_scan = (
                    _discard_points_near_zero(
                        self.x_per_scan, self.y_per_scan, antenna_index
                    )
                )

                # Generate Gaussian fits plot
                gaussian_plots.plot_single_beam(
                    beam_per_ant, updated_x_per_scan, updated_y_per_scan
                )
            else:
                for chunk in range(num_beams):
                    # Discards zero visibility or gain amplitudes
                    updated_x_per_scan, updated_y_per_scan = (
                        _discard_points_near_zero(
                            self.x_per_scan,
                            gain_amp_per_ant[chunk,],
                            antenna_index,
                        )
                    )
                    if updated_y_per_scan.size == 0:
                        continue

                    # Generate Gaussian fits plot
                    gaussian_plots.plot_multiple_beams(
                        self.frequency_per_chunk[chunk],
                        updated_x_per_scan,
                        updated_y_per_scan,
                        beam_per_ant[chunk],
                        gaussian_plots.axis.flatten()[chunk],
                    )

            plt.tight_layout()
            plt.savefig(self.prefix + f"fitting_results_{antenna.name}.png")
            plt.cla()

    @exception_handler
    def generate_parallel_hands_pol_amp_fits(self, polarisation):
        """
        Generates the Gaussian fits plots when the beams are fitted to the
        parallel-hands polarisations gain amplitudes

        :param polarisation: Polarisation products
        """
        _check_amplitudes_dim(self.y_per_scan, fit_to_sep_pol=True)
        for beam_index, beam in enumerate(self.fitted_beams):
            if beam_index == 0:
                fig_label = f"stokes{polarisation[0]}"
            else:
                fig_label = f"stokes{polarisation[-1]}"

            for antenna_index, antenna in enumerate(self.ants):
                beam_per_ant = beam[antenna.name]
                num_beams = self.y_per_scan.shape[2]
                gain_amp_per_ant = self.y_per_scan[beam_index, antenna_index]
                gaussian_plots = PlotStokesGaussianFits(
                    num_beams, antenna.name, fig_label
                )
                if num_beams == 1:
                    # Discards zero visibility or gain amplitudes
                    updated_x_per_scan, updated_y_per_scan = (
                        _discard_points_near_zero(
                            self.x_per_scan,
                            self.y_per_scan[beam_index],
                            antenna_index,
                        )
                    )

                    # Generate Gaussian fits plot
                    gaussian_plots.plot_single_beam(
                        beam_per_ant, updated_x_per_scan, updated_y_per_scan
                    )
                else:
                    for chunk in range(num_beams):
                        # Discards zero visibility or gain amplitudes
                        updated_x_per_scan, updated_y_per_scan = (
                            _discard_points_near_zero(
                                self.x_per_scan,
                                gain_amp_per_ant[chunk,],
                                antenna_index,
                            )
                        )
                        if updated_y_per_scan.size == 0:
                            continue

                        # Generate Gaussian fits plot
                        gaussian_plots.plot_multiple_beams(
                            self.frequency_per_chunk[chunk],
                            updated_x_per_scan,
                            updated_y_per_scan,
                            beam_per_ant[chunk],
                            gaussian_plots.axis.flatten()[chunk],
                        )

                plt.tight_layout()
                plt.savefig(
                    self.prefix
                    + f"fitting_results_{fig_label}_{antenna.name}.png"
                )
                plt.cla()
