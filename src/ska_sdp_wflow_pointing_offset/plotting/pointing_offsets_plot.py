"""
Functions for plotting and visualisation of the pointing
offsets and other fitted parameters and their uncertainties
"""

import logging

import matplotlib.pyplot as plt
import numpy
from matplotlib.ticker import FormatStrFormatter

from .plot_exception_handler import exception_handler

LOG = logging.getLogger("ska-sdp-pointing-offset")


class PlotOffsetClass:
    """
    Direct scatter plot of the fitted pointing offsets and other parameters.

    :param pointing_table: The PointingTable hosting the pointing offsets and
        other fitted parameters and their uncertainties.
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """

    def __init__(self, pointing_table, prefix="test"):
        self.pointing_table = pointing_table
        self.prefix = prefix

    def avg_pol_vis(self):
        """
        Direct scatter plot of the fitted pointing offsets
        obtained from fitting the primary beams to the StokesI
        visibility amplitudes.
        """
        _, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(
            3, 2, figsize=(8, 12)
        )
        plt.suptitle(
            "Pointing offsets after fitting beams to StokesI vis amplitudes",
            fontsize=16,
        )

        # Cross-elevation offset and uncertainties
        # The uncertainties were converted to weights as
        # 1/square of uncertainties so we need to revert it
        ax1.errorbar(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.degrees(
                numpy.squeeze(self.pointing_table.pointing.data)[0, :, 0]
            )
            * 60.0,
            yerr=numpy.degrees(
                1.0
                / numpy.sqrt(
                    numpy.squeeze(self.pointing_table.weight.data)[0, :, 0]
                )
            )
            * 60.0,
            c="r",
            fmt="o",
        )
        ax1.set_ylabel("Cross-elevation Offset (arcmin)")
        ax1.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        # Elevation offset and uncertainties
        # The uncertainties were converted to weights as
        # 1/square of uncertainties so we need to revert it
        ax2.errorbar(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.degrees(
                numpy.squeeze(self.pointing_table.pointing.data)[0, :, 1]
            )
            * 60.0,
            yerr=numpy.degrees(
                1.0
                / numpy.sqrt(
                    numpy.squeeze(self.pointing_table.weight.data)[0, :, 1]
                )
            )
            * 60.0,
            c="b",
            fmt="o",
        )
        ax2.set_ylabel("Elevation Offset (arcmin)")
        ax2.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        # Expected H and V beamwidths
        ax3.plot(
            numpy.degrees(
                numpy.squeeze(self.pointing_table.expected_width.data)[0, :, 0]
            )
            * 60.0,
            "g*",
            label="Expected H width (arcmin)",
        )
        ax4.plot(
            numpy.degrees(
                numpy.squeeze(self.pointing_table.expected_width.data)[0, :, 1]
            )
            * 60.0,
            "mx",
            label="Expected V width (arcmin)",
        )

        # Fitted H and V beamwidths (and uncertainties)
        try:
            ax3.errorbar(
                numpy.arange(len(self.pointing_table.antenna.data)),
                numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width.data)[
                        0, :, 0
                    ]
                )
                * 60.0,
                yerr=numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width_std.data)[
                        0, :, 0
                    ]
                )
                * 60.0,
                c="brown",
                fmt=".",
                label="Fitted H width (arcmin)",
            )
            ax3.legend()

            ax4.errorbar(
                numpy.arange(len(self.pointing_table.antenna.data)),
                numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width.data)[
                        0, :, 1
                    ]
                )
                * 60.0,
                yerr=numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width_std.data)[
                        0, :, 1
                    ]
                )
                * 60.0,
                c="orange",
                fmt="+",
                label="Fitted V width (arcmin)",
            )
            ax4.legend()
        except ValueError:
            LOG.warning(
                "Negative standard deviations encountered in the fitted "
                "widths. Carefully inspect the fitted widths!"
            )

        # Tolerance on fitted beamwidths
        ax5.scatter(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.squeeze(self.pointing_table.fitted_width.data)[0, :, 0]
            / numpy.squeeze(self.pointing_table.expected_width.data)[0, :, 0],
            c="olive",
            marker="+",
            label="Tolerance on H width",
        )
        ax5.scatter(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.squeeze(self.pointing_table.fitted_width.data)[0, :, 1]
            / numpy.squeeze(self.pointing_table.expected_width.data)[0, :, 1],
            c="deeppink",
            marker="x",
            label="Tolerance on V width",
        )
        ax5.set_ylabel("Fitted Width / Expected Width")
        ax5.set_xlabel("Number of antennas")
        ax5.legend()

        # Fitted height and uncertainties
        ax6.errorbar(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.squeeze(self.pointing_table.fitted_height.data)[0],
            yerr=numpy.squeeze(self.pointing_table.fitted_height_std.data)[0],
            c="c",
            fmt="o",
        )
        ax6.set_ylabel("Fitted Height")
        ax6.set_xlabel("Number of antennas")

        plt.tight_layout()
        plt.savefig(self.prefix + "offsets.png")
        plt.cla()

    def avg_pol_gains(self, polarisation, fit_to_sep_pol=False):
        """
        Direct scatter plot of the fitted pointing offsets obtained from
        fitting the primary beams to the StokesI gain amplitudes.

        :param polarisation: Polarisation products
        :param fit_to_sep_pol: Fit primary beam to the parallel-hands gain
        """
        _, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(
            3, 2, figsize=(8, 12)
        )
        if fit_to_sep_pol:
            plt.suptitle(
                f"Pointing offsets after fitting beams to Stokes "
                f"{polarisation[0]} and {polarisation[-1]} G amplitudes",
                fontsize=16,
            )
        else:
            plt.suptitle(
                "Pointing offsets after fitting beams to StokesI G amplitudes",
                fontsize=16,
            )

        # Cross-elevation offset and uncertainties
        # The uncertainties were converted to weights as
        # 1/square of uncertainties so we need to revert it
        ax1.errorbar(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.degrees(
                numpy.squeeze(self.pointing_table.pointing.data)[:, 0]
            )
            * 60.0,
            yerr=numpy.degrees(
                1.0
                / numpy.sqrt(
                    numpy.squeeze(self.pointing_table.weight.data)[:, 0]
                )
            )
            * 60.0,
            c="r",
            fmt="o",
        )
        ax1.set_ylabel("Cross-elevation Offset (arcmin)")
        ax1.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        # Elevation offset and uncertainties
        # The uncertainties were converted to weights as
        # 1/square of uncertainties so we need to revert it
        ax2.errorbar(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.degrees(
                numpy.squeeze(self.pointing_table.pointing.data)[:, 1]
            )
            * 60.0,
            yerr=numpy.degrees(
                1.0
                / numpy.sqrt(
                    numpy.squeeze(self.pointing_table.weight.data)[:, 1]
                )
            )
            * 60.0,
            c="b",
            fmt="o",
        )
        ax2.set_ylabel("Elevation Offset (arcmin)")
        ax2.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        # Expected H ahd V beamwidths
        ax3.plot(
            numpy.degrees(
                numpy.squeeze(self.pointing_table.expected_width.data)[:, 0]
            )
            * 60.0,
            "g*",
            label="Expected H width (arcmin)",
        )
        ax4.plot(
            numpy.degrees(
                numpy.squeeze(self.pointing_table.expected_width.data)[:, 1]
            )
            * 60.0,
            "mx",
            label="Expected V width (arcmin)",
        )

        # Fitted H and V beamwidths and uncertainties
        try:
            ax3.errorbar(
                numpy.arange(len(self.pointing_table.antenna.data)),
                numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width.data)[:, 0]
                )
                * 60.0,
                yerr=numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width_std.data)[
                        :, 0
                    ]
                )
                * 60.0,
                c="brown",
                fmt=".",
                label="Fitted H width (arcmin)",
            )
            ax3.legend()

            ax4.errorbar(
                numpy.arange(len(self.pointing_table.antenna.data)),
                numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width.data)[:, 1]
                )
                * 60.0,
                yerr=numpy.degrees(
                    numpy.squeeze(self.pointing_table.fitted_width_std.data)[
                        :, 1
                    ]
                )
                * 60.0,
                c="orange",
                fmt="+",
                label="Fitted V width (arcmin)",
            )
            ax4.legend()
        except ValueError:
            LOG.warning(
                "Negative standard deviations encountered in the fitted "
                "widths. Carefully inspect the fitted widths!"
            )

        # Tolerance on fitted beamwidths
        ax5.scatter(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.squeeze(self.pointing_table.fitted_width.data)[:, 0]
            / numpy.squeeze(self.pointing_table.expected_width.data)[:, 0],
            c="olive",
            marker="+",
            label="Tolerance on H width",
        )

        ax5.scatter(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.squeeze(self.pointing_table.fitted_width.data)[:, 1]
            / numpy.squeeze(self.pointing_table.expected_width.data)[:, 1],
            c="deeppink",
            marker="x",
            label="Tolerance on V width",
        )

        ax5.set_ylabel("Fitted Width / Expected Width")
        ax5.set_xlabel("Number of antennas")
        ax5.legend()

        # Fitted height and uncertainties
        ax6.errorbar(
            numpy.arange(len(self.pointing_table.antenna.data)),
            numpy.squeeze(self.pointing_table.fitted_height.data),
            yerr=numpy.squeeze(self.pointing_table.fitted_height_std.data),
            c="c",
            fmt="o",
        )
        ax6.set_ylabel("Fitted Height")
        ax6.set_xlabel("Number of antennas")

        plt.tight_layout()
        plt.savefig(self.prefix + "offsets.png")
        plt.cla()


@exception_handler
def plot_offsets(
    pointing_table,
    polarisation,
    fit_to_sep_pol=False,
    fit_to_vis=False,
    prefix="test",
):
    """
    Direct scatter plot of the fitted pointing offsets and other parameters.
    The information are same as those in the output text file

    :param pointing_table: The PointingTable hosting the pointing offsets and
        other fitted parameters and their uncertainties.
    :param polarisation: Polarisation products
    :param fit_to_sep_pol: Fit primary beams to the parallel-hands gain
        amplitudes
    :param fit_to_vis: Fit primary beam to visibility amplitudes instead of
        gain amplitudes
    :param prefix: Unique plot name prefix (e.g. MeerKAT)
    """
    if numpy.isnan(numpy.squeeze(pointing_table.pointing.data)).all():
        LOG.warning("The offset array all NaN, no figure plotted!")
    else:
        plot_init = PlotOffsetClass(pointing_table, prefix)
        if fit_to_vis:
            # Plots the offsets when the beams are fitted to the
            # visibility amplitudes
            plot_init.avg_pol_vis()
        else:
            # Plots the offsets when the beams are fitted to the
            # gain amplitudes
            plot_init.avg_pol_gains(polarisation, fit_to_sep_pol)
