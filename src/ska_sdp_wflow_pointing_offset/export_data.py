"""
Functions for exporting pointing offset data to text file
"""

import logging
import os

import numpy
from ska_sdp_dataproduct_metadata import MetaData, ObsCore

from ska_sdp_wflow_pointing_offset.utils import (
    antenna_and_offsets_reorder,
    convert_dict_to_numpy_array,
)

LOG = logging.getLogger("ska-sdp-pointing-offset")


def export_pointing_offset_data(filename, offsets, visibility):
    """
    Writes the results of the pointing offset calibration
    to a text file.

    :param filename: Output filename
    :param offsets: A dictionary of the weighted-average pointing
        offsets output by :func:`array_data_func.weighted_average`
    :param visibility: Visibility object

    :return: True-Success, False-Failed
    """
    header = (
        "AntennaName,"
        "CrossElevationOffset,CrossElevationOffsetStd,"
        "ElevationOffset,ElevationOffsetStd,"
        "ExpectedWidthH,ExpectedWidthV,"
        "FittedWidthH,FittedWidthHStd,"
        "FittedWidthV,FittedWidthVStd,"
        "FittedHeight,FittedHeightStd"
    )
    if (
        offsets["antenna_names"]
        != visibility.attrs["configuration"].names.data
    ).all():
        LOG.info(
            "Antenna names and antenna names in Configuration "
            "do not match. Fixing it and their corresponding "
            "fitted parameters before writing to CSV file"
        )
        offsets = antenna_and_offsets_reorder(offsets)

    numpy.savetxt(
        filename,
        convert_dict_to_numpy_array(offsets),
        fmt="%s",
        header=header,
        delimiter=",",
    )


def create_metadata(metadata_file_path):
    """
    Create SDP dataproduct metadata file based on the
    ska-sdp-dataproduct-metadata library

    :param metadata_file_path: file (full path) to metadata file
    """
    if os.path.exists(metadata_file_path):
        metadata = MetaData(path=metadata_file_path)
        metadata.output_path = metadata_file_path

    else:
        metadata = MetaData()
        metadata.output_path = metadata_file_path
        # load basic data from processing block
        metadata.load_processing_block()

        # add obscore attributes
        data = metadata.get_data()
        data.obscore.dataproduct_type = ObsCore.DataProductType.POINTING
        data.obscore.calib_level = ObsCore.CalibrationLevel.LEVEL_0
        data.obscore.obs_collection = (
            f"{ObsCore.SKA}/"
            f"{ObsCore.SKA_MID}/"
            f"{ObsCore.DataProductType.POINTING}"
        )
        data.obscore.access_format = ObsCore.AccessFormat.HDF5
        data.obscore.facility_name = ObsCore.SKA
        data.obscore.instrument_name = ObsCore.SKA_MID

        try:
            metadata.write()
        except MetaData.ValidationError as err:
            LOG.error("Validation failed with error(s): %s", err.errors)
            raise err

    return metadata


def create_result_dir(result_directory, scan_ids):
    """Create scan based result directory

    :param result_directory: result directory from the args
    :param scan_ids: A list contains scan IDs

    :return: Created directory name
    """

    # Extracting the first and last scan IDs
    first_scan_id = scan_ids[0]
    last_scan_id = scan_ids[-1]

    # Creating the directory path
    directory_name = f"scan{first_scan_id}-{last_scan_id}"
    result_dir = os.path.join(result_directory, directory_name)
    return result_dir
