"""
Pointing offset calibration pipeline - version to be used in SDP.
"""

import asyncio
import logging
import os
from datetime import datetime
from itertools import groupby
from operator import itemgetter

from katpoint import lightspeed
from ska_sdp_config import Config, entity
from ska_sdp_datamodels.calibration import export_pointingtable_to_hdf5
from ska_sdp_dataproduct_metadata import MetaData
from ska_sdp_dataqueues import DataQueueProducer
from ska_sdp_dataqueues.schemas import PointingNumpyArray

from ska_sdp_wflow_pointing_offset import export_pointing_offset_data
from ska_sdp_wflow_pointing_offset.export_data import (
    create_metadata,
    create_result_dir,
)
from ska_sdp_wflow_pointing_offset.pointing_pipeline import (
    DATA_COMMENT,
    InitPipeline,
)
from ska_sdp_wflow_pointing_offset.utils import (
    construct_ms_file_name,
    convert_offsets_to_degrees_for_kafka,
    convert_to_structured,
)

LOG = logging.getLogger("ska-sdp-pointing-offset")

WATCHER_TIMEOUT = int(os.getenv("WATCHER_TIMEOUT", "60"))
POINTING_METADATA_FILE = "ska-data-product.yaml"


# pylint: disable-next=too-many-instance-attributes
class SDPPipeline(InitPipeline):
    """
    Run the pointing offset calibration pipeline standalone.

    Exception raised if (in addition to base class):
    - --eb_id is empty
    - --results_dir is emtpy
    - SDP_PB_ID environment variable is not set

    This class implements the interaction with the Configuration Database
    as well as a few other methods like generating and updating the
    metadata file.
    """

    def __init__(self, cli_args):
        super().__init__(cli_args)

        self.num_scans = int(cli_args["--num_scans"])
        LOG.info("Expecting %s number of scans", self.num_scans)

        self.eb_id = cli_args.get("--eb_id")
        if not self.eb_id:
            raise ValueError(
                "Provide an execution block id for processing in SDP"
            )
        self.pb_id = os.getenv("SDP_PB_ID")
        if not self.pb_id:
            raise ValueError(
                "Processing block ID is not set in "
                "SDP_PB_ID environment variable"
            )

        self.config = Config()

        self.scan_ids = None
        self.last_index = None
        self._receptors = None
        self._results_dir = None
        self._msdir = None
        # Kafka server information, set to default None
        self._data_model = None
        self._kafka_server = None
        self._kafka_topic = None

    def close(self):
        """Reset variables at the end of each run."""
        self.scan_ids = None
        self._results_dir = None

    def set_scan_ids_and_last_index(self):
        """Update scan_ids and last_index."""
        if self.last_index is None:
            self.last_index = self.find_last_scan_index()

        scan_ids, last_index = self.get_pointing_scan_ids(self.last_index)
        self.scan_ids = scan_ids
        self.last_index = last_index

    def _get_vis_receive_sink_path(self):
        """Get the sink path of the vis_receive flow"""
        pointing_offest_key = entity.flow.Flow.Key(
            pb_id=self.pb_id,
            kind="data-product",
            name="pointing-offset",
        )
        for txn in self.config.txn():
            if pointing_flow := txn.flow.get(pointing_offest_key):
                LOG.info("Found pointing-offset flow: %s", pointing_flow)
                vis_receive_key = pointing_flow.sources[0].uri
                if vis_receive_flow := txn.flow.get(vis_receive_key):
                    vis_receive_sink_path = str(
                        pointing_flow.sink.data_dir.pvc_mount_path
                        / vis_receive_flow.sink.data_dir.pvc_subpath
                    )
                    LOG.info(
                        "Found vis-receive DataProduct in "
                        "Configuration Database: %s",
                        vis_receive_sink_path,
                    )
                return vis_receive_sink_path
        return None

    @property
    def msdir(self):
        """The path that measurement sets are stored.
        This is not only an attribute, but also gets
        the sink path and store it in self._msdir.
        """
        if self._msdir is not None:
            return self._msdir

        vis_receive_sink_path = self._get_vis_receive_sink_path()
        if vis_receive_sink_path is None and self._ms_dir_base is None:
            raise ValueError(
                "Directory containing measurement sets is required!"
            )

        self._ms_dir_base = vis_receive_sink_path
        self._msdir = self._ms_dir_base
        return self._msdir

    @property
    def ms_files(self):
        """Collect measurement file paths."""
        if not self._ms_files:
            self._ms_files = sorted(
                construct_ms_file_name(self.msdir, self.scan_ids)
            )
        # pylint: disable=duplicate-code
        if not self._ms_files:
            raise FileNotFoundError(
                f"Given directory ({self.msdir}) doesn't contain "
                "the right (or any) MeasurementSets"
            )

        return self._ms_files

    def _get_pointing_offset_sink_path(self):
        """Get the sink path of the pointing offset flow"""
        pointing_offset_key = entity.flow.Flow.Key(
            pb_id=self.pb_id, kind="data-product", name="pointing-offset"
        )
        for txn in self.config.txn():
            pointing_offset_flow = txn.flow.get(pointing_offset_key)
        if pointing_offset_flow is not None:
            pointing_offset_sink_path = (
                pointing_offset_flow.sink.data_dir.path.as_posix()
            )
            LOG.info(
                "Found pointing-offset DataProduct in "
                "Configuration Database: %s",
                pointing_offset_sink_path,
            )
            return pointing_offset_sink_path
        return None

    @property
    def results_dir(self):
        """Set results_dir based on scan_ids."""
        if self._results_dir is None:
            pointing_offset_sink_path = self._get_pointing_offset_sink_path()

            if pointing_offset_sink_path is not None:
                self._results_dir_base = pointing_offset_sink_path
            elif self._results_dir_base is None:
                raise FileNotFoundError(
                    "Need to set the directory for results via "
                    "--results_dir"
                )
            if self.scan_ids is None:
                self.set_scan_ids_and_last_index()

            results_dir = create_result_dir(
                self._results_dir_base, self.scan_ids
            )
            self._results_dir = results_dir

        return self._results_dir

    @property
    def common_prefix(self):
        """Set the common_prefix for saving data."""
        if self._common_prefix is None:
            self._common_prefix = self.results_dir + "/"
        return self._common_prefix

    @property
    def receptors(self):
        """Return receptors assigned to the execution block, if any."""
        if self._receptors is None:
            for txn in self.config.txn():
                execution_block = txn.execution_block.get(self.eb_id)
                resources = execution_block.resources
                if resources:
                    self._receptors = resources.get("receptors")
                else:
                    self._receptors = None

        return self._receptors

    def update_processing_block_state(self, scan_list):
        """
        Update the parameters in the processing block for the scans
        that have been successfully processed.

        :param scan_list: List of most recent processed scans.
                        Each item being a dictionary that contains
                        {"scan_id": id, "status": "success" or "fail",
                        "error": str(error_message)}
        """
        for txn in self.config.txn():
            state = txn.processing_block.state(self.pb_id).get()
            try:
                processed = state["processed"]
                LOG.debug("Current list of processed scans: %s", processed)
                state["processed"] = processed + scan_list
            except KeyError:
                state["processed"] = scan_list

            txn.processing_block.state(self.pb_id).update(state)

    def update_data_flow_state(self, error_data=None, kind="data-queue"):
        """
        Update data flow state (include both DataProduct and DataQueue)
        based on processing status.

        In the case of success, only update the status
        in case processing error occurs, add the error to the log

        :param error_data: Input exception data
        :param kind: Kind of data flow ("data-queue" or "data-product")
        """
        if kind not in ("data-queue", "data-product"):
            LOG.error("Flow kind not supported. Nothing gets updated.")
            return

        for txn in self.config.txn():
            key = entity.flow.Flow.Key(
                pb_id=self.pb_id,
                kind=kind,
                name="pointing-offset",
            )
            state = txn.flow.state(key).get()

            to_create = False
            if not state:
                state = {}
                to_create = True

            if error_data:
                error_log = {
                    "type": "error",
                    "message": error_data["error"],
                    "time": str(datetime.now()),
                    "traceback": error_data["traceback"],
                }
                state["status"] = "FAULT"

                if "internal" in state.keys():
                    state["internal"].append(error_data["scan_ids"])
                else:
                    state["internal"] = [error_data["scan_ids"]]

                if "log" in state.keys():
                    state["log"].append(error_log)
                else:
                    state["log"] = [error_log]

            else:
                LOG.info("No error data, data flow generated successfully.")
                state["status"] = "COMPLETE"
                state["log"] = []  # Clear error logs

            LOG.debug("Updated data flow state after this batch is %s", state)

            if to_create:
                LOG.info("Creating data flow state.")
                txn.flow.state(key).create(state)
            else:
                txn.flow.state(key).update(state)

    def find_last_scan_index(self):
        """
        Find index of last processed Scan ID
        in case the pod is interrupted.
        Note it does not return the Scan ID number,
        but the index of it in the list of scans.
        If scan list is empty, return -1.
        """
        for txn in self.config.txn():
            eb_state = txn.execution_block.state(self.eb_id).get()
            scans = eb_state.get("scans")
            pb_state = txn.processing_block.state(self.pb_id).get()
            try:
                processed_scans = pb_state["processed"]
                if processed_scans:
                    last_id = processed_scans[-1]["scan_ids"][-1]
                    LOG.debug(
                        "Read from processing block state entry. "
                        "Last processed Scan ID is %s",
                        last_id,
                    )
                    try:
                        # Note: this only returns the first index
                        # if occurred more than once
                        scan_ids = [scan["scan_id"] for scan in scans]
                        last_index = scan_ids.index(last_id)
                    except ValueError:
                        LOG.error("Processed scan not in list of scans.")
                        last_index = -1

                else:
                    # if processed list is empty, start from initial state.
                    LOG.debug("Processed list empty.")
                    last_index = -1

            except KeyError:
                # When processed information is not available
                LOG.info("No information from the processing block state.")
                last_index = -1

        LOG.info(
            "Index of the last processed Scan ID from initial state: %s",
            last_index,
        )
        return last_index

    # pylint: disable-next=inconsistent-return-statements
    def wait_for_pointing_scan_type(self):
        """
        Wait for current scan type to be a string starting with 'pointing'
        """
        for watcher in self.config.watcher(timeout=WATCHER_TIMEOUT):
            for txn in watcher.txn():
                eb = txn.execution_block.state(self.eb_id).get()
                scan_type = eb.get("scan_type")
            if scan_type is not None and scan_type.startswith("pointing"):
                return True

    def get_pointing_scan_ids(self, last_index=-1):
        """
        Obtain all pointing scan IDs to process. It only returns scans of
        type starting with 'pointing' and status = FINISHED. Function
        only returns when the expected number of scans have been finished.

        Note: if a pointing-type scan is interrupted by another pointing-type
        scan, the first will not be processed. Only the expected number of
        consecutive scans of the same pointing type are processed. These
        can only be interrupted by non-pointing type scans, or scans that
        are not "FINISHED".

        :param last_index: Index of last processed scan ID (int)
                        If -1, means no scans have been processed yet

        :returns:
            1. list of scan IDs with the same scan_type to process in
            the order in which they were executed
            (note: this assumes that's the order in
            which they are saved in the eb)
            2. Updated last_index in scan list
        """
        pointing_scans = []

        is_break = False

        for watcher in self.config.watcher(timeout=WATCHER_TIMEOUT):
            for txn in watcher.txn():
                eb = txn.execution_block.state(self.eb_id).get()
                scans = eb.get("scans")
                scans_to_search = scans[last_index + 1 :]
                pointing_scans = [
                    scan
                    for scan in scans_to_search
                    if scan["scan_type"].startswith("pointing")
                    and scan["status"] == "FINISHED"
                ]

            groups = [
                (scan_group[0], list(scan_group[1]))
                for scan_group in groupby(
                    pointing_scans, key=itemgetter("scan_type")
                )
            ]

            for i, (stype, scans_list) in enumerate(groups):
                if len(scans_list) < self.num_scans and i + 1 < len(groups):
                    LOG.warning(
                        "scans with type %s for ids %s are being skipped "
                        "because of insufficient number of scans"
                        " in the group",
                        stype,
                        [sc["scan_id"] for sc in scans_list],
                    )
                elif len(scans_list) >= self.num_scans:
                    scan_ids = [sc["scan_id"] for sc in scans_list][
                        : self.num_scans
                    ]
                    last_index = (
                        last_index
                        + 1
                        + scans_to_search.index(scans_list[self.num_scans - 1])
                    )
                    is_break = True
                    break

            if is_break:
                break

        return scan_ids, last_index

    def create_metadata_file(self):
        """Create initial metadata file."""
        create_metadata(f"{self.results_dir}/{POINTING_METADATA_FILE}")

    def _update_metadata_file(
        self, results_file_hdf, visibility, pointing_table
    ):
        """
        Update metadata file with obscore data and
        dataproduct file information.

        :param results_file_hdf: full path to the HDF5 dataproduct file
        :param visibility: Visibility object
        :param pointing_table: The PointingTable hosting the pointing
            offsets and other fitted parameters and their uncertainties.
        """
        metadata_path = f"{self.results_dir}/{POINTING_METADATA_FILE}"
        metadata = create_metadata(metadata_path)
        metadata.output_path = metadata_path

        # Update the obscore metadata
        metadata = _update_obscore_metadata(
            metadata, results_file_hdf, visibility, pointing_table
        )

        # need to save the file path without the mount directory
        # it needs to start with /product
        file_path_wo_mount = f"/product{results_file_hdf.split('product')[1]}"
        metadata_file = metadata.new_file(
            dp_path=file_path_wo_mount,
            description=f"Pointing offsets for scans: {self.scan_ids}",
        )

        return metadata_file

    def configure_kafka_from_dataqueue(self):
        """
        Use a DataQueue flow type (as part of data flow)
        To get Kafka server information and data format,
        and update the corresponding attributes

        """
        if self._kafka_server is not None:
            LOG.debug(
                "Skipping searching data flow,"
                "Current Kafka topic and host are %s, %s",
                self._kafka_topic,
                self._kafka_server,
            )
            return
        # Else move forward
        key = entity.flow.Flow.Key(
            pb_id=self.pb_id,
            kind="data-queue",
            name="pointing-offset",
        )
        for txn in self.config.txn():
            flow = txn.flow.get(key)
            if flow:
                LOG.info("Found DataQueue in Configuration Database.")
                self._data_model, self._kafka_server, self._kafka_topic = (
                    flow.data_model,
                    flow.sink.host.bootstrap_address,
                    flow.sink.topics,
                )
            else:
                # Set default values
                LOG.info("DataQueue not found, use default values instead.")
                self._data_model = "PointingNumpyArray"
                self._kafka_topic = "pointing_offset"
                self._kafka_server = "localhost:9092"

    async def send_data_to_kafka(self, data):
        """
        Send data to kafka.

        :param data: structured numpy array containing pointing data
        """

        # convert to structured numpy array before sending
        # In case of other data models, the error will be raised
        # In the next step
        if self._data_model == "PointingNumpyArray":
            data = convert_to_structured(data, self.scan_ids[-1])
            LOG.debug("Data as sent to Kafka topic: %s", data)

        # Send structured numpy array to Kafka
        # The only current allowed schema is numpy array
        # Others won't be supported yet
        allowed_schemas = {
            "PointingNumpyArray": ["msgpack_numpy", PointingNumpyArray]
        }
        schema = allowed_schemas.get(self._data_model)
        if schema:
            LOG.info(
                "Data format is %s, and the schema to validate is %s",
                schema[0],
                type(schema[1]).__name__,
            )
        else:
            LOG.error("Data model not supported.")

        LOG.info("Sending output offsets data to Kafka queue.")

        async with DataQueueProducer(
            self._kafka_server, topic=self._kafka_topic
        ) as kafka_producer:
            await kafka_producer.send(
                data,
                schema[0],
                schema=schema[1],
                validate=True,
            )

        LOG.info("Data is sent successfully.")

    def export_data(self, output_offsets, pointing_table, visibility):
        """
        Perform a set of data export operations:
            1. write data into .txt file
            2. write data into .hdf5 file
            3. update the metadata file with additional obscore data
            4. Configure data flow for Kafka information
            5. send data to Kafka queue
        """
        # pylint: disable=duplicate-code
        LOG.info("Writing fitted parameters and computed offsets to file...")
        results_file = self.common_prefix + "pointing_offsets.txt"
        export_pointing_offset_data(results_file, output_offsets, visibility)
        LOG.info(
            "Fitted parameters and computed offsets written to %s",
            results_file,
        )

        results_file_hdf = results_file.replace(
            ".txt", f"_scans_{self.scan_ids[0]}-{self.scan_ids[-1]}.hdf5"
        )
        export_pointingtable_to_hdf5(
            pointing_table, results_file_hdf, **DATA_COMMENT
        )
        LOG.info("Written PointingTable into HDF5 file %s", results_file_hdf)

        metadata_file = self._update_metadata_file(
            results_file_hdf, visibility, pointing_table
        )

        self.configure_kafka_from_dataqueue()
        LOG.info("Sending to Kafka host %s", self._kafka_server)

        if self._data_model == "PointingNumpyArray":
            data = convert_offsets_to_degrees_for_kafka(output_offsets)
        elif self._data_model == "PointingTable":
            data = pointing_table
        else:
            # This will raise an error when sending
            data = output_offsets

        asyncio.run(self.send_data_to_kafka(data))

        try:
            metadata_file.update_status("done")
        except MetaData.ValidationError as err:
            LOG.error("Validation failed with error(s): %s", err.errors)


def _update_obscore_metadata(
    metadata, results_file_hdf, visibility, pointing_table
):
    """
    Updates the metadata object with obscore values.

    :param metadata: metadata to update
    :param results_file_hdf: path and filename to HDF file
    :param visibility: Visibility object
    :param pointing_table: The PointingTable hosting the
        pointing offsets and other fitted parameters and their
        uncertainties.
    """
    if os.path.isfile(results_file_hdf):
        hdf_filesize_kb = round(os.path.getsize(results_file_hdf) / 1024.0)
    else:
        LOG.warning(
            "Pointing output HDF file, %s, does not exist. Setting the "
            "file size to zero in the metadata yaml file.",
            results_file_hdf,
        )
        hdf_filesize_kb = 0

    obscore_to_fill = {
        "obs_id": "",
        "access_estsize": hdf_filesize_kb,
        "target_name": visibility.source,
        "s_ra": visibility.phasecentre.ra.deg,
        "s_dec": visibility.phasecentre.dec.deg,
        "t_min": visibility.time.data[0],
        "t_max": visibility.time.data[-1],
        "em_min": lightspeed / (visibility.frequency.data[0]),
        "em_max": lightspeed / (visibility.frequency.data[-1]),
        "pol_states": pointing_table.receptor_frame.type,
        "pol_xel": pointing_table.receptor_frame.nrec,
    }

    data = metadata.get_data()
    for obscore_item in obscore_to_fill.items():
        setattr(data.obscore, obscore_item[0], obscore_item[1])

    return metadata
