.. _stokesI_fitting:

Fitting the Beams to the Stokes I Amplitudes
============================================

The pipeline supports fitting the primary beams to the
time-frequency-polarisation-averaged visibility or gain amplitudes (total
intensity amplitudes). The user has the option to fit the primary beams to the
these amplitudes across the entire frequency band (not recommended for
wide-bands) or in a number of frequency chunks. In the latter case, the first
and last chunks are discarded (so using a frequency chunk of two is not
permissible) and the beams are then fitted to these amplitudes in each chunk.

Fitting the Beams to the Stokes I Visibility Amplitudes
-------------------------------------------------------

The user has the option to fit the primary beams to the cross-correlation
visibility amplitudes across the entire frequency band or in a number of
frequency chunks. The primary beams are fitted to the cross-correlation
visibility amplitudes of the moving dish (see :ref:`single_baseline`).

Fitting the Beams to the Stokes I Gain Amplitudes
-------------------------------------------------

The user has the option to solve for the un-normalised gain amplitudes (G-terms)
across the entire frequency band or in a number of frequency chunks. The
un-normalised G-terms for each scan, over number of dishes with one curve per
frequency chunk in the 950 - 1350 MHz band with RFI mask applied for the sample
9-point scan with the MeerKAT array are shown below.

.. image:: ../images/1672727931_sdp_l0_scan_gain_amplitude.png
   :alt: Un-normalised gain amplitudes (G terms)
   :width: 10000%

The Gaussian fits in cross-elevation and elevation in each frequency chunk is
shown below for dish m000 from the sample 9-point scan with the MeerKAT array.

.. image:: ../images/1672727931_sdp_l0_scan_fitting_results_m000.png
   :alt: Gaussian fits for dish m000
   :width: 10000%

The weighted-average of the fitted parameters for all dishes from the sample
nine-point scan with the MeerKAT array

.. image:: ../images/1672727931_sdp_l0_scan_offsets.png
   :alt: Fitted pointing offsets and their uncertainties
   :width: 10000%
