.. _sep_pol_fitting:

Fitting the Beams to the Parallel-hands Gain Amplitudes
=======================================================

The user has the option to solve for the un-normalised gain amplitudes (G-terms)
across the entire frequency band (not recommended for wide-bands) or in a number
of frequency chunks for the parallel-hands polarisations. In the latter case,
the top and bottom part of the chunks are discarded (so using a frequency chunk
of two is not permissible) and the beams are then fitted to the time-frequency
averaged G-terms in each chunk for each parallel-hand polarisation. These fitted
offsets for the parallel-hands polarisations are then averaged. This option can
be requested with the ``--fit_to_sep_pol`` command line argument. A concern
associated with this approach is the possibility of low SNR compared to fitting
to the Stokes I amplitude. It is therefore possible that some dishes could have
invalid fits in one or both parallel-hand polarisations because of low SNR. This
option should therefore only be used if the telescope is well understood,
possibly via repeated pointing calibrations.

The un-normalised G-terms for each scan for each parallel-hand polarisation,
over number of dishes with one curve per frequency chunk in the 950 - 1350 MHz
band with RFI mask applied for the sample 9-point scan with the MeerKAT array is
shown below.

.. image:: ../images/1672727931_sdp_l0_scan_sep_pol_gain_amplitude.png
   :alt: Un-normalised gain amplitudes (G terms) for the parallel-hands polarisations
   :width: 10000%

The Gaussian fits in cross-elevation and elevation in each frequency chunk for the parallel-hands are shown below
for dish m000 from the sample 9-point scan with the MeerKAT array.

.. image:: ../images/1672727931_sdp_l0_scan_fitting_results_stokesXX_m000.png
   :alt: Gaussian fits for Stokes XX for dish m000
   :width: 10000%

.. image:: ../images/1672727931_sdp_l0_scan_fitting_results_stokesYY_m000.png
   :alt: Gaussian fits for Stokes YY for dish m000
   :width: 10000%


The weighted-average of the fitted parameters for all dishes from fitting to the
parallel-hands polarisations gain amplitudes from the sample 9-point scan with
the MeerKAT array

.. image:: ../images/1672727931_sdp_l0_scan_offsets_fit_to_sep_pol.png
   :alt: Fitted pointing offsets and their uncertainties from fitting to the parallel-hands gain amplitudes
   :width: 10000%

