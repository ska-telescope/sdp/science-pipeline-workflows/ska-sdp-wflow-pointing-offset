.. _fitting_qa:

Validity of the Gaussian Fits
=============================

For the Gaussian fit to be declared valid so the fitted parameters can be
accepted for use, the pipeline ensures that all the following conditions are
met:

- The fitted Gaussian centre (a direct estimate of the cross-elevation and
  elevation offsets of interest) is physical, i.e not NaNs.
- The fitted Gaussian height is greater than zero. The height is in arbitrary
  units and this condition is currently not configurable from the command line.
- The ratio of fitted width to expected width must be between 0.9 and a
  user-specified upper limit. The default upper limit is 1.15.
- The SNR of the fitted width is greater than zero (where the SNR is defined as
  width divided by the standard error on width). This condition is currently not
  configurable from the command line.

If one or more of these criteria are not met, the pipeline reports a warning
indicating a valid fit was not obtained for that particular dish, and NaNs are
stored for all the fitted parameters including the pointing uncertainties.

It is worth noting that while the pipeline extracts the standard errors on the
fitted Gaussian parameters, these are not used internally by the pipeline to
determine whether or not a fit is valid (this may be possible to add in future,
if required). However, we output the standard error as described in the
:ref:`data_products`. This way, the user can investigate these standard errors
and choose to accept or reject the fitted pointing offsets of interest derived
by the pipeline.

The uncertainties on each fitted parameter are calculated as the "standard
error" on that parameter (see :ref:`averaging`). They are written into the
output file, and so are available for the user to assess the fit outside of the
pipeline. Note that the uncertainty on the pointing offsets themselves is
written out as a "weight", equal to the inverse square of the standard error.

Understanding Beam Squint
-------------------------

Polarisation-dependent pointing errors can impact the accuracy of pointing
calibration achieved. `de Villiers et. al (2022)
<https://iopscience.iop.org/article/10.3847/1538-3881/ac460a/pdf>`_ [#f1]_ and
`de Villiers (2023)
<https://iopscience.iop.org/article/10.3847/1538-3881/ac460a/pdf>`_ [#f2]_
carried out detailed beam measurements using holography for the MeerKAT array at
UHF, L, and S bands.

Since the SKA will also incorporate the MeerKAT dishes, it is important users study these papers and any that may be
published for the SKA dishes to identify the range of frequencies to use for the pointing calibration to minimise the impact
of beam squint whether fitting to the Stokes I or the parallel-hands amplitudes.


.. rubric:: References
.. [#f1] de Villiers, M. S., & Cotton, W. D., 2022, AJ, 163, 135.
.. [#f2] de Villiers, M. S., 2023, AJ, 165, 78.
