.. _static_rfi_mask:

Applying static RFI mask
========================

The pipeline pulls the channel-dependent RFI mask from the `Telescope Model Data
Repository`_ and applies it to the visibilities. Therefore, the mask should be
uploaded to the repository by the user of this pipeline. Instructions on how to
do this are given below.

To apply the mask, the repository hosting the mask should first be set as an
environment variable as in the example below::

    export SKA_TELMODEL_SOURCES=gitlab://gitlab.com/ska-telescope/ska-telmodel-data?yan-1618-add-rfi-model#tmdata

The ``--apply_mask`` command line argument should be given, and the full path to
the RFI file supplied via the ``--rfi_file`` command line argument . For example::

    --rfi_file instrument/ska1_mid/static-rfi/rfi_mask.h5

The pipeline pulls the mask from the specified location in real time and applies
it to the visibilities. The pipeline is able to extract a smaller mask from the
existing larger mask and apply that to the visibilities if the frequency
channels of the observation are fewer than the RFI mask in the `Telescope Model
Data Repository`_. In such a case, the observing start and end frequencies,
however few, should match those in the mask exactly.  If this is not the case,
an error (``"Start and end frequencies of observation do not fall exactly within
those of the static RFI mask!"``) will be raised.

The pipeline cannot apply a mask with less frequency coverage than the
observing frequencies. In such a case, the pipeline will raise an
error (``"Observation has more frequency channels than static RFI mask!"``)

Generating static RFI mask in HDF5 format
-----------------------------------------

The pointing pipeline supports the RFI mask in HDF5 format only. To generate the
mask for your observation, use the
:external+ska-sdp-datamodels:func:`~ska_sdp_datamodels.visibility.export_staticmasktable_to_hdf5`
as in the example below after installing `ska-sdp-datamodels`_. ::

    from ska_sdp_datamodels.visibility.vis_io_and_convert import StaticMaskTable
    from ska_sdp_datamodels.visibility import export_staticmasktable_to_hdf5

    # First create the mask table (xarray object) for the mask
    mask_table = StaticMaskTable.constructor(frequency, channel_bandwidth, mask)

    # Write the mask table to HDF5 format and save to disc
    export_staticmasktable_to_hdf5(mask_table, filename)

The arguments are defined as follows:

- ``frequency``: Array of the frequencies of your observation in Hz.
- ``channel_bandwidth``: Array of the bandwidth of your observation in Hz. Has
  the same length as ``frequency``.
- ``mask``: 1-D array of ones and zeros you want to apply. 1 is a flagged
  channel and 0 is an accepted channel. The ``mask`` has the same length as
  ``frequency``.
- ``filename``: The filename of the output HDF5 file to be uploaded to the
  `Telescope Model Data Repository`_.

..
    Ideally, we should cross link to the API docs of the StaticMaskTable class, 
    which will cover the parameters above, but this class is not in the docs of
    ska-sdp-datamodels (yet?)

Uploading the static RFI mask to Telescope Model Data Repository
----------------------------------------------------------------
To upload the mask you have generated with the lines of code above to the
`Telescope Model Data Repository`_, follow the steps below:

- Create a branch in the repository. Set the ``SKA_TELMODEL_SOURCES``
  environment variable to point to this branch (in the example above
  ``yan-1618-add-rfi-model`` is the branch name, which you need to replace with
  yours).
- Upload the generated mask (in HDF5 format) to
  ``tmdata/instrument/<telescope_name>/static-rfi/rfi_mask.h5``. The filename
  used here should be referenced as the expected file provided to the pipeline
  with the ``--rfi_file`` command line argument.


.. _Telescope Model Data Repository: https://gitlab.com/ska-telescope/ska-telmodel-data
.. _ska-sdp-datamodels: https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels