.. _run_in_standalone:

Running the pipeline as standalone
==================================

The pointing offset calibration pipeline can be run as a stand-alone pipeline
(see :mod:`pointing_offset_cli`). To do this, below are steps to follow to
prepare your Measurement Sets:

- Generate one Measurement Set for each scan. For example, if your observation
  is a 9-point scan, the pipeline expects to process 9 Measurement Sets. If all
  9 scans are in a single Measurement Set, `CASA`_ task ``split`` can be used to
  split them into separate Measurement Sets.
- The dish pointings should be written into the ``POINTING`` sub-table following
  the `MSv2`_ format. If the user prefers to use the commanded pointings, they
  should be written into the ``TARGET`` column of the ``POINTING`` sub-table of
  each Measurement Set. Alternatively, the commanded pointings relative to the
  pointing calibrator should be written into the ``SOURCE_OFFSET`` column and
  used with the ``--use_source_offset_column`` command line argument. Only one
  dish pointing option can be requested at a time to be used in the calibration.

To apply a static RFI mask, refer to the :ref:`static_rfi_mask` section and the
output data products are described in the :ref:`data_products` section.


.. _CASA: https://casa.nrao.edu
.. _MSv2: https://casacore.github.io/casacore-notes/264.html#x1-3500016
