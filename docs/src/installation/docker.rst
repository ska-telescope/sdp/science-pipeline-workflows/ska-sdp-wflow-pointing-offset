Docker image
============

A Docker image which has all the requirements already installed is also available.
The image is based on `artefact.skao.int/ska-python` and its entrypoint is ["pointing-offset"].
Running the Docker image will be just like running a command line application.

In order to run the pipeline using the compute option, execute the following:

.. parsed-literal::

    docker run -it -v ${PWD}:/data artefact.skao.int/ska-sdp-wflow-pointing-offset:|version| compute --msdir=/data/

Make sure you run this from the directory where the Measurement Sets used for calibration are,
and use the correct version tag. The latest version is |version|.
The other command line arguments follows as in the example above.