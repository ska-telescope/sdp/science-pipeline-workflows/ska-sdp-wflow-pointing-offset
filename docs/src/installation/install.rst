Installation Instructions
=========================

The package is installable via pip.

If you would like to view the source code or install from git, use::

    git clone --recurse-submodules https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset.git

Please ensure you have all the dependency packages installed. The installation is managed through `poetry`_.
Refer to their page for instructions.

A docker image is also available. Refer to :doc:`docker<docker>` page for details.

.. _poetry: https://python-poetry.org/docs
