.. _averaging:

Averaging and Propagation of Uncertainties
==========================================
The fitting routines used in the pipeline outputs the centre, width, and height
of the fitted Gaussian, as well as the standard errors of these parameters. If a
user sets ``num_chunks`` to a value greater than 2, the visibility or gain
amplitudes are generated for the specified number of frequency chunks, and must
be combined to produce the final result.

The pipeline computes the weighted-average of the visibility or gain amplitudes
for each frequency chunk within each scan of the pointing observation as the
input to the fitter. This averaging is either carried out in time only, or in
time and polarisation. Averaging only in time allows :ref:`sep_pol_fitting`.
Averaging over both time and polarisation allows  :ref:`stokesI_fitting`. It is
worth noting that the pipeline currently does not support fitting the primary
beams on the parallel-hands polarisation visibility amplitudes.

Averaging the Visibility and Gain Amplitudes
--------------------------------------------

When computing the weighted-average of the gain amplitudes, the weights on the
gains are extracted from the gain solver used by the pipeline. For the
weighted-average of the visibility amplitudes, the weights are extracted from
the ``WEIGHT`` column of the ``MAIN`` table of the Measurement Sets. The weights
are used as a measure of the reliability of each sample.

The 2D Gaussian fitter also uses the standard deviation for each averaged
visibility or gain amplitude (see `scikits.fitting
<https://github.com/ska-sa/scikits.fitting/blob/master/scikits/fitting/gaussian.py>`_).
The pipeline calculates the standard deviation for each point from the unbiased
weighted-variance of the visibility or gain amplitudes. The fitter then
determines the best fit parameters of interest (i.e the fitted Gaussian centre,
width, and height) for each frequency chunk, along with the standard error on
each fitted parameter.

Averaging the Fitted Parameters
-------------------------------

The pipeline data product contains a single set of fitted parameters for each
dish, and so if more than one frequency chunk are used, the results must be
combined. This is carried out using a weighted-average for each fitted parameter
(ignoring invalid fits - see :ref:`fitting_qa` for more information on how the
fits are declared valid). The weights used in the average for each fit parameter
are calculated from the inverse square of their standard errors. The final
weighted-standard error is calculated from the square root of the unbiased
weighted variance adjusted by the effective sample size.
