.. _observing_modes:

Observing Modes
===============

The pointing offset calibration pipeline is primarily designed to estimate the pointing offsets (in cross-elevation and elevation)
of SKA dishes in two ways:

- By fitting the power beams to the cross-correlation visibility amplitudes (in the pipeline, this is done via setting
  ``--fit_to_vis`` to True). This functionality requires two dishes in two sets of observations where one dish tracks
  the pointing calibrator and the other performs a five-point or similar scan pattern. The dish roles are then swapped.
  The beams are then fitted to the visibility amplitudes of the moving dish in each set of observation and the pointing
  offsets derived. We simply refer to this scenario as ``"the two-dish mode scenario"``.
- By fitting the voltage beams to the gain amplitudes of the dishes (setting ``--fit_to_vis`` to False). This functionality
  requires at least three dishes all moving simultaneously in a five-point or similar scan pattern. We simply refer to this
  scenario as the ``"three-plus-dish mode scenario"``.

The pipeline provides two options to extract the on-sky offsets from the Measurement Sets of each scan:

- The commanded pointings in AzEl coordinates are extracted from the ``TARGET`` column of the ``POINTING`` sub-table.
- The commanded on-sky offsets in cross-elevation and elevation are extracted from the ``SOURCE_OFFSET`` column of the
  ``POINTING`` sub-table.

When the commanded pointings in AzEl coordinates are used, katpoint is first used to convert the target position from radec
to AzEl at all timestamps of each scan. The dish pointings relative to the target in cross-elevation is calculated as the
product of the difference in commanded and target Az, and the cosine of the target elevation. The dish pointings relative
to the target in elevation is the difference in commanded and target El.

For a detailed description of the pipeline operation for each observing scenario, refer to the following pages:

.. toctree::
   :maxdepth: 3

   single_baseline
   standard_interferometry