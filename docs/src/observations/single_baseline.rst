.. _single_baseline:

Single-baseline Interferometry
==============================

This functionality requires two dishes, with two sets of observations, where one
dish tracks the pointing calibrator and the other performs a five-point or
similar scan pattern. The dish roles are then swapped. The power beams are then
fit to the cross-correlation visibility amplitudes of the moving dish in each
set of observations and the pointing offsets derived.

A sample simulated five-point observation with two SKA dishes showing the on-sky
offsets in cross-elevation and elevation is shown below.

.. image:: ../images/on_sky_offsets1.png
   :alt: On-sky Offsets for first set of observation

.. image:: ../images/on_sky_offsets2.png
   :alt: On-sky Offsets for second set of observation

The pointing offsets in cross-elevation and elevation for each dish in the
interferometric array are calculated following the functionality diagram below

.. image:: ../images/single-baseline-func-diagram.png
   :alt: Pointing Offset Functionality Diagram
   :width: 1000%