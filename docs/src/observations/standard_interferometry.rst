Standard Interferometry
=======================

To fit the voltage beams to the gain amplitudes, at least three dishes are
required for the gain calibration. Also, the pipeline requires all dishes to be
moved simultaneously during a pointing offset calibration scan. The primary beam
modelled by a 2D Gaussian is fit to the un-normalised gain (G terms) amplitudes
from all the scans.

A sample nine-point scan with the MeerKAT array showing the on-sky offsets in
cross-elevation and elevation is shown below

.. image:: ../images/1672727931_sdp_l0_on_sky_offsets.png
   :alt: On-sky Offsets


The pointing offsets in cross-elevation and elevation for each dish in the
interferometric array are calculated following the functionality diagram below

.. image:: ../images/standard-interferometry-func-diagram.png
   :alt: Pointing Offset Functionality Diagram
   :width: 1000%
