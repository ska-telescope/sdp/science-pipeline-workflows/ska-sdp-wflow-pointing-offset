# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Pointing Offset Calibration Pipeline"
copyright = "2025 SKA Observatory"
author = "See CONTRIBUTORS"
version = "1.0.0"
release = "1.0.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]

exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"


# -- Extension configuration -------------------------------------------------

# Intersphinx configuration
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "ska-sdp-datamodels": (
        "https://developer.skao.int/projects/ska-sdp-datamodels/en/latest",
        None,
    ),
    "ska-sdp-script": (
        "https://developer.skatelescope.org/projects/ska-sdp-script/en/latest/",
        None,
    ),
    "ska-sdp-config": (
        "https://developer.skatelescope.org/projects/ska-sdp-config/en/latest/",
        None,
    ),
}
