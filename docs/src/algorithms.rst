Algorithms and Tools
====================

A list of the algorithms and tools used in the pipeline are:

- `ska-sdp-datamodels`_: Provides function for reading measurement sets and converting the visibilities and all relevant tables to `xarray`_ object.
- `ska-sdp-func-python`_: Provides solver for calibrating for the complex gains of an antenna and function for converting antenna positions in Earth-Centred, Earth-Fixed to longitude, latitude and altitude coordinate system.
- `SciPy`_: Provides interpolation routine for aligning the actual pointings relative to the pointing calibrator and the visibility timestamps.
- `scikits.fitting`_: Provides routines for modelling the primary beam with a 2D Gaussian and fitting them to the gain amplitudes or visibilities.
- `katpoint`_: Used for constructing list of antenna information (location, diameter, etc) for easy use by the fitting routine.
- `NumPy`_: Used for manipulating arrays e.g, applying RFI masks.


.. _ska-sdp-datamodels: https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels
.. _xarray: https://github.com/pydata/xarray
.. _ska-sdp-func-python: https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python
.. _SciPy: https://scipy.org
.. _scikits.fitting: https://github.com/ska-sa/scikits.fitting
.. _katpoint: https://gitlab.com/ska-telescope/katpoint
.. _NumPy: https://github.com/numpy/numpy
