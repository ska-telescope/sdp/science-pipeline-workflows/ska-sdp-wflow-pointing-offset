ska\_sdp\_wflow\_pointing\_offset package
=========================================

.. toctree::
   :maxdepth: 2

   Pointing Offset CLI<pointing_offset_cli>
   Pointing Pipeline Init Class<pointing_pipeline>
   Pointing Pipeline SDP Class<pointing_pipeline_sdp>
   Pointing Pipeline Stand-alone Class<pointing_pipeline_standalone>
   Reading Data<read_data>
   Applying RFI mask, Interpolation, PointingTable<array_data_func>
   Gain calibration<calibration/gt_solve>
   Primary Beam Fitting<beam_fitting>
   Exporting Computed Offsets<export_data>
   plotting/plotting_func
   Utilities<utils>

