Plotting Calibration Outputs
============================

.. toctree::
  :maxdepth: 3


.. autofunction:: ska_sdp_wflow_pointing_offset.plotting.on_sky_offsets_plot.plot_on_sky_offsets(on_sky_offsets, ants, prefix="test")

   Plot the on-sky offsets which are the position coordinates for the beam
   fitting routine

   :param on_sky_offsets: The commanded pointings relative to the target
        in degrees in the reference pointing observation in xEl-El
        coordinates with dimensions (nscans, nants, 2)
   :param ants: A list of Katpoint Antenna objects [nants]
   :param prefix: Unique plot name prefix (e.g. MeerKAT)

.. autofunction:: ska_sdp_wflow_pointing_offset.plotting.vis_amp_plot.plot_vis_amp(amplitudes, prefix="test")

    Plot visibility amplitude for each scan over number of antennas with one curve per
    frequency chunk

    :param amplitudes: Visibility amplitudes with dimensions
        (nants, num_chunks, nscans). If num_chunks=1, then
        has dimension (nants, nscans)
    :param prefix: Unique plot name prefix (e.g. MeerKAT)

.. automodule:: ska_sdp_wflow_pointing_offset.plotting.gain_amp_plot
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: ska_sdp_wflow_pointing_offset.plotting.gaussian_fits_plot
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: ska_sdp_wflow_pointing_offset.plotting.pointing_offsets_plot
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: ska_sdp_wflow_pointing_offset.plotting.plot_exception_handler
   :members:
   :undoc-members:
   :show-inheritance:

