.. _cli:

Pointing offset CLI module
==========================

The command line interface to the pipeline for estimating the elevation and
cross-elevation offsets from Measurement Sets.

Usage
-----

.. code-block:: none

    > pointing_offset --help

    Program with many options using docopt for computing pointing offsets.
    Note: to run it as an SDP processing script set the SDP_PROCESSING_SCRIPT
          environment variable to "True"

    Usage:
      pointing-offset COMMAND [--msdir=DIR] [--fit_to_sep_pol]
                              [--fit_to_vis] [--apply_mask]
                              [--use_source_offset_column]
                              [--rfi_file=FILE] [--results_dir=None]
                              [--start_freq=None] [--end_freq=None]
                              [(--bw_factor <bw_factor>) [<bw_factor>...]]
                              [--num_chunks=<int>] [--thresh_width=<float>]
                              [--eb_id=None] [--num_scans=<int>]
                              [--use_modelvis]

    Commands:
      compute   Runs all required routines for computing the
      pointing offsets.

    Options:
      -h --help             show this help message and exit
      -q --quiet            report only file names
      --msdir=DIR           Directory including Measurement set files
      --fit_to_sep_pol      Fit primary beams to the parallel-hands gain
                            amplitudes instead of the Stokes I gain amplitudes
                            (Optional) [default: False]
      --fit_to_vis          Fit primary beams to the cross-correlation visibility
                            amplitudes instead of the antenna gain amplitudes
                            (Optional) [default: False]
      --apply_mask          Apply mask (Optional) [default: False]
      --use_source_offset_column  Read on-sky offsets in cross-elevation and
                            elevation from the SOURCE_OFFSET column of the pointing
                            sub-tables? If False, antenna pointings in azimuth
                            and elevation are read from the TARGET column of
                            the pointing table and the on-sky offsets in
                            cross-elevation and elevation are computed thereafter
                            (Optional) [default: False]
      --rfi_file=FILE       RFI file (Optional). If not None, the expected static
                            RFI mask is an HDF5 file located in the SKA Telescope
                            Model Data Repository. Refer to the documentation for
                            how to upload the file to the repository for use if not
                            already in there
      --results_dir=None    Directory where the results (data products) need to be
                            saved (Optional). If None, they are stored in the
                            directory containing the Measurement Sets
      --start_freq=None     Start frequency in MHz (Optional)
      --end_freq=None       End frequency in MHz (Optional)
      --bw_factor           Beamwidth factor for the horizontal and vertical
                            co-polarisations respectively. The constant of
                            proportionality, k when computing theoretical
                            beamwidth (k*lambda/D) [default: 0.976, 1.098]
      --num_chunks=<int>    Number of frequency chunks for calibration
                            [default: 16]
      --thresh_width=<float>  The maximum ratio of the fitted to expected beamwidth
                            [default: 1.15]
      --eb_id=None          Execution block ID (Processing via SDP only)
      --num_scans=<int>     How many scans we expect for a single pointing
                            observation [default: 5] (Processing via SDP only)
      --use_modelvis        Use modelvis for gain solver (Optional)
                            Needs ska-sdp-func to be installed.
                            [default: False]


Commands \& Options
-------------------
List of commands for accessing the functionalities of the pipeline.

.. list-table::
   :widths: 25 25
   :header-rows: 1

   * -
     - Action
   * - **compute**
     - Implements the list of actions below
   * - **msdir**
     - Directory containing measurement set for each discrete pointing scan
   * - **fit_to_sep_pol**
     - Fit primary beams to the parallel-hands gain amplitudes instead of the
       Stokes I amplitudes
   * - **fit_to_vis**
     - Fit primary beams to visibilities instead of antenna gains
   * - **apply_mask**
     - Boolean to apply the RFI mask provided by the ``rfi_file`` argument
   * - **use_source_offset_column**
     - Read on-sky offsets in cross-elevation and elevation from the SOURCE_OFFSET
       column of the pointing sub-tables? If False, antenna pointings in azimuth
       and elevation are read from the TARGET column of the pointing table and
       the on-sky offsets in cross-elevation and elevation are computed thereafter
   * - **rfi_file**
     - Filename containing RFI mask to be applied with the ``apply_mask`` argument,
       in the format of .h5 file
   * - **results_dir**
     - Directory to save the fitted parameters and calculated offsets
   * - **start_freq**
     - Start frequency in MHz to use
   * - **end_freq**
     - End frequency in MHz to use
   * - **bw_factor**
     - Beamwidth factor for the horizontal and vertical polarisations respectively.
       The constant of proportionality, k when computing theoretical beamwidth (``k*lambda/D``)
   * - **num_chunks**
     - Number of frequency chunks for calibration in both fitting the primary beams to
       visibility and gain amplitudes
   * - **thresh_width**
     - The maximum ratio of the fitted to expected beamwidth
   * - **eb_id**
     - Execution block ID (Processing via SDP only)
   * - **num_scans**
     - The expected number of scans for a single pointing observation (Processing via SDP only)
   * - **use_modelvis**
     - Boolean to use a model visibility in the gain solver
