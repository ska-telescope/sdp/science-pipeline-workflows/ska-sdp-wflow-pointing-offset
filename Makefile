include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-wflow-pointing-offset
PROJECT_PATH = ska-telescope/sdp/ska-sdp-wflow-pointing-offset
DOCS_RTD_PROJECT_SLUG = ska-telescope-sdp-pointing-offset-calibration-pipeline
ARTEFACT_TYPE = oci
CHANGELOG_FILE = CHANGELOG.rst

# E203: whitespace before ':'
# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503

# Sphinx options for doc builds
DOCS_SPHINXOPTS = "--fail-on-warning"