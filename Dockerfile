FROM artefact.skao.int/ska-build-python:0.1.1 as build

WORKDIR /install

ENV POETRY_VIRTUALENVS_CREATE=false

COPY . ./

RUN poetry install --no-root --extras "python-casacore ska-sdp-func"
RUN pip install --no-compile .

FROM artefact.skao.int/ska-python:0.1.2

WORKDIR /app

COPY --from=build /usr/local/ /usr/local/

ENTRYPOINT ["pointing-offset"]
