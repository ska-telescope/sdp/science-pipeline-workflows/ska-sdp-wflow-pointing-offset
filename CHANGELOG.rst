Changelog
=========

1.0.0
-----

- Fix loading MS path information from vis-receive data flow
  (`MR146 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/146>`__)
- Update documentation
  (`MR145 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/145>`__)
- Update dependencies, including:

  - ska-sdp-datamodels: 1.0.0
  - ska-sdp-dataqueues: 1.0.0.post1
  - ska-sdp-dataproduct-metadata: 1.0.0.post1
  - ska-sdp-func-python: 1.0.0.post1
  - ska-telmodel: 1.20.0

  (`MR144 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/144>`__, `MR145 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/145>`__)

- Updates the uncertainty calculation after the fitted parameters are averaged
  (`MR140 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/140>`__)
- Data flow state status values are now FAULT or COMPLETE
  (`MR143 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/143>`__)
- Data flow state “internal” is now a list of lists
  (`MR143 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/143>`__)
- Bug fix in updating state of data flows in SDP
  (`MR142 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/142>`__)
- Bug fix in writing metadata file when running pipeline in SDP
  (`MR136 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/136>`__)
- Add documentation on how averaging is computed
  (`MR141 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/141>`__)
- Simplify beam fitting methods
  (`MR138 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/138>`__)
- Compute weighted standard deviation instead of sum of weights in the
  time-and-polarisation averaging function
  (`MR139 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/139>`__)
- Update Dockerfile to use SKA Python base image
  (`MR132 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/132>`__)
- Update pointing pipeline to monitor multiple pointing
  scans(`MR134 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/134>`__)
- Update documentation for users to use pipeline with minimal or no
  support
  (`MR133 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/133>`__)
- Split visibility into frequency chunks before applying static RFI
  mask(`MR131 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/131>`__)

0.9.0
-----

- Update pointing offset pipeline to obtain MS directory from flow
  source
  (`MR127 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/127>`__)
- Update usage of send via data queue with version ska-sdp-dataqueues
  0.4.0
  (`MR128 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/128>`__)

.. _section-1:

0.8.0
-----

- Remove generating plots when running the pipeline in SDP
  (`MR126 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/126>`__)
- Add back Gaussian fits plot in two-dish standalone mode
  (`MR125 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/125>`__)
- Add reporting processing errors in the data flow state
  (`MR123 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/123>`__)
- Ensure dish names and their corresponding fitted pointing offsets
  written into the PointingTable and CSV are ordered correctly, and they
  match
  (`MR122 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/122>`__)
- Update usage of DataQueue with version ska-sdp-dataqueues 0.3.0
  (`MR121 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/121>`__)
- Remove providing empty “interval” and “residual” parameters in
  PointingTable (using latest ska-sdp-datamodels)
  (`MR120 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/120>`__)
- Update ska-sdp-dataqueues==0.2.0, ska-sdp-datamodels==0.3.3
  (`MR120 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/120>`__)
- Add track_duration to PointingTable
  (`MR119 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/119>`__)
- Re-structure Gaussian fits plot to handle beam fits to parallel-hands
  gain amplitudes
  (`MR82 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/82>`__)
- Update documentation for the option to fit the primary beams to the
  parallel-hands gain amplitudes
  (`MR82 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/82>`__)
- Added the option to fit the primary beams to the parallel-hands gain
  amplitudes
  (`MR82 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/82>`__)

.. _section-2:

0.7.0
-----

- Bug fix when calculating on-sky-offsets from TARGET column
  (`MR116 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/116>`__)
- Use data flow entries to determine where to store data products
  (`MR115 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/115>`__)
- Using data flow entry in Config DB to get Kafka queue information
  (`MR110 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/110>`__)
- Use the Data Queue library to pass the pointing offset results to
  Kafka
  (`MR112 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/112>`__)

.. _section-3:

0.6.0
-----

- Fix error in plotting the Gaussian fits because of zero gain
  amplitudes for an antenna for all scans
  (`MR113 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/113>`__)
- Update tests for fitting beams to visibility amplitudes
  (`MR111 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/111>`__)
- Discard zero gain amplitudes and amplitudes close to approximately
  zero on-sky offsets
  (`MR101 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/101>`__)
- Improvements to documentation
  (`MR107 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/107>`__,
  `MR108 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/108>`__)
- Make sure data are sent with dish IDs to Kafka, even if pipeline fails
  (`MR106 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/106>`__)
- Move convert_to_structured function to utils.py
  (`MR105 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/105>`__)
- Set up configurable logging and remove log duplication
  (`MR104 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/104>`__)
- Add reference time and corresponding commanded azel when fitting to
  the visibility amplitudes
  (`MR102 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/102>`__)
- Introduce InitPipeline, SDPPipeline, and StandAlonePipeline classes to
  reduce CLI complexity
  (`MR81 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/81>`__)
- Reduce read_data.py complexity
  (`MR99 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/99>`__)
- A single HDF5 file, text file, and offset plot is output in the
  two-dish scenario
  (`MR95 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/95>`__)
- The beams are fitted to the moving antenna in a two-dish scenario
  (`MR95 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/95>`__)
- Minor restructuring of the CLI
  (`MR95 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/95>`__)
- Fix phase centre bug and failing test with data
  (`MR98 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/98>`__)
- Refactor plotting and output functions to adapt to two-dish mode
  (`MR94 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/94>`__)
- Refactor plotting functions
  (`MR93 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/93>`__)
- Add documentation for the two-dish mode scenario
  (`MR92 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/92>`__)
- Refactor documentation
  (`MR91 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/91>`__)
- Add functionality to split the MS files for two-dish scan modes
  (`MR89 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/89>`__)

.. _section-4:

0.5.0
-----

- Document the data products and unify variable names
  (`MR90 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/90>`__)
- Add reference time and corresponding commanded azel
  (`MR85 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/85>`__,
  bugfix:
  `MR86 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/86>`__)
- Fixing corrupted pointing table
  (`MR84 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/84>`__)
- Updated to send data to Kafka as a structured numpy data. Also include
  the index of the final scan in this data
  (`MR83 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/83>`__)
- Updated to publish pointing offset results in degrees for kafka
  (`MR80 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/80>`__)
- Add exception handler for plotting
  functions(`MR79 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/79>`__)
- Generate a smaller RFI mask from an existing larger one in the
  telmodel store
  (`MR77 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/77>`__)
- Add support for visibility fitting per sub-band
  (`MR77 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/77>`__)

.. _section-5:

0.4.0
-----

- Remove need for extra ghost scan in pointing processing
  (`MR67 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/67>`__)
- Add ska-sdp-func as optional dependency
  (`MR71 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/71>`__)
- Added extra metadata to PointingTable
  (`MR68 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/68>`__)
- Save data and metadata file into a scan-based directory structure
  (`MR74 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/74>`__)
- Load RFI mask in HDF5 format from the telescope model data repository
  (`MR69 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/69>`__)
- Update the metadata saved in ska-data-product.yaml with more Obscore
  items
  (`MR73 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/73>`__)
- Update the URL containing the test datasets after reorganisation of
  data on GCP
  (`MR72 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/72>`__)
- Fixed a bug where the pointing and visibility time sampling were
  assumed to be the same and Gaussian fits plot bugs
  (`MR70 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/70>`__)
- Handle errors generated by pipeline and store information in
  processing blocks state
  (`MR64 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/64>`__)
- Allow pipeline to read and write processed entry into processing block
  (`MR61 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/61>`__)
- Pointing results saved automatically and removed some command line
  arguments
  (`MR63 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/63>`__)
- Bug fix in reading pointing timestamps from the TIME column of the
  pointing sub-table
  (`MR62 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/62>`__)
- Refactor pipeline to output HDF5 data product
  (`MR57 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/57>`__)
- Update CI test jobs to uses standard make targets
  (`MR58 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/58>`__)
- All parameters in the output text file are now written into the
  updated PointingTable
  (`MR56 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/56>`__)
- Added option to send pointing results to Kafka in JSON format
  (`MR55 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/55>`__)
- Fixed shape of antenna pointings read from pointing sub-table of
  Measurement Sets
  (`MR54 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/54>`__)
- Added option to read on-sky offsets in cross-elevation and elevation
  from SOURCE_OFFSET column of the pointing sub-table
  (`MR47 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/47>`__)

.. _section-6:

0.3.0
-----

- Added functions to process multiple scan batches
  (`MR52 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/52>`__)
- Plots fitted parameters with errorbars
  (`MR46 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/46>`__)
- Added skart toml file which handles dependencies in SKA repositories
  (`MR45 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/45>`__)
- Bug fixes, documentation and Gaussian plot improvements
  (`MR44 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/44>`__)
- Added option to use model visibility in the gain solver
  (`MR43 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/43>`__)
- Converts discrete offset pointings in azimuth to cross-elevation
  before fitting
  (`MR42 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/42>`__)
- Update read_data to support MJD in pointing table
  (`MR39 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/39>`__)

0.2.0 (same as 0.1.1)
---------------------

- Added end-to-end test that uses MeerKAT AA0.5 data
  (`MR35 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/35>`__)
- Improved each scan’s pointing timestamps in the tests
  (`MR34 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/34>`__)
- Antenna pointings read from Measurement Sets are in radians
  (`MR32 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/32>`__)
- Fixed documentation build for pipeline
  (`MR31 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/31>`__)
- Update logic to obtain pointing scan IDs from the Configuration
  Database
  (`MR30 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/30>`__)
- Added function to send data to Kafka queues
  (`MR28 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/28>`__,
  `MR36 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/36>`__)
- Updated pipeline to read data from multiple Measurement Sets Based on
  Scan ID
  (`MR27 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/27>`__)
- Used PointingTable to output pointing offsets
  (`MR26 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/26>`__)
- Using zeroth-order spline interpolation to align pointing and
  visibility datasets
  (`MR25 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/25>`__)
- Added monitoring of execution blocks for scan type and id
  (`MR24 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/24>`__)
- Added plotting functions
  (`MR23 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/23>`__)
- Added support for gain calibration per sub-band
  (`MR22 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/22>`__)
- Added option to use calibration weights when fitting primary beams to
  the gain amplitudes
  (`MR21 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/21>`__)
- Bug fix in accessing the concatenated antenna pointings
  (`MR20 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/20>`__)
- Added functionality to read multiple Measurement Sets, one per
  discrete pointing scan
  (`MR19 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/19>`__)

.. _section-7:

0.1.0
-----

- Added Dockerfile for docker image build
  (`MR18 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/18>`__)
- Fixed reshaping of cross-correlation visibilities when using the
  fit_to_vis option
  (`MR17 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/17>`__)
- Added function to interpolate timestamps from data
  (`MR16 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/16>`__)
- Added functionality for fitting the primary beams to the gain
  amplitudes
  (`MR14 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/14>`__)
- Added beamwidth factor parameter
  (`MR13 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/13>`__)
- Modified function to read RFI mask file to read data from txt
  (`MR12 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/12>`__)
- Removed the use of rdb option as parameters are read from the
  Measurement Set
  (`MR10 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/10>`__)
- Updated documentation for the pipeline
  (`MR9 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/9>`__)
- Added integration test for the pipeline
  (`MR8 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/8>`__)
- Added functionality for fitting the primary beams to the visibility
  amplitudes
  (`MR7 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/7>`__)
- Created documentation for the pipeline
  (`MR5 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/5>`__)
- Added functions for reading Measurement Set and writing pointing
  offsets to text file
  (`MR3 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/3>`__)
- Added supporting functions for filtering data and coordinates
  transformation
  (`MR2 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/2>`__)
- Initial setup of repository
  (`MR1 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/1>`__,
  `MR4 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset/-/merge_requests/4>`__)
